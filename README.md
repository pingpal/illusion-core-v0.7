# Illusion Core

## Patch

1. PJDIR=/path/to/this/project/patch/dir
2. THEDIR=/path/to/that/source/dir
3. THEFILE=filename.ext
4. RSTR=$(date '+%s')

5. cd $THEDIR
6. atom $THEFILE
7. - make changes
8. - save as (cmd+shift+s) 'patch.temp'
9. diff -u $THEFILE patch.temp > "$THEFILE.patch"
10. rm -rf patch.temp
11. mv "$THEFILE.patch" "$PJDIR/$THEFILE.$RSTR.patch"
12. cd $PJDIR
13. echo "(cd $(python -c "import os.path; print os.path.relpath('$THEDIR', '$PJDIR')") && patch) < \"$THEFILE.$RSTR.patch\"" >> patch.txt

### AUTHORS
Jonas Hallin

### License

Copyright (c) 2011 Jonas Hallin

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is furnished
to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.