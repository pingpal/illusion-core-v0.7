<?php

namespace Illusion\Session\Backend;

use Illusion\Config;

class Backends {

    static function getDefault() {

        switch (Config::$DEFAULT_SES_BACKEND) {

            case 'sql' : return new SQL();
        }
    }
}
