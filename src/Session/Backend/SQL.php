<?php

namespace Illusion\Session\Backend;

use Illusion\Access\Sql\Nonce;

use Illusion\Access\Sql\Token;

use Illusion\Core\Util;

use Exception;

class SQL {

    function __construct() {

        Util::bool(0.001) && $this->cleanNonce();
        Util::bool(0.001) && $this->cleanToken();
    }

    function newNonce() {

        return (new Nonce())->create();
    }

    function checkNonce($nonce) {

        return (new Nonce())->check($nonce);
    }

    function cleanNonce() {

        (new Nonce())->clean();
    }

    function newToken() {

        return (new Token())->create();
    }

    function cleanToken() {

        (new Token())->clean();
    }

    function getSecretsFromId($id) {

        return $this->getSecretsFromField('id', $id);
    }

    function getSecretsFromPublic($public) {

        return $this->getSecretsFromField('public', $public);
    }

    private function getSecretsFromField($field, $value) {

        $sql = sql()->select()->from('user');

        $sql->leftJoin('group:');
        $sql->leftJoin('keys:');

        $sql->field('user.id');
        $sql->field('keys.public');
        $sql->field('keys.private');
        $sql->field('group:name');

        if ($field == 'id') {

            $sql->where('user.id', $value);
            $sql->where('keys.preferred', true);

        } else {

            $sql->where('keys.public', $value);
        }

        // $sql->where("user.$field = ?", $value);

        $all = $sql->all();

        if (!$all) {

            throw new Exception('bad access');
        }

        $data['id']         = t($all, '0.id/natural?digit', -1);
        $data['public']     = t($all, '0.public/text');
        $data['private']    = t($all, '0.private/text');

        $data['groups'] = [];

        foreach ($all as $row) {

            $data['groups'][] = @$row['name'];
        }

        $data['groups'] = t($data, 'groups/text-rn');

        return $data;
    }
}
