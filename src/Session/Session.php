<?php

namespace Illusion\Session;

use Exception;

class Session {

    protected $store;
    protected $backend;

    protected $secrets = [];

    /**
     * Provide a session store and a session backend to initialize a session
     */
    function __construct($store, $backend) {

        $this->store = $store;
        $this->backend = $backend;

        $this->load();
    }

    /**
     * Get this sessions store
     */
    function store() {

        return $this->store;
    }

    /**
     * Get this sessions backend
     */
    function backend() {

        return $this->backend;
    }

    /**
     * Save secrets state cross requests using a session store
     */
    function save() {

        $this->store->save($this->secrets);
    }

    /**
     * Load secrets state cross requests using a session store
     */
    function load() {

        $secrets = $this->store->load();

        $this->login(@$secrets['id'], null, null, null, false);
    }

    /**
     * Get current set id
     */
    function id() {

        // return 1;

        return @$this->secrets['id'] ?: -1;
    }

    /**
     * Get current set public key
     */
    function getPublicKey() {

        return @$this->secrets['public'];
    }

    /**
     * Get current set private key
     */
    function getPrivateKey() {

        return @$this->secrets['private'];
    }

    /**
    * Get current set groups
    */
    function getGroups() {

        return @$this->secrets['groups'] ?: [];
    }

    /**
     * Retrieve secrets form id using a session backend
     */
    function getSecretsFromId($id) {

        return $this->backend->getSecretsFromId($id);
    }

    /**
     * Retrieve secrets form public key using a session backend
     */
    function getSecretsFromPublic($public) {

        return $this->backend->getSecretsFromPublic($public);
    }

    /**
     * Undet secrets state
     */
    function unsetSecrets() {

        $this->secrets = [];
    }

    /**
     * Set secrets state, must be secrets obtained by one of the getSecretsFrom methods
     */
    function setSecrets($id, $public, $private, $groups) {

        $this->setSecretsArray([ 'id' => $id, 'public' => $public, 'private' => $private, 'groups' => $groups ]);
    }

    /**
     * Set secrets state, must be secrets obtained by one of the getSecretsFrom methods
     */
    function setSecretsArray($secrets) {

        $id = t($secrets, 'id/natural?digit', -1);

        $public =   t($secrets, 'public/text');
        $private =  t($secrets, 'private/text');
        $groups =   t($secrets, 'groups/text-rn');

        if (!in_array('someone', $groups)) {

            $groups = array_merge([ 'someone' ], $groups);
        }

        if (-1 < $id && $public && $private) {

            $this->secrets = [ 'id' => $id, 'public' => $public, 'private' => $private, 'groups' => $groups ];
        }
    }

    /**
     * Clearing set and stored secrets, consequently loging out.
     */
    function logout() {

        $this->secrets = [];

        $this->save();
    }

    /**
     * Set provided secrets or retrieve and set secrets then save them to the store, consequently loging in.
     *
     * If no arguments are provided, current set secrets will be saved to the store, consequently loging in.
     */
    function login($id = null, $public = null, $private = null, $groups = null, $save = null) {

        if (($id || $public) && !$private) {

            if ($id) {

                $secrets = $this->getSecretsFromId($id);

            } else if ($public) {

                $secrets = $this->getSecretsFromPublic($public);
            }

            $this->setSecretsArray($secrets);

        } else if ($id || $public || $private || $groups) {

            $this->setSecrets($id, $public, $private, $groups);
        }

        ($save || $save === null) && $this->save();
    }

    /**
     * Check if session user is currently member of one or more provided groups
     */
    function isMemberOf($groups) {

        is_array($groups) || $groups = [ $groups ];

        $my = @$this->secrets['groups'] ?: [ 'nobody' ];

        foreach ($my as $me) {

            foreach ($groups as $group) {

                if ($me == $group) {

                    return true;
                }
            }
        }

        return false;
    }
}
