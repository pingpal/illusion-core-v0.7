<?php

namespace Illusion\Session\Store;

class CGI {

    function save($secrets) {

        $_SESSION['secrets'] = $secrets;
    }

    function load() {

        return @$_SESSION['secrets'] ?: [];
    }
}
