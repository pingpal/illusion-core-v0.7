<?php

namespace Illusion\Session\Store;

use Illusion\Config;

class Stores {

    static function getDefault($in, $out) {

        switch (Config::$DEFAULT_SES_STORE) {

            case 'cgi' : return new CGI($in, $out);
            case 'sql' : return new SQL($in, $out);
        }
    }
}
