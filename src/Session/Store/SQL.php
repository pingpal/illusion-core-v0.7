<?php

namespace Illusion\Session\Store;

use Illusion\Core\CookieUtil;

use Illusion\Core\Util;

use Exception;

class SQL {

    protected $in;
    protected $out;

    protected $sesid;

    function __construct($in, $out) {

        $this->in = $in;
        $this->out = $out;

        $cookie = $in->vars()->header('cookie');

        $cookie = CookieUtil::instance()->parse($cookie);

        $this->sesid = @$cookie['ILLSESID'];

        if (!$this->sesid) {

            $this->sesid = Util::key();
            $out->cookie('ILLSESID', $this->sesid);
        }
    }

    function save($secrets) {

        $id = @$secrets['id'];

        sql()->delete()->from('session')->where('sesid', $this->sesid)->exe();

        if ($id) {

            sql()->insert()->into('session')->set('sesid', $this->sesid)->set('user', $id)->exe();
        }
    }

    function load() {

        try {

            $act = sql()->beginTransaction();

            $id = sql()->select()->from('session')->field('user')->where('sesid', $this->sesid)->one();

            $act->commitTransaction();

            return [ 'id' => $id ];

        } catch (Exception $e) {

            @$act && $act->rollbackTransaction();
        }
    }
}
