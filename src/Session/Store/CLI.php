<?php

namespace Illusion\Session\Store;

use Illusion\Core\DataStore;

class CLI {

    function save($secrets) {

        DataStore::instance()->save('session.store.cli', $secrets);
    }

    function load() {

        return DataStore::instance()->load('session.store.cli');
    }
}
