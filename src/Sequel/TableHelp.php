<?php

namespace Illusion\Sequel;

use Illusion\Core\DocComments;

use Illusion\Core\Cache;

use Illusion\Config;

use Exception;

class TableHelp {

	static protected $instance;

	static function instance() {

		return self::$instance ?: self::$instance = new self;
	}

	private $tables = [];

	function __construct() {

		$this->tables = Cache::instance()->get('tablehelp');

		if (!$this->tables) {

			$default = 'string';

			$def['id']		= "int not null primary key auto_increment";
			$def['uid']		= "int unsigned not null unique";
			$def['fid']		= "int not null";
			$def['int']		= "int not null default 0";
			$def['uint']	= "int unsigned not null default 0";
			$def['boolean']	= "tinyint(1) not null default 0";
			$def['string']	= "varchar(255) not null default ''";
			$def['unique']	= "varchar(255) not null unique";
			$def['text']	= "text not null";

			foreach (DocComments::instance()->get() as $class => $doc) {

				$table = [];

				if (isset($doc['class'][$class]['table'])) {

					$table['class'] = $class;

					$table['table'] =	end(	$doc['class'][$class]['table']);
					$table['engine'] =	isset(	$doc['class'][$class]['engine']) ? end(	$doc['class'][$class]['engine']) :	null;
					$table['customs'] =	isset(	$doc['class'][$class]['custom']) ? 		$doc['class'][$class]['custom'] :	[];

					$table['columns'] =		[];
					$table['foreigners'] =	[];
					$table['associates'] =	[];

					if (isset($doc['class'][$class]['column'])) {

						foreach ($doc['class'][$class]['column'] as $column) {

							list($name, $type) = array_merge(preg_split('/\s+/', $column, 2), ['']);

							$table['columns'][] = [$name, isset($def[$type]) ? $def[$type] : ($type !== '' ? $type : $def[$default])];
						}
					}

					if (isset($doc['class'][$class]['foreign'])) {

						foreach ($doc['class'][$class]['foreign'] as $foreign) {

							$args = preg_split('/\s+/', $foreign);

							if (!isset($args[1])) {

								$args[] = $args[0];
							}

							if (!isset($args[2])) {

								$args[] = 'id';
							}

							$table['foreigners'][] = $args;
							$table['associates'][] = $args;

							$table['columns'][] = [$args[0], @$args[3] && in_array('null', explode('|', $args[3])) ? 'int' : 'int not null'];
						}
					}

					if (isset($doc['class'][$class]['associate'])) {

						foreach ($doc['class'][$class]['associate'] as $associate) {

							$args = preg_split('/\s+/', $associate);

							if (!isset($args[1])) {

								$args[] = $args[0];
							}

							if (!isset($args[2])) {

								$args[] = 'id';
							}

							$table['associates'][] = $args;
						}
					}

					$this->tables[$table['table']] = $table;
				}
			}

			Cache::instance()->put('tablehelp', $this->tables);
		}
	}

	function getTableFromName($name) {

		return @$this->tables[$name];
	}
}
