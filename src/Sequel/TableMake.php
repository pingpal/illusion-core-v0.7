<?php

namespace Illusion\Sequel;

interface TableMake {

	/**
	 * Get table definition from name using table help
	 */
	function getTableFromName($name);

	/**
	 * Attempts to create table from name using table help
	 */
	function createTableFromName($name);
}
