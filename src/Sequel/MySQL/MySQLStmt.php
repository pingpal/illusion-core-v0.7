<?php

namespace Illusion\Sequel\MySQL;

use Illusion\Sequel\Builder\Statement;

use Exception;

use mysqli_stmt;
use mysqli_result;

class MySQLStmt implements Statement {

	protected $statement;
	protected $query;

	function __construct(mysqli_stmt $statement, $query) {

		$this->statement = $statement;
		$this->query = $query;
	}

	function __destruct() {

		$this->close();
	}

	function statement() {

		return $this->statement;
	}

	function close() {

		try {

			$this->statement->close();

		} catch (Exception $e) {

		}
	}

	function affectedRows() {

		return t($this->statement->affected_rows, 'natural', -1);
	}

	function lastInsertId() {

		return t($this->statement->insert_id, 'natural', -1);
	}

	function bind(array $list) {

		$args = [''];

		for ($i = 0; $i < count($list); $i++) {

			if (is_int($list[$i]) || is_float($list[$i])) {

				$args[0] .= is_int($list[$i]) ? 'i' : 'd';

			} else if (is_string($list[$i])) {

				$args[0] .= strtolower($list[$i]) === 'true' || strtolower($list[$i]) === 'false' ? 'i' : 's';

			} else {

				continue;
			}

			$args[] = &$list[$i];
		}

		if ($args[0] !== '') {

			call_user_func_array([$this->statement, 'bind_param'], $args);
		}
	}

	function execute() {

		$this->statement->execute();

		$result = $this->statement->get_result();

		if ($result instanceof mysqli_result) {

			$result->free();
		}

		$isInsert = !strncasecmp($this->query, 'insert', 6);

		return $isInsert ? $this->lastInsertId() : $this->affectedRows();
	}

	function query() {

		$this->statement->execute();

		$result = $this->statement->get_result();

		if ($result instanceof mysqli_result) {

			return new MySQLSet($result);
		}
	}
}
