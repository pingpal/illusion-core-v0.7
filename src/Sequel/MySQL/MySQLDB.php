<?php

namespace Illusion\Sequel\MySQL;

use Illusion\Sequel\Builder\SelectBuilder;

use Illusion\Sequel\Builder\UpdateBuilder;

use Illusion\Sequel\Builder\InsertBuilder;

use Illusion\Sequel\Builder\DeleteBuilder;

use Illusion\Sequel\Builder\CreateTableBuilder;

use Illusion\Sequel\Builder\Database;

use Illusion\Sequel\Builder\Transactions;

use Illusion\Sequel\TableHelp;

use Illusion\Sequel\TableMake;

use Illusion\Core\Util;

use Exception;

use mysqli;
use mysqli_result;
use mysqli_sql_exception;

class MySQLDB implements TableMake, Database {

	protected $transactions;

	protected $db = null;
	protected $options = null;

	function __construct() {

		$this->transactions = new Transactions();

		function_exists('mysqli_report') && mysqli_report(MYSQLI_REPORT_ALL ^ MYSQLI_REPORT_INDEX);
	}

	function __destruct() {

		$this->disconnect();
	}

	function database() {

		return $this->db;
	}

	function select() {

		return new SelectBuilder($this);
	}

	function update() {

		return new UpdateBuilder($this);
	}

	function insert() {

		return new InsertBuilder($this);
	}

	function delete() {

		return new DeleteBuilder($this);
	}

	function create() {

		return new CreateTableBuilder($this);
	}

	function connect($options = null) {

		if (!$options) {

			$options = $this->options;

		} else {

			$this->options = $options;
		}

		$server = 	t($options, 'server/text');
		$username =	t($options, 'username/text');
		$password = t($options, 'password/text');
		$database = t($options, 'database/text');

		if (function_exists('mysqli_connect')) {

			$this->db = new mysqli($server, $username, $password, $database);
		}
	}

	function disconnect() {

		if ($this->db) {

			try {

				$this->db->close();

			} catch (Exception $e) {

			}
		}

		$this->db = null;
	}

	function reconnect() {

		$this->disconnect();
		$this->connect();
	}

	function failedWithException(mysqli_sql_exception $exception, $tries) {

		if ($tries || $exception->getCode() !== 1317 && $exception->getCode() !== 2006) {

			throw $exception;
		}

		for ($i = 1; $i < 10; $i++) {

			sleep($i);

			try {

				$this->reconnect();

				return;

			} catch (Exception $e) {

			}
		}

		throw $exception;
	}

	function affectedRows() {

		return $this->db ? t($this->db->affected_rows, 'natural', -1) : -1;
	}

	function lastInsertId() {

		return $this->db ? t($this->db->insert_id, 'natural', -1) : -1;
	}

	function escape($string) {

		return $this->db ? $this->db->escape_string($string) : '';
	}

	function execute($query) {

		if (!$this->db) {

			throw new Exception('connect() needs to be called');
		}

		for ($i = 0; 1; $i++) {

			try {

				$result = $this->db->query($query);

				break;

			} catch (mysqli_sql_exception $e) {

				$this->failedWithException($e, $i);
			}
		}

		if ($result instanceof mysqli_result) {

			$result->free();
		}

		$isInsert = !strncasecmp($query, 'insert', 6);

		return $isInsert ? $this->lastInsertId() : $this->affectedRows();
	}

	function query($query) {

		if (!$this->db) {

			throw new Exception('connect() needs to be called');
		}

		for ($i = 0; 1; $i++) {

			try {

				$result = $this->db->query($query);

				break;

			} catch (mysqli_sql_exception $e) {

				$this->failedWithException($e, $i);
			}
		}

		if ($result instanceof mysqli_result) {

			return new MySQLSet($result);
		}
	}

	function prepare($query) {

		if (!$this->db) {

			throw new Exception('connect() needs to be called');
		}

		for ($i = 0; 1; $i++) {

			try {

				$statement = $this->db->prepare($query);

				break;

			} catch (mysqli_sql_exception $e) {

				$this->failedWithException($e, $i);
			}
		}

		return new MySQLStmt($statement, $query);
	}

	function getStatement($query, $param = []) {

		$statement = $this->prepare($query);

		$statement->bind($param);

		return $statement;
	}

	function getResult($query, $param = []) {

		if (count($param)) {

			for ($i = 0; 1; $i++) {

				try {

					return $this->getStatement($query, $param)->query();

				} catch (mysqli_sql_exception $e) {

					$this->failedWithException($e, $i);
				}
			}
		}

		return $this->query($query);
	}

	function exe($query, $param = []) {

		if (count($param)) {

			for ($i = 0; 1; $i++) {

				try {

					return $this->getStatement($query, $param)->execute();

				} catch (mysqli_sql_exception $e) {

					$this->failedWithException($e, $i);
				}
			}
		}

		return $this->execute($query);
	}

	function all($query, $param = []) {

		$result = $this->getResult($query, $param);

		return $result ? $result->all() : null;
	}

	function row($query, $param = []) {

		$result = $this->getResult($query, $param);

		return $result ? $result->row() : null;
	}

	function one($query, $param = []) {

		$result = $this->getResult($query, $param);

		return $result ? $result->one() : null;
	}

	function getTableFromName($name) {

		return TableHelp::instance()->getTableFromName($name);
	}

	function createTableFromName($name) {

		$table = $this->getTableFromName($name);

		if (!$table) {

			throw new Exception("bad table: $name");
		}

		$builder = $this->create()->table($table['table'])->engine($table['engine']);

		foreach ($table['columns'] as $args) {

			$builder->column($args[0], $args[1]);
		}

		foreach ($table['foreigners'] as $args) {

			$builder->foreign($args[0], $args[1], $args[2], @$args[3]);
		}

		foreach ($table['customs'] as $arg) {

			$builder->custom($arg);
		}

		$builder->exe();
	}

	function dropTableWithName($name) {

		$this->exe("DROP TABLE IF EXISTS `$name`");
	}

	function isImplicitCommitSafe() {

		return $this->transactions->isImplicitCommitSafe();
	}

	function requireImplicitCommit() {

		return $this->transactions->requireImplicitCommit();
	}

	function beginTransaction($save = false) {

		$query = $this->transactions->beginTransaction($save);

		$query && $this->execute($query);

		return $this;
	}

	function commitTransaction($save = false) {

		$query = $this->transactions->commitTransaction($save);

		$query && $this->execute($query);

		return $this;
	}

	function rollbackTransaction($save = false) {

		$query = $this->transactions->rollbackTransaction($save);

		$query && $this->execute($query);

		return $this;
	}
}
