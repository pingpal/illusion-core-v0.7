<?php

namespace Illusion\Sequel\MySQL;

use Illusion\Sequel\Builder\ResultSet;

use Exception;

use mysqli_result;

class MySQLSet implements ResultSet {

	protected $result;

	function __construct(mysqli_result $result) {

		$this->result = $result;
	}

	function __destruct() {

		$this->free();
	}

	function result() {

		return $this->result;
	}

	function free() {

		try {

			$this->result->free();

		} catch (Exception $e) {

		}
	}

	function numberOfRows() {

		return t($this->result->num_rows, 'natural', -1);
	}

	function all($options = null) {

		$style = t($options, 'style/number', MYSQLI_ASSOC);

		$rows = $this->result->fetch_all($style);

		return $rows;
	}

	function row($options = null) {

		$style = t($options, 'style/number', MYSQLI_ASSOC);

		return $this->result->fetch_array($style);
	}

	function one() {

		$row = $this->result->fetch_row();

		return is_array($row) && count($row) ? $row[0] : null;
	}
}
