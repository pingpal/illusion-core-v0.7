<?php

namespace Illusion\Sequel\Builder;

class OrderByBlock extends BaseBlock {

	protected $orders = [];

	function order($field, $asc = true) {

		$this->orders[] = (object) ['field' => $field, 'dir' => !!$asc];
	}

	function buildQuery($qb) {
		
		$orders = [];

		foreach ($this->orders as $order) {

			$orders[] = $this->quickQuote("$order->field ") . ($order->dir ? 'ASC' : 'DESC');
		}

		return $orders ? 'ORDER BY ' . implode(', ', $orders) : '';
	}
}