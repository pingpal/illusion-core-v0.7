<?php

namespace Illusion\Sequel\Builder;

use Exception;

class InsertFieldValueBlock extends SetFieldBlock {

	function buildQuery($qb) {

		if (!$this->fields) {

			throw new Exception("set() not called");
		}

		$fields = [];
		$values = [];

		foreach ($this->fields as $field => $value) {

			$fields[] = $this->quickQuote($field);
			$values[] = $this->formatValue($value, true);
		}

		return '(' . implode(', ', $fields) .  ') VALUES (' . implode(', ', $values) . ')';
	}

	function buildParam($qb) {

		if (!$this->fields) {

			throw new Exception('set() not called');
		}

		$holders = [];
		$fields = [];
		$values = [];

		foreach ($this->fields as $field => $value) {

			if ($value instanceof QueryBuilder) {

				$holders[] = '(' . $value->toQuery() . ')';
				$fields[] = $this->quickQuote($field);

				foreach ($value->getValues() as $item) {
					
					$values[] = $this->formatValue($item);
				}
			
			} else {

				$holders[] = '?';
				$fields[] = $this->quickQuote($field);
				$values[] = $this->formatValue($value);
			}
		}

		return (object) ['query' => '(' . implode(', ', $fields) . ') VALUES (' . implode(', ', $holders) . ')', 'values' => $values];
	}
}