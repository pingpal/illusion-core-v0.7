<?php

namespace Illusion\Sequel\Builder;

use Exception;

class SetFieldBlock extends BaseBlock {

	protected $fields = [];

	function set($field, $value) {

		$this->fields[$field] = $value;
	}

	function buildQuery($qb) {

		if (!$this->fields) {

			throw new Exception("set() not called");
		}

		$fields = [];

		foreach ($this->fields as $field => $value) {

			$fields[] = $this->quickQuote("$field = ") . $this->formatValue($value, true);
		}

		return 'SET ' . implode(', ', $fields);
	}

	function buildParam($qb) {

		if (!$this->fields) {

			throw new Exception('set() not called');
		}

		$fields = [];
		$values = [];

		foreach ($this->fields as $field => $value) {

			if ($value instanceof QueryBuilder) {

				$fields[] = $this->quickQuote($field) . ' = (' . $value->toQuery() . ')';

				foreach ($value->getValues() as $item) {
					
					$values[] = $this->formatValue($item);
				}
			
			} else {

				$fields[] = $this->quickQuote("$field = ?");
				$values[] = $this->formatValue($value);
			}
		}

		return (object) ['query' => 'SET ' . implode(', ', $fields), 'values' => $values];
	}
}