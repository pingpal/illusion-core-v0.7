<?php

namespace Illusion\Sequel\Builder;

class StringBlock extends BaseBlock {

	protected $string;

	function __construct(Database $db = null, $string = '') {

		parent::__construct($db);

		$this->string = $string;
	}

	function buildQuery($qb) {

    	return $this->string;
    }
}