<?php

namespace Illusion\Sequel\Builder;

use Exception;

class FromTableBlock extends AbstractTableBlock {

	function from($table, $alias = null) {

		$this->table($table, $alias);
	}

	function buildQuery($qb) {

		if (!$this->tables) {

			throw new Exception("from() not called");
		}

		$tables = parent::buildQuery($qb);

		return "FROM $tables";
	}
}