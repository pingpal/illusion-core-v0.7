<?php

namespace Illusion\Sequel\Builder;

interface ResultSet {

	/**
	 * Returns internal result object
	 */
	function result();

	/**
	 * Frees the memory associated with a result
	 */
	function free();

	/**
	 * Gets the number of rows in a result
	 */
	function numberOfRows();

	/**
	 * Fetches all result rows as an array
	 */
	function all($options = null);

	/**
	 * Fetches the first result row as an array
	 */
	function row($options = null);

	/**
	 * Fetches the first column in the result set
	 */
	function one();
}