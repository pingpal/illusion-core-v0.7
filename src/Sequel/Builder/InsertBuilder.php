<?php

namespace Illusion\Sequel\Builder;

class InsertBuilder extends QueryBuilder {

	function __construct(Database $db = null) {

		parent::__construct($db);

		$this->blocks['string'] =			new StringBlock($db, 'INSERT');
		$this->blocks['intoTable'] =		new IntoTableBlock($db);
		$this->blocks['insertFieldValue'] =	new InsertFieldValueBlock($db);
		$this->blocks['where'] =			new WhereBlock($db);
	}

	function into($table) {

		$this->blocks['intoTable']->into($table);

		return $this;
	}

	function set($field, $value) {

		$this->blocks['insertFieldValue']->set($field, $value);

		return $this;
	}

	function where($condition) { //??

		return $this->whereArgs($condition, array_slice(func_get_args(), 1));
	}

	function whereArgs($condition, $values = []) { //??

		$this->blocks['where']->whereArgs($condition, $values);

		return $this;
	}
}
