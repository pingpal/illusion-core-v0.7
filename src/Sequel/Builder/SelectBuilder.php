<?php

namespace Illusion\Sequel\Builder;

use Exception;

class SelectBuilder extends QueryBuilder {

	function __construct(Database $db = null) {

		parent::__construct($db);

		$this->blocks['string'] =		new StringBlock($db, 'SELECT');
		$this->blocks['distinct'] =		new DistinctBlock($db);
		$this->blocks['getField'] =		new GetFieldBlock($db);
		$this->blocks['fromTable'] =	new FromTableBlock($db);
		$this->blocks['join'] =			new JoinBlock($db);
		$this->blocks['where'] =		new WhereBlock($db);
		$this->blocks['groupBy'] =		new GroupByBlock($db);
		$this->blocks['orderBy'] =		new OrderByBlock($db);
		$this->blocks['limit'] =		new LimitBlock($db);
		$this->blocks['offset'] =		new OffsetBlock($db);
	}

	function isNestable() {

    	return true;
    }

	function distinct() {

		$this->blocks['distinct']->distinct();

		return $this;
	}

	function field($field, $alias = null) {

		foreach ($this->findRefs($field) as $string) {

			$this->blocks['join']->list[] = $string;
		}

		$field = $this->stripRefs($field);

		$this->blocks['getField']->field($field, $alias);

		return $this;
	}

	function from($table, $alias = null) {

		$block = $this->blocks['fromTable'];
		$block->from($table, $alias);

		if (is_string($table)) {

			$used = $alias ? $alias : $table;

			$this->blocks['getField']->froms[] = $used; //??
			$this->blocks['join']->used[$used] = 1;
			$this->blocks['join']->base = $used;
			$this->blocks['join']->real = $table;
		}

		return $this;
	}

	// function base($table, $alias = null) {
	//
	// 	$used = $alias ? $alias : $table;
	//
	// 	$this->blocks['join']->base = $used;
	// 	$this->blocks['join']->real = $table;
	//
	// 	return $this;
	// }

	function join($table, $alias = null, $condition = null, $type = null) {

		$this->blocks['join']->join($table, $alias, $condition, $type);

		return $this;
	}

	function leftJoin($table, $alias = null, $condition = null) {

		$this->blocks['join']->leftJoin($table, $alias, $condition);

		return $this;
	}

	function rightJoin($table, $alias = null, $condition = null) {

		$this->blocks['join']->rightJoin($table, $alias, $condition);

		return $this;
	}

	function innerJoin($table, $alias = null, $condition = null) {

		$this->blocks['join']->innerJoin($table, $alias, $condition);

		return $this;
	}

	function outerJoin($table, $alias = null, $condition = null) {

		$this->blocks['join']->outerJoin($table, $alias, $condition);

		return $this;
	}

	function where($condition) {

		return $this->whereArgs($condition, array_slice(func_get_args(), 1));
	}

	function whereArgs($condition, $values = []) {

		foreach ($this->findRefs($condition) as $string) {

			$this->blocks['join']->list[] = $string;
		}

		$condition = $this->stripRefs($condition);

		$this->blocks['where']->whereArgs($condition, $values);

		return $this;
	}

	function group($field) {

		foreach ($this->findRefs($field) as $string) {

			$this->blocks['join']->list[] = $string;
		}

		$field = $this->stripRefs($field);

		$this->blocks['groupBy']->group($field);

		return $this;
	}

	function order($field, $asc = true) {

		foreach ($this->findRefs($field) as $string) {

			$this->blocks['join']->list[] = $string;
		}

		$field = $this->stripRefs($field);

		$this->blocks['orderBy']->order($field, $asc);

		return $this;
	}

	function limit($max) {

		$this->blocks['limit']->limit($max);

		return $this;
	}

	function offset($start) {

		$this->blocks['offset']->offset($start);

		return $this;
	}
}
