<?php

namespace Illusion\Sequel\Builder;

use Exception;

class BaseBlock {

	protected $db;

	function __construct(Database $db = null) {

		$this->db = $db;
	}

	function buildQuery($qb) {

		return '';
	}

	function buildParam($qb) {

		return (object) ['query' => $this->buildQuery($qb), 'values' => []];
	}

	function quickQuote($query, $skip = []) {

		$skip = $skip ? implode('|', $skip) : '';

		return preg_replace("/(^|(?<=[\s.,()=]))(?!($skip)((?=[\s.,()])|$))(?<!`)([a-z_][a-z0-9_]*)(?!`)/i", '`\\4`', $query);
	}

	function formatValue($value, $flatten = null) {

		if ($flatten) {

			if (is_string($value)) {

				$value = "'" . ($this->db ? $this->db->escape($value) : '') . "'";

			} else if ($value instanceof QueryBuilder) {

				$value = '(' . $value->toString() . ')';
			}
		}

		if ($value === null) {

			return "NULL";
		}

		if ($value === true || $value === false) {

			return $value ? "1" : "0";
		}

		if (is_int($value) || is_float($value) || is_string($value)) {

			return $value;
		}

		throw new Exception('bad value:' . var_export($value, true));
	}
}
