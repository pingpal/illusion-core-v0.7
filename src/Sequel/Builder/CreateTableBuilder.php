<?php

namespace Illusion\Sequel\Builder;

class CreateTableBuilder extends QueryBuilder {

	function __construct(Database $db = null) {

		parent::__construct($db);

		$this->blocks['createTable'] = new CreateTableBlock($db);
	}

	function table($table) {

		$this->blocks['createTable']->table($table);

		return $this;
	}

	function engine($engine) {

		$this->blocks['createTable']->engine($engine);

		return $this;
	}

	function column($name, $definition) {

		$this->blocks['createTable']->column($name, $definition);

		return $this;
	}

	function foreign($name, $table, $column, $options = null) {

		$this->blocks['createTable']->foreign($name, $table, $column, $options);

		return $this;
	}

	function custom($string) {

		$this->blocks['createTable']->custom($string);

		return $this;
	}
}