<?php

namespace Illusion\Sequel\Builder;

use Exception;

class Transactions {

    protected $level = 0;

    function isImplicitCommitSafe() {

        return $this->level === 0;
    }

    function requireImplicitCommit() {

        if (0 < $this->level) {

            throw new Exception('bad transaction state');
        }
    }

    function beginTransaction($save = false) {

        if ($this->level++ == 0) {

            return 'BEGIN';

        } elseif ($save) {

            return "SAVEPOINT LEVEL{$this->level}";
        }
    }

    function commitTransaction($save = false) {

        if (0 < $this->level) {

            $this->level--;

            if (!$this->level) {

                return 'COMMIT';

            } elseif ($save) {

                return "RELEASE SAVEPOINT LEVEL{$this->level}";
            }
        }
    }

    function rollbackTransaction($save = false) {

        if (0 < $this->level) {

            $this->level--;

            if (!$this->level) {

                return 'ROLLBACK';

            } elseif ($save) {

                return "ROLLBACK TO SAVEPOINT LEVEL{$this->level}";
            }
        }
    }
}
