<?php

namespace Illusion\Sequel\Builder;

class GroupByBlock extends BaseBlock {

	protected $groups = [];

	function group($field) {
		
		$this->groups[] = $field;
	}

	function buildQuery($qb) {

		return $this->groups ? 'GROUP BY ' . $this->quickQuote(implode(', ', $this->groups)) : '';
	}
}