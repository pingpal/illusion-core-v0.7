<?php

namespace Illusion\Sequel\Builder;

interface Database {

	/**
	 * Returns internal database object
	 */
	function database();

	/**
	 * Returns select query builder
	 */
	function select();

	/**
	 * Returns update query builder
	 */
	function update();

	/**
	 * Returns insert query builder
	 */
	function insert();

	/**
	 * Returns delete query builder
	 */
	function delete();

	/**
	 * Returns create table query builder
	 */
	function create();
	
	/**
	 * Connects the database using needed options
	 */
	function connect($options = null);

	/**
	 * Disconnects the database
	 */
	function disconnect();

	/**
	 * Reconnects the database using previously given options
	 */
	function reconnect();

	/**
	 * Returns the auto generated id used in the last query
	 */
	function lastInsertId();

	/**
	 * Escapes special characters in a string for use in a statement
	 */
	function escape($string);

	/**
	 * Execute a query (INSERT, UPDATE or DELETE)
	 * Returns the total number of rows changed, deleted, or inserted (affected rows)
	 */
	function execute($query);

	/**
	 * Execute a query (not INSERT, UPDATE or DELETE), returning a result set
	 */
	function query($query);

	/**
	 * Prepare and returns a statement for execution
	 */
	function prepare($query);

	/**
	 * Quick and not so dirty execute with parameters
	 */
	function exe($query, $param = []);

	/**
	 * Quick and not so dirty fetch all with parameters
	 */
	function all($query, $param = []);

	/**
	 * Quick and not so dirty fetch row with parameters
	 */
	function row($query, $param = []);

	/**
	 * Quick and not so dirty fetch column with parameters
	 */
	function one($query, $param = []);
}