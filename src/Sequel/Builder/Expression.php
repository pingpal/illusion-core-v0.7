<?php

namespace Illusion\Sequel\Builder;

use Exception;

class Expression {

	protected $tree;
	protected $current;

	function __construct() {

		$this->tree = (object) ['parent' => null, 'nodes' => []];
		$this->current = $this->tree;
	}

	function sql($node = null) {

		if (!$node) {

			 if ($this->current->parent) {

				throw new Exception('close() not called');
			 }

			 return $this->sql($this->tree);
		}    

		$string = '';
		
		foreach ($node->nodes as $child) {

			if (@$child->expr) {

				$str = $child->expr;

			} else {
				
				$str = $this->sql($child);
				$str === '' || $str = "($str)";
			}

			if ($str !== '') {

				$string === '' || $string .= " $child->type ";
				$string .= $str;
			}
		}

		return $string;
	}

	function open($operator) {

		$tree = (object) ['type' => $operator, 'parent' => $this->current, 'nodes' => []];

		$this->current->nodes[] = $tree;
		$this->current = $tree;

		return $this;
	}

	function add($operator, $expr) {

		if (!is_string($expr)) {

			throw new Exception('bad expr');
		}

		$this->current->nodes[] = (object) ['type' => $operator, 'expr' => $expr];

		return $this;
	}

	function close() {

		if (!$this->current->parent) {

			throw new Exception('open() not called');
		}

		$this->current = $this->current->parent;

		return $this;
	}
}