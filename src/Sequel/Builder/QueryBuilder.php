<?php

namespace Illusion\Sequel\Builder;

class QueryBuilder {

	public $blocks = [];

	protected $db;
	protected $queryAndValues = null;

	function __construct(Database $db = null) {

		$this->db = $db;
	}

	function isNestable() {

		return false;
	}

	function findRefs($string) {

		preg_match_all('/\\b([a-z_][a-z0-9_.:]*[.:])[a-z_]*/i', $string, $matches);

		return $matches[1];
	}

	function stripRefs($string) {

		return preg_replace('/(?:\\b[a-z_][a-z0-9_.:]*[.:])?([a-z_][a-z0-9_]*)[.:]([a-z_*])/i', '\\1.\\2', $string);
	}

	function format($query) {

		if (preg_match('/^CREATE TABLE/i', $query)) {

			$query = preg_replace('/^(.*?\\()(.*)(\\).*?)$/', "$1\n\x20\x20\x20\x20$2\n$3", $query, 1);
			$query = preg_replace("/,\s*/", ",\n\x20\x20\x20\x20", $query);
		}

		$query = preg_replace('/\\b(WHERE|FROM|GROUP BY|HAVING|ORDER BY|LIMIT|OFFSET|UNION|ON DUPLICATE KEY UPDATE|VALUES)\\b/', "\n$0", $query);
		$query = preg_replace('/\\b(INNER|LEFT|RIGHT|CASE|WHEN|END|ELSE|AND)\\b/', "\n\x20\x20\x20\x20$0", $query);
		$query = preg_replace("/\s+\n/", "\n", $query);

		return $query;
	}

	function numberedParameters() {

		return false;
	}

	function toString() {

		$query = [];

		foreach ($this->blocks as $block) {

			$string = $block->buildQuery($this);
			$string === '' || $query[] = $string;
		}

		return $this->format(implode(' ', $query));
	}

	function toQueryAndValues() {

		if (!$this->queryAndValues) {

			$query = [];
			$values = [];

			foreach ($this->blocks as $block) {

				$param = $block->buildParam($this);
				$param->query === '' || $query[] = $param->query;

				foreach ($param->values as $value) {

					$values[] = $value;
				}
			}

			$query = implode(' ', $query);

			if ($this->numberedParameters()) {

				for ($i = 1; strpos($query, '?') !== false; $i++) {

					$callback = function () use ($i) { return "\$$i"; };
					$query = preg_replace_callback('/\\?/', $callback, $query, 1);
				}
			}

			$query = QueryBuilder::format($query);

			$this->queryAndValues = (object) ['query' => $query, 'values' => $values];
		}

		return $this->queryAndValues;
	}

	function toQuery() {

		return $this->toQueryAndValues()->query;
	}

	function getValues() {

		return $this->toQueryAndValues()->values;
	}

	function db() {

		return $this->db;
	}

	function exe() {

		if ($this->db) {

			return $this->db->exe($this->toQuery(), $this->getValues());
		}
	}

	function all() {

		if ($this->db) {

			return $this->db->all($this->toQuery(), $this->getValues());
		}
	}

	function row() {

		if ($this->db) {

			return $this->db->row($this->toQuery(), $this->getValues());
		}
	}

	function one() {

		if ($this->db) {

			return $this->db->one($this->toQuery(), $this->getValues());
		}
	}
}
