<?php

namespace Illusion\Sequel\Builder;

class DeleteBuilder extends QueryBuilder {

	function __construct(Database $db = null) {

		parent::__construct($db);

		$this->blocks['string'] =		new StringBlock($db, 'DELETE');
		$this->blocks['fromTable'] =	new FromTableBlock($db);
		$this->blocks['join'] =			new JoinBlock($db);
		$this->blocks['where'] =		new WhereBlock($db);
		$this->blocks['orderBy'] =		new OrderByBlock($db);
		$this->blocks['limit'] =		new LimitBlock($db);
	}

	function from($table, $alias = null) {

		$this->blocks['fromTable']->from($table, $alias);

		return $this;
	}

	function join($table, $alias = null, $condition = null, $type = null) {

		$this->blocks['join']->join($table, $alias, $condition, $type);

		return $this;
	}

	function leftJoin($table, $alias = null, $condition = null) {

		$this->blocks['join']->leftJoin($table, $alias, $condition);

		return $this;
	}

	function rightJoin($table, $alias = null, $condition = null) {

		$this->blocks['join']->rightJoin($table, $alias, $condition);

		return $this;
	}

	function outerJoin($table, $alias = null, $condition = null) {

		$this->blocks['join']->outerJoin($table, $alias, $condition);

		return $this;
	}

	function where($condition) {

		return $this->whereArgs($condition, array_slice(func_get_args(), 1));
	}

	function whereArgs($condition, $values = []) {

		$this->blocks['where']->whereArgs($condition, $values);

		return $this;
	}

	function order($field, $asc = true) {

		$this->blocks['orderBy']->order($field, $asc);

		return $this;
	}

	function limit($max) {

		$this->blocks['limit']->limit($max);

		return $this;
	}
}
