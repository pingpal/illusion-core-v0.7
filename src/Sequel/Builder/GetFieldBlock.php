<?php

namespace Illusion\Sequel\Builder;

class GetFieldBlock extends BaseBlock {

	public $froms = []; //??

	protected $fields = [];

	function field($field, $alias = null) {

		$this->fields[] = (object) ['name' => $field, 'alias' => $alias];
	}

	function buildQuery($qb) {

		$fields = '';

		foreach ($this->fields as $field) {

			$fields === '' || $fields .= ', ';
			$fields .= $field->name;

			if ($field->alias) {

				$fields .= " AS $field->alias";
			}
		}

		$fields = $this->quickQuote($fields, ['COUNT', 'AS', 'SUM']);

		return $fields !== '' ? $fields : '*';
	}
}
