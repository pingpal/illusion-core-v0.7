<?php

namespace Illusion\Sequel\Builder;

interface Statement {

	/**
	 * Returns internal statement object
	 */
	function statement();

	/**
	 * Closes a prepared statement
	 */
	function close();

	/**
	 * Returns the total number of rows changed, deleted, or inserted by the last executed statement
	 */
	function affectedRows();

	/**
	 * Binds list of references to a prepared statement as parameters
	 */
	function bind(array $list);

	/**
	 * Execute a prepared query (INSERT, UPDATE or DELETE)
	 * Returns the total number of rows changed, deleted, or inserted (affected rows)
	 */
	function execute();

	/**
	 * Execute a prepared query (not INSERT, UPDATE or DELETE), returning a result set
	 */
	function query();
}