<?php

namespace Illusion\Sequel\Builder;

use Exception;

class IntoTableBlock extends BaseBlock {

	protected $table = null;

	function into($table) {

		$this->table = $table;
	}

	function buildQuery($qb) {

		if (!$this->table) {

			throw new Exception('into() not called');
		}

		if (!is_string($this->table)) {

			throw new Exception('bad table');
		}

		return 'INTO ' . $this->quickQuote($this->table);
	}
}