<?php

namespace Illusion\Sequel\Builder;

class UpdateBuilder extends QueryBuilder {

	function __construct(Database $db = null) {

		parent::__construct($db);

		$this->blocks['string'] =		new StringBlock($db, 'UPDATE');
		$this->blocks['updateTable'] =	new AbstractTableBlock($db);
		$this->blocks['join'] =			new JoinBlock($db);
		$this->blocks['setField'] =		new SetFieldBlock($db);
		$this->blocks['where'] =		new WhereBlock($db);
		$this->blocks['orderBy'] =		new OrderByBlock($db);
		$this->blocks['limit'] =		new LimitBlock($db);
	}

	function table($table, $alias = null) {

		$block = $this->blocks['updateTable'];
		$block->table($table, $alias);

		if (is_string($table)) {

			$used = $alias ? $alias : $table;

			$this->blocks['join']->used[$used] = 1;
			$this->blocks['join']->base = $used;
			$this->blocks['join']->real = $table;
		}

		return $this;
	}

	function set($field, $value) {

		foreach ($this->findRefs($field) as $string) {

			$this->blocks['join']->list[] = $string;
		}

		$field = $this->stripRefs($field);

		$this->blocks['setField']->set($field, $value);

		return $this;
	}

	function where($condition) {

		return $this->whereArgs($condition, array_slice(func_get_args(), 1));
	}

	function whereArgs($condition, $values = []) {

		foreach ($this->findRefs($condition) as $string) {

			$this->blocks['join']->list[] = $string;
		}

		$condition = $this->stripRefs($condition);

		$this->blocks['where']->whereArgs($condition, $values);

		return $this;
	}

	function order($field, $asc = true) {

		foreach ($this->findRefs($field) as $string) {

			$this->blocks['join']->list[] = $string;
		}

		$field = $this->stripRefs($field);

		$this->blocks['orderBy']->order($field, $asc);

		return $this;
	}

	function limit($max) {

		$this->blocks['limit']->limit($max);

		return $this;
	}
}
