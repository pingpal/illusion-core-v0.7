<?php

namespace Illusion\Sequel\Builder;

use Exception;

class CreateTableBlock extends BaseBlock {

	protected $table =	'';
	protected $engine =	null;

	protected $columns =	[];
	protected $foreigners =	[];
	protected $customs =	[];

	function table($table) {

		$this->table = $table;
	}

	function engine($engine) {

		$this->engine = $engine;
	}

	function column($name, $definition) {

		$this->columns[] = [$name, $definition];
	}

	function foreign($name, $table, $column, $options = null) {

		$this->foreigners[] = [$name, $table, $column, $options];
	}

	function custom($string) {

		$this->customs[] = $string;
	}

	function buildQuery($qb) {

		if (!$this->columns) {

			throw new Exception('column() not called');
		}

		$string = '';

		foreach ($this->columns as $column) {

			$string .= $string === '' ? "CREATE TABLE IF NOT EXISTS `$this->table` (" : ', ';
			$string .= "`$column[0]` $column[1]";
		}

		foreach ($this->foreigners as $foreign) {

			$string .= ", FOREIGN KEY (`$foreign[0]`) REFERENCES `$foreign[1]` (`$foreign[2]`)";

			$foreign[3] && in_array('odc', explode('|', $foreign[3])) && $string .= ' ON DELETE CASCADE';
		}

		foreach ($this->customs as $custom) {

			$string .= ", $custom";
		}

		$string .= ")";

		if ($this->engine) {

			$string.= " ENGINE=$this->engine";
		}

		return $string;
	}
}