<?php

namespace Illusion\Sequel\Builder;

class DistinctBlock extends BaseBlock {

	protected $distinct = false;

	function distinct() {

		$this->distinct = true;
	}

	function buildQuery($qb) {

		return $this->distinct ? 'DISTINCT' : '';
	}
}