<?php

namespace Illusion\Sequel\Builder;

class WhereBlock extends BaseBlock {

	protected $wheres = [];
	protected $wheresParam = [];

	protected $keys = [];
	protected $vals = [];

	function where($condition) {

		return $this->whereArgs($condition, array_slice(func_get_args(), 1));
	}

	function whereArgs($condition, $values = []) {

		if ($condition instanceof Expression) {

			$condition = $condition->toString();
		}

		$this->keys[] = $condition;
		$this->vals[] = $values;
	}

	function buildQuery($qb) {

		$string = '';

		for ($i = 0; $i < count($this->keys); $i++) {

			$condition = $this->keys[$i];

			if (preg_match('/^[a-z_][a-z0-9_.]*$/i', $condition)) {

				if (is_array(@$this->vals[$i][0])) {

					$condition = "$condition IN ?";

				} else if ($this->vals[$i]) {

					$condition = "$condition = ?";
				}
			}

			$condition = $this->quickQuote($condition, ['NULL', 'TRUE', 'FALSE', 'LIKE', 'AND', 'OR', 'NOT', 'IN', 'IS', 'EXISTS']);

			foreach ($this->vals[$i] as $value) {

				if (is_array($value)) {

					$items = '';

					foreach ($value as $item) {

						if ($items !== '') {

							$items .= ', ';
						}

						$items .= $this->formatValue($item, true);
					}

					$value = "($items)";

				} else {

					$value = $this->formatValue($value, true);
				}

				$condition = preg_replace('/\\?/', $value, $condition, 1);
			}

			if ($string !== '') {

				$string .= ') AND (';
			}

			$string .= $condition;
		}

		if ($string !== '') {

			$string = "WHERE ($string)";
		}

		return $string;
	}

	function buildParam($qb) {

		$string = '';
		$array = [];

		for ($i = 0; $i < count($this->keys); $i++) {

			$condition = $this->keys[$i];

			if (preg_match('/^[a-z_][a-z0-9_.]*$/i', $condition)) {

				if (is_array(@$this->vals[$i][0])) {

					$condition = "$condition IN ?";

				} else {

					$condition = "$condition = ?";
				}
			}

			$condition = str_replace('?', '$', $condition);

			$condition = $this->quickQuote($condition, ['NULL', 'TRUE', 'FALSE', 'LIKE', 'AND', 'OR', 'NOT', 'IN', 'IS', 'EXISTS']);

			$values = [];

			foreach ($this->vals[$i] as $value) {

				if ($value instanceof QueryBuilder) {

					$placeholder = '(' . $value->toQuery() . ')';

					foreach ($value->getValues() as $item) {

						$values[] = $this->formatValue($item);
					}

				} else if (is_array($value)) {

					$items = '';

					foreach ($value as $item) {

						if ($items !== '') {

							$items .= ', ';
						}

						$items .= '?';
						$values[] = $this->formatValue($item);
					}

					$placeholder = "($items)";

				} else {

					$placeholder = '?';
					$values[] = $this->formatValue($value);
				}

				$condition = preg_replace('/\\$/', $placeholder, $condition, 1);
			}

			foreach ($values as $value) {

				$array[] = $value;
			}

			if ($string !== '') {

				$string .= ') AND (';
			}

			$string .= $condition;
		}

		if ($string !== '') {

			$string = "WHERE ($string)";
		}

		$string = str_replace('$', '?', $string);

		return (object) ['query' => $string, 'values' => $array];
	}
}
