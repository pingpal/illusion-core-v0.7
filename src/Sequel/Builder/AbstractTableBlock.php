<?php

namespace Illusion\Sequel\Builder;

use Exception;

class AbstractTableBlock extends BaseBlock {

	protected $tables = [];

	function table($table, $alias = null) {

		$this->tables[] = (object) ['table' => $table, 'alias' => $alias];
	}

	function buildQuery($qb) {

		if (!$this->tables) {

			throw new Exception('table() not called');
		}

		$tables = '';

		foreach ($this->tables as $table) {

			$tables === '' || $tables .= ', ';

			if ($table->table instanceof QueryBuilder && $table->table->isNestable()) {

				$tables .= '(' . $table->table->toString() . ')';
			
			} else if (is_string($table->table)) {

				$tables .= $table->table;

			} else {

				throw new Exception('bad table');
			}

			if ($table->alias) {

				$tables .= " $table->alias";
			}
		}

		return $this->quickQuote($tables);
	}
}