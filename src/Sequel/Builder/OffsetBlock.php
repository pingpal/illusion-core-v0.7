<?php

namespace Illusion\Sequel\Builder;

class OffsetBlock extends BaseBlock {

	protected $offset = 0;

	function offset($i) {

      	$this->offset = is_string($i) && ctype_i($i) ? intval($i) : (is_int($i) && 0 < $i ? $i : 0);
    }

    function buildQuery($qb) {

     	return $this->offset ? "OFFSET $this->offset" : '';
    }
}