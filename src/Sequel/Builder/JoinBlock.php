<?php

namespace Illusion\Sequel\Builder;

use Exception;

class JoinBlock extends BaseBlock {

	public $base = '';
	public $real = '';

	public $used = [];
	public $list = [];

	public $joins = [];

	function fix($string, $alias = null, $type = null) {

		$base = $this->base;
		$real = $this->real;

		$n = preg_match_all('/([a-z_][a-z0-9_]*[.:]?)/i', "$string.", $matches) - 1;

		foreach ($matches[1] as $i => $table) {

			$back = substr($table, -1) === ':';
			$table = substr($table, 0, -1);

			if ($base && $base !== $table) {

				if ($back) {

					$condition = ($n == $i && $alias ? $alias : $table) . ".$real = $base.id";

				} else {

					$condition = ($n == $i && $alias ? $alias : $table) . ".id = $base.$table";
				}

				$this->join($table, $n == $i ? $alias : null, $condition, $type);
			}

			$base = $table;
			$real = $table;
		}
	}

	function join($table, $alias = null, $condition = null, $type = null) {

		if (!$condition) {

			if (!is_string($table)) {

				throw new Exception('bad table');
			}

			$this->fix($table, $alias, $type);

			return;
		}

		if ($condition instanceof Expression) {

			$condition = $condition->toString();
		}

		if (!$type) {

			$type = 'INNER';
		}

		$type = strtoupper($type);

		$used = $alias ? $alias : (is_string($table) ? $table : null);

		if (!$used || !@$this->used[$used]) {

			$used && $this->used[$used] = 1;

			$this->joins[] = (object) ['table' => $table, 'alias' => $alias, 'condition' => $condition, 'type' => $type];
		}
	}

	function leftJoin($table, $alias = null, $condition = null) {

		return $this->join($table, $alias, $condition, 'LEFT');
	}

	function rightJoin($table, $alias = null, $condition = null) {

		return $this->join($table, $alias, $condition, 'RIGHT');
	}

	function innerJoin($table, $alias = null, $condition = null) {

		return $this->join($table, $alias, $condition, 'INNER');
	}

	function outerJoin($table, $alias = null, $condition = null) {

		return $this->join($table, $alias, $condition, 'OUTER');
	}

	function buildQuery($qb) {

		foreach ($this->list as $string) {

			$this->join($string);
		}

		$joins = '';

		foreach ($this->joins as $join) {

			if ($joins !== '') {

				$joins .= ' ';
			}

			$joins .= "$join->type JOIN ";

			if ($join->table instanceof QueryBuilder && $join->table->isNestable()) {

				$joins .= '(' . $join->table->toString() . ')';

			} else if (is_string($join->table)) {

				$joins .= $join->table;

			} else {

				throw new Exception('bad table');
			}

			if ($join->alias) {

				$joins .= " $join->alias";
			}

			if ($join->condition) {

				$joins .= " ON $join->condition";
			}
		}

		return $this->quickQuote($joins, ['LEFT', 'RIGHT', 'INNER', 'OUTER', 'JOIN', 'ON']);
	}
}
