<?php

namespace Illusion\Sequel\Builder;

class LimitBlock extends BaseBlock {

	protected $limit = 0;

	function limit($i) {

		$this->limit = is_string($i) && ctype_i($i) ? intval($i) : (is_int($i) && 0 < $i ? $i : 0);
    }

	function buildQuery($qb) {

		return $this->limit ? "LIMIT $this->limit" : '';
    }
}