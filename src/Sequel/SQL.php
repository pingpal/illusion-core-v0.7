<?php

namespace Illusion\Sequel;

use Illusion\Sequel\MySQL\MySQLDB;

use Illusion\Config;

use Exception;

class SQL {

	static protected $instance;

	static function instance() {

		return self::$instance ?: self::$instance = new self;
	}

	protected $POOL = [];

	static function factory() {

		return function ($query = null) {

			return $query ? SQL::instance()->db()->exe($query) : SQL::instance()->db();
		};
	}

	function db() {

		switch (Config::$DEFAULT_DATABASE) {

			case 'mysql' : return $this->mysql();
		}
	}

	function mysql($server = null, $username = null, $password = null, $database = null) {

		$opt['server'] =	$server		?: Config::$MYSQL_SERVER;
		$opt['username'] =	$username	?: Config::$MYSQL_USERNAME;
		$opt['password'] =	$password	?: Config::$MYSQL_PASSWORD;
		$opt['database'] =	$database	?: Config::$MYSQL_DATABASE;

		$hash = sha1("mysql$opt[server]$opt[username]$opt[password]$opt[database]");

		if (!@$this->POOL[$hash]) {

			$db = new MySQLDB();

			try {

				$db->connect($opt);
				$this->POOL[$hash] = $db;

			} catch (Exception $e) {

				throw new Exception('unable to connect mysql');
			}
		}

		return $this->POOL[$hash];
	}
}
