<?php

function sql() {

    static $sql;

    $sql ?: $sql = \Illusion\Sequel\SQL::factory();

    return call_user_func_array($sql, func_get_args());
}
