<?php

    declare(ticks = 1);

    namespace Illusion\Core;

    use Illusion\Core\Context\CLI;

    use Illusion\Config;

    include 'Core/App.php';

    $app = new App();
    $copy = $argv;

    $args = OptParse::instance()->apply($copy);

    $action = @$args[0];
    $action && array_shift($args);

    @array_shift($copy);
    @array_shift($copy);
    $raw = implode(' ', $copy);

    $vars = new InitVars($args, $raw);
    $context = new CLI($vars);
    $app->context($context);

    $logo  = "        ___\n";
    $logo .= "       / / \\\n";
    $logo .= "      / /   \\\n";
    $logo .= "     / / / \ \\\n";
    $logo .= "    / / / \ \ \\\n";
    $logo .= "   / /_/___\ \ \\\n";
    $logo .= "  /_________\ \ \\\n";
    $logo .= "  \____________\/\n";
    $text  = "     IllUSION\n";

    $style  = "<bold><white>%s</white></bold>";
    $style .= "<bold><cyan>%s</cyan></bold>\n%s\n";

    $image = sprintf($style, $logo, $text, Config::$HELLO);

    $action ? l($action, $args, $context) : $context->out()->put($image);
