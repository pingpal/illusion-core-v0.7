<?php

namespace Illusion\Net;

class Pool {

    protected $pool = [];

    function addClient($client) {

        $uid = $client->id();
        $cid = $client->endpoint()->id();

        if ($uid && $cid) {

            $this->pool[$uid][$cid] = $client;

            return count($this->pool[$uid]) == 1;
        }

        return false;
    }

    function removeClient($client) {

        $uid = $client->id();
        $cid = $client->endpoint()->id();

        if ($uid && $cid) {

            $had = isset($this->pool[$uid]);

            unset($this->pool[$uid][$cid]);

            if (!@$this->pool[$uid]) {

                unset($this->pool[$uid]);

                return $had;
            }
        }

        return false;
    }

    function getClients($uid) {

        return array_values(@$this->pool[$uid] ?: []);
    }

    function getAllClients() {

        $all = [];

        foreach ($this->pool as $clients) {

            foreach ($clients as $client) {

                $all[] = $client;
            }
        }

        return $all;
    }
}
