<?php

namespace Illusion\Net\Protocol;

use Illusion\Net\Package;

use Illusion\Net\Message;

use Illusion\Core\Packet;

use Illusion\Core\HttpUtil;

use Illusion\Core\Util;

use Exception;

class WebSocket implements Protocol {

    protected $length = -1;
    protected $buffer = '';
    protected $excess = '';

    protected $frames = [];

    protected $request = null;

    protected $handshake;

    protected $http;

    protected $isJson;
    protected $isHttp;

    function __construct() {

        $this->http = HttpUtil::instance();
    }

    function testBytePattern($bytes) {

    }

    protected function bytesToNumber($bytes) {

        for ($number = 0, $i = 0; $i < strlen($bytes); $i++) {

            $number = $number << 8 | ord($bytes[$i]);
        }

        return $number;
    }

    function createMessages($bytes) {

        $this->buffer .= $bytes;
        $length = strlen($this->buffer);

        if ($this->length < 0) {

            // If closing handshake, return bytes
            if (ord($this->buffer[0] & "\x0F") == 8) {

                $this->handshake = $this->buffer[0];

                $this->buffer = '';
                $this->frames = [];

                return;
            }

            // If ping, respond with pong
            if (ord($this->buffer[0] & "\x0F") == 9) {

                $this->buffer[0] = $this->buffer[0] & "\xF0" | "\xA";

                $this->handshake = $this->buffer[0];

                $this->buffer = '';
                $this->frames = [];

                return;
            }

            if ($length < 8) {

                return;
            }

            $this->length = ord($this->buffer[1]) & 0x7f;

            if ($this->length == 126) {

                $this->length = $this->bytesToNumber(substr($this->buffer, 2, 2));

            } else if ($this->length == 127) {

                $this->length = $this->bytesToNumber(substr($this->buffer, 3, 7));
            }
        }

        if (65535 < $this->length) {

            throw new Exception('frame too long');
        }

        $len = ord($this->buffer[1]) & 0x7f;
        $msk = ord($this->buffer[1]) & 0x80;
        $fin = ord($this->buffer[0]) & 0x80;

        $offset = (125 < $len ? 126 == $len ? 4 : 10 : 2) + ($msk ? 4 : 0);

        if($this->length > $length - $offset) {

            return;
        }

        $mask = $msk ? substr($this->buffer, $offset - 4, 4) : null;
        $frame = substr($this->buffer, $offset, $this->length);
        $this->buffer = substr($this->buffer, $offset + $this->length);

        $this->length = -1;

        if ($mask) {

            for ($i = 0; $i < strlen($frame); $i++) {

                $frame[$i] = chr(ord($frame[$i]) ^ ord($mask[$i % 4]));
            }
        }

        $this->frames[] = $frame;

        if (99 < count($this->frames)) {

            throw new Exception('too many frames');
        }

        if($fin) {

            $this->excess = $this->buffer;
            $this->buffer = '';

            $frames = implode($this->frames);
            $this->frames = [];

            if ($frames[0] == '1' || $frames[1] == '2') {

                $packet = new Packet();
                $packet->bytes($frames);

                $message = new Package();
                $message->packet($packet);

            } else {

                $this->isJson = false;
                $this->isHttp = false;

                $data = @json_decode($frames, true);

                if ($data) {

                    $this->isJson = true;

                } else {

                    $data = $this->http->parseRequest($frames);

                    if ($data) {

                        $this->isHttp = true;

                        $json = @json_decode($data['body'], true);

                        if ($json) {

                            $data = $json;

                            $this->isJson = true;
                        }
                    }
                }

                if (!$data) {

                    $data = [];
                }

                $message = new Message();
                $message->data($data);
            }

            return [ $message ];
        }
    }

    function getOverflow() {

        return $this->excess;
    }

    function serializeMessage($message) {

        if ($message instanceof Package) {

            $bytes = $message->getSignedBytes();

        } else {

            $bytes = '';
            $type = null;
            $data = $message->data();

            if ($this->isJson) {

                $bytes = json_encode($data);
                $bytes = Util::prettyPrintJSON($bytes);
                $type = 'application/javascript';
                $data = null;
            }

            if ($this->isHttp) {

                $bytes = $this->http->renderResponse($bytes, $type, null, null, $data);
            }
        }

        $length = strlen($bytes);

        $head = ' ';

        $head[0] = chr(129);

        if ($length <= 125) {

            $head[1] = chr($length);

        } else {

            if ($length >= 126 && $length <= 65535) {

                $head[1] = chr(126);
                $max = 3;

            } else {

                $head[1] = chr(127);
                $max = 9;
            }

            for ($i = 2; $i <= $max; $i++) {

                $head[$i] = chr(( $length >> (($max - $i) * 8) ) & 255);
            }
        }

        return $head . $bytes;
    }

    function handshake() {

        $handshake = $this->handshake;

        $this->handshake = null;

        return $handshake;
    }

    function upgrade() {

    }
}
