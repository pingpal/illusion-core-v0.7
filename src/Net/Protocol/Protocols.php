<?php

namespace Illusion\Net\Protocol;

use Illusion\Net\Protocol\APNSV2;

use Illusion\Net\Protocol\GCMHTTP;

use Illusion\Net\Protocol\Headed;

use Illusion\Net\Protocol\Chunked;

use Illusion\Net\Protocol\WebSocket;

use Illusion\Net\Protocol\WSHS;

use Illusion\Net\Protocol\Hypertext;

class Protocols {

    static function protocol($bytes) {

        if (($protocol = new GCMHTTP()) && $protocol->testBytePattern($bytes)) {

            return $protocol;

        } else

        if (($protocol = new Headed()) && $protocol->testBytePattern($bytes)) {

            return $protocol;

        } else

        if (($protocol = new Chunked()) && $protocol->testBytePattern($bytes)) {

            return $protocol;

        } else

        if (($protocol = new WSHS()) && $protocol->testBytePattern($bytes)) {

            return $protocol;

        } else

        if (($protocol = new Hypertext()) && $protocol->testBytePattern($bytes)) {

            return $protocol;

        } else

        {}
    }
}
