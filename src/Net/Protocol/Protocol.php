<?php

namespace Illusion\Net\Protocol;

interface Protocol {

    /**
     * Test whether or not byte pattern matches this protocol
     */
    function testBytePattern($bytes);

    /**
     * Constructs zero or more messages from this and previous input.
     */
    function createMessages($bytes);

    /**
     * Returns unused buffered bytes.
     */
    function getOverflow();

    /**
     * Transforms a message to bytes
     */
    function serializeMessage($message);

    /**
     * Returned bytes will immediately be written to the socket
     */
    function handshake();

    /**
     * Returned protocol will be considered negotiated to be used from now on until next upgrade
     */
    function upgrade();
}
