<?php

namespace Illusion\Net\Protocol\Parser;

interface Parser {

    /**
     * Parse bytes into a list of messages
     */
    function apply($bytes, $options);
}
