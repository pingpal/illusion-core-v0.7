<?php

namespace Illusion\Net\Protocol\Parser;

use Illusion\Net\Message;

use Illusion\Net\Package;

use Illusion\Core\Packet;

use Illusion\Core\Util;

class PathPackets implements Parser {

    protected $meta;

    function getCleanMetaData() {

        $meta = $this->meta ?: [];

        $this->meta = null;

        return $meta;
    }

    function apply($bytes, $options) {

        // Parse query string and path
        $query = Util::queryToArray($options['query']);
        $path = explode('/', trim($options['path'], '/'));

        // Store query string as meta data
        $this->meta = $query;

        // path packet protocol
        if (@$path[0] == '0') {

            $data = [];

            // Gather relevant data from query string
            foreach ([ 'callback', 'noack' ] as $key) {

                @$query[$key] && $data[$key] = $query[$key];
            }

            // If not just bind & uid but more packets available,
            // client dont need no ack to tell him he is connected
            3 < count($path) && $data['noack'] = 'yes';

            // Uid used for load balancing
            $message = new Message();
            $message->data($data + [ 'uid' => $path[1] ]);
            $list[] = $message;

            // Add regular packets
            for ($i = 2; $i < count($path); $i++) {

                $packet = new Packet();
                $packet->bytes($path[$i]);

                $message = new Package();
                $message->packet($packet);
                $message->data($data + $message->data());

                $list[] = $message;
            }

        } else {

            // Does not fit path packets protocol,
            // just provide data from query string
            $list = [ $message = new Message() ];
            $message->data([ 'path' => $path ] + $query);
        }

        return $list;
    }
}
