<?php

namespace Illusion\Net\Protocol;

use Illusion\Net\Protocol\Parser\PathPackets;

use Illusion\Net\Protocol\Serializer\JSONResp;

use Illusion\Core\HttpUtil;

class Hypertext implements Protocol {

    protected $length = -1;
    protected $buffer = '';
    protected $excess = '';

    protected $request = null;

    protected $parser;
    protected $serializer;

    protected $http;

    function __construct() {

        $this->parser = new PathPackets();
        $this->serializer = new JSONResp();

        $this->http = HttpUtil::instance();
    }

    function testBytePattern($bytes) {

        $this->request = $this->http->parseRequest($bytes);

        return !!$this->request;
    }

    function createMessages($bytes) {

        $this->buffer .= $bytes;

        if ($this->length < 0 && $this->request) {

            if (@$this->request['headers']['content-length']) {

                $this->length = +$this->request['headers']['content-length'];

            } else {

                $this->length = 0;
            }

            $this->buffer = substr($this->buffer, $this->request['length']) .'';
        }

        if (0 <= $this->length && $this->length <= strlen($this->buffer)) {

            $bytes =		substr($this->buffer, 0, $this->length) .'';
            $this->excess =	substr($this->buffer, $this->length) .'';

            $this->length = -1;
            $this->buffer = '';

            $messages = $this->parser->apply($bytes, $this->request);

            $this->request = null;

            return $messages;
        }
    }

    function getOverflow() {

        return $this->excess;
    }

    function serializeMessage($message) {

        $bytes = $this->serializer->apply($message, $this->parser->getCleanMetaData());

        return $this->http->renderResponse($bytes, 'application/javascript');
    }

    function handshake() {

    }

    function upgrade() {

    }
}
