<?php

namespace Illusion\Net\Protocol\Serializer;

interface Serializer {

    /**
     * Serialize a message into bytes
     */
    function apply($message, $meta);
}
