<?php

namespace Illusion\Net\Protocol\Serializer;

use Illusion\Net\Package;

use Illusion\Core\Util;

class JSONResp implements Serializer {

    function apply($message, $meta) {

        $data = $message->data();

        if (@$meta['format'] == 'packet' && $message instanceof Package) {

            $bytes = $message->getSignedBytes();

            $reference = @$meta['reference'];
            $reference || $reference = 'packet';
            $data = [ $reference => $bytes ];
        }

        $bytes = json_encode($data);
        $bytes = Util::prettyPrintJSON($bytes);

        $callback = @$meta['callback'];

        if ($callback) {

            $bytes = "window['$callback'] && window['$callback']($bytes)";
        }

        return $bytes;
    }
}
