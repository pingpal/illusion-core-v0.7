<?php

namespace Illusion\Net\Protocol;

use Illusion\Net\Protocol\Parser\PathPackets;

use Illusion\Net\Protocol\Serializer\JSONResp;

use Illusion\Core\HttpUtil;

class Chunked implements Protocol {

	protected $buffer = '';
	protected $excess = '';

	protected $request = null;

    protected $parser;
    protected $serializer;

    protected $http;

    function __construct() {

        $this->parser = new PathPackets();
        $this->serializer = new JSONResp();

        $this->http = HttpUtil::instance();
    }

	function testBytePattern($bytes) {

        $this->request = $this->http->parseRequest($bytes);

		return @$this->request['headers']['transfer-encoding'] == 'chunked';
	}

	function createMessages($bytes) {

		$this->buffer .= $bytes;

		$position = strpos($this->buffer, "\r\n0\r\n\r\n");

		if ($position !== false && $this->request) {

			$position = $position - $this->request['length'] + 7;
			$this->buffer = substr($this->buffer, $this->request['length']).'';

			$bytes =		substr($this->buffer, 0, $position).'';
			$this->excess =	substr($this->buffer, $position).'';

			$this->length = -1;
			$this->buffer = '';

            $messages = $this->parser->apply($bytes, $this->request);

			$this->request = null;

			return $messages;
		}
	}

	function getOverflow() {

		return $this->excess;
	}

	function serializeMessage($message) {

        $bytes = $this->serializer->apply($message, $this->parser->getCleanMetaData());

        return $this->http->renderResponse($bytes, 'application/javascript');
	}

    function handshake() {

    }

    function upgrade() {

    }
}
