<?php

namespace Illusion\Net\Protocol;

use Illusion\Net\Message;

use Illusion\Core\HttpUtil;

class GCMHTTP implements Protocol {

	protected $length = -1;
	protected $buffer = '';
	protected $excess = '';

	protected $response = null;
	protected $data = null;

    protected $http;

    function __construct() {

        $this->http = HttpUtil::instance();
    }

	function testBytePattern($bytes) {

        $this->response = $this->http->parseResponse($bytes);

        if (@$this->response['headers']['transfer-encoding'] == 'chunked') {

            $body = @$this->response['body'];
            $body = $this->http->decodeTransferEncodingChunked($body);
            $json = @json_decode($body);

            if ($json) {

                $this->data = (array) $json;
            }

            return @$this->data['multicast_id'] !== null && strtolower(@$this->response['headers']['server']) === 'gse';
        }
	}

	function createMessages($bytes) {

		$this->buffer .= $bytes;

		$position = strpos($this->buffer, "\r\n0\r\n\r\n");

		if ($position !== false && $this->response) {

			$position = $position - $this->response['length'] + 7;
			$this->buffer = substr($this->buffer, $this->response['length']).'';

			$bytes =		substr($this->buffer, 0, $position).'';
			$this->excess =	substr($this->buffer, $position).'';

			$this->length = -1;
			$this->buffer = '';

			$message = $this->parseBytes($bytes);

			$this->response = null;

			return [$message];
		}
	}

	function getOverflow() {

		return $this->excess;
	}

	function serializeMessage($message) {

		$data = $message->data();

		$device = @$data['device'];
		$apikey = @$data['apikey'];

		if (!$device || !$apikey) {

			return '';
		}

		$data = @$data['data'];

		if (!$data) {

			$data = [];
		}

		$root['data'] = $data;
		$root['to'] = $device;

		$json = json_encode($root, defined('JSON_UNESCAPED_UNICODE') ? JSON_UNESCAPED_UNICODE : 0);

		if (!defined('JSON_UNESCAPED_UNICODE') && function_exists('mb_convert_encoding')) {

			$json = preg_replace_callback('~\\\\u([0-9a-f]{4})~i', function ($match) {

				return mb_convert_encoding(pack("H*", $match[1]), "UTF-8", "UTF-16");

			}, $json);
		}

		$request[] = 'POST /gcm/send HTTP/1.1';
		$request[] = 'Host: android.googleapis.com';
		$request[] = 'Content-Type: application/json';
		$request[] = 'Authorization: key=%s';
		$request[] = 'Content-Length: %d';

		$request[] = '';
		$request[] = '%s';

		$request =	implode("\r\n", $request);

		$request = sprintf($request, $apikey, strlen($json), $json);

		return $request;
	}

	protected function parseBytes($bytes) {

		$data = is_array($this->data) ? $this->data : [];

		$data['status'] = $this->response['status'];

		$message = new Message();

        $message->data($data);

		return $message;
	}

    function handshake() {

    }

    function upgrade() {

    }
}
