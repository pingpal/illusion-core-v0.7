<?php

namespace Illusion\Net\Protocol;

use Illusion\Net\Protocol\Parser\PathPackets;

use Illusion\Net\Protocol\Serializer\JSONResp;

use Illusion\Core\HttpUtil;

use Illusion\Net\Message;

class WSHS implements Protocol {

    protected $length = -1;
    protected $buffer = '';
    protected $excess = '';

    protected $request = null;

    protected $parser;
    protected $serializer;

    protected $http;

    protected $handshake;
    protected $upgrade;

    function __construct() {

        $this->parser = new PathPackets();
        $this->serializer = new JSONResp();

        $this->http = HttpUtil::instance();
    }

    function testBytePattern($bytes) {

        $this->request = $this->http->parseRequest($bytes);

        return $this->request && @$this->request['headers']['upgrade'] == 'websocket';
    }

    function createMessages($bytes) {

        $this->buffer .= $bytes;

        if ($this->length < 0 && $this->request) {

            if (@$this->request['headers']['content-length']) {

                $this->length = +$this->request['headers']['content-length'];

            } else {

                $this->length = 0;
            }

            $this->buffer = substr($this->buffer, $this->request['length']) .'';
        }

        if (0 <= $this->length && $this->length <= strlen($this->buffer)) {

            $bytes =		substr($this->buffer, 0, $this->length) .'';
            $this->excess =	substr($this->buffer, $this->length) .'';

            $this->length = -1;
            $this->buffer = '';

            $magic = '258EAFA5-E914-47DA-95CA-C5AB0DC85B11';
            $key = $this->request['headers']['sec-websocket-key'];
            $accept = base64_encode(sha1($key . $magic, true));

            $this->request = null;

            $response[] = "HTTP/1.1 101 Switching Protocols";
            $response[] = "Upgrade: websocket";
            $response[] = "Connection: Upgrade";

            $response[] = "Sec-WebSocket-Accept: $accept";
            $response[] = '';
            $response[] = '';

            $this->handshake = implode("\r\n", $response);

            $this->upgrade = new Websocket();
        }
    }

    function getOverflow() {

        return $this->excess;
    }

    function serializeMessage($message) {

        $bytes = $this->serializer->apply($message, $this->parser->getCleanMetaData());

        return $this->http->renderResponse($bytes, 'application/javascript');
    }

    function handshake() {

        $handshake = $this->handshake;

        $this->handshake = null;

        return $handshake;
    }

    function upgrade() {

        $upgrade = $this->upgrade;

        $this->upgrade = null;

        return $upgrade;
    }
}
