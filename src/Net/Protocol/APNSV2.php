<?php

namespace Illusion\Net\Protocol;

use Illusion\Net\Message;

class APNSV2 implements Protocol {

	protected $buffer = '';
	protected $excess = '';

	function testBytePattern($bytes) {

		return @unpack('Ccommand/Cstatus/Nidentifier', $bytes)['command'] === 8;
	}

	function createMessages($bytes) {

		$this->buffer .= $bytes;

		if (6 <= strlen($this->buffer)) {

			$bytes =		substr($this->buffer, 0, 6);
			$this->excess =	substr($this->buffer, 6).'';

			$message = $this->parseBytes($bytes);

			return [$message];
		}
	}

	function getOverflow() {

		return $this->excess;
	}

	function serializeMessage($message) {

		$data = $message->data();

		$device = @$data['device'];

		if (!$device) {

			return '';
		}

		$mid = t($data, 'message/natural?digit', 0);
		$ttl = t($data, 'expire/natural?digit', 604800);

		if ($ttl) {

			$ttl = time() + $ttl;
		}

		$data = @$data['data'];

		if (!$data) {

			$data = [];
		}

		if (!@$data['aps']) {

			$data['aps'] = [];
		}

		$json = json_encode($data, defined('JSON_UNESCAPED_UNICODE') ? JSON_UNESCAPED_UNICODE : 0);

		if (!defined('JSON_UNESCAPED_UNICODE') && function_exists('mb_convert_encoding')) {

			$json = preg_replace_callback('~\\\\u([0-9a-f]{4})~i', function ($match) {

				return mb_convert_encoding(pack("H*", $match[1]), "UTF-8", "UTF-16");

			}, $json);
		}

		$json = str_replace('"aps":[]', '"aps":{}', $json);

		$length = strlen($json);

		if (256 < $length) {

			return '';
		}

		$pack = pack('CNNnH*', 1, $mid, $ttl, 32, $device);
		$pack.= pack('n', $length);
		$pack.= $json;

		return $pack;
	}

	protected function parseBytes($bytes) {

		$data = @unpack('Ccommand/Cstatus/Nidentifier', $bytes);

		$message = new Message($data);

		$message->data($data);

		return $message;
	}

	function handshake() {

	}

	function upgrade() {

	}
}
