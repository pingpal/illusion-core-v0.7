<?php

namespace Illusion\Net\Protocol;

use Illusion\Net\Package;

use Illusion\Core\Packet;

use Illusion\Core\AuthUtil;

class Headed implements Protocol {

	protected $length = -1;
	protected $buffer = '';
	protected $excess = '';

	protected $auth;

    function __construct() {

        $this->auth = AuthUtil::instance();
    }

	function testBytePattern($bytes) {

		return !!preg_match('/^0[a-p]{4}/', $bytes);
	}

	function createMessages($bytes) {

		$this->buffer .= $bytes;

		if ($this->length < 0) {

			$this->length = $this->auth->unpack(substr($this->buffer, 1, 4));
			$this->buffer = substr($this->buffer, 5).'';
		}

		if (0 <= $this->length && $this->length <= strlen($this->buffer)) {

			$bytes =		substr($this->buffer, 0, $this->length).'';
			$this->excess =	substr($this->buffer, $this->length).'';

			$this->length = -1;
			$this->buffer = '';

            if ($bytes) {

                $packet = new Packet();
                $packet->bytes($bytes);

                $message = new Package();
                $message->packet($packet);

                return [ $message ];
            }

			return [];
		}
	}

	function getOverflow() {

		return $this->excess;
	}

	function serializeMessage($message) {

        // $bytes = $message->getSignedBytes();
		$bytes = $message->bytes();

		$length = $this->auth->pack(strlen($bytes));

		return "0$length$bytes";
	}

    function handshake() {

    }

    function upgrade() {

    }
}
