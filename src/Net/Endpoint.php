<?php

namespace Illusion\Net;

use Illusion\Net\Protocol\Protocols;

use Exception;

class Endpoint {

    protected $MAX = 999;

    protected $socket;

    protected $buffer = '';
    protected $protos = [];

    protected $protocol;
    protected $hold;
    protected $keep;

    protected $zip;
    protected $sem;

    protected $ok = true;

    protected $cb = [];
    protected $q = [];

    function __construct($socket) {

        $this->socket = $socket;

        $socket->on('data', [ $this, 'read' ]);

        $socket->on('drain', function () {

            $this->drain();
        });

        $socket->on('close', function () {

            $this->cb('drop', [ $this ]);
        });

        $socket->on('error', function () {

            throw new Exception("error in react");
        });
    }

    function on($verb, $cb = null) {

        $this->cb[$verb] = is_callable($cb) ? $cb : null;
    }

    function cb($verb, $args) {

        @$this->cb[$verb] && call_user_func_array($this->cb[$verb], $args);
    }

    function id() {

        return $this->socket->id;
    }

    function socket() {

        return $this->socket;
    }

    function zip($enable = null) {

        return func_num_args() ? $this->zip = !!$enable : $this->zip;
    }

    function write($message) {

        if ($this->zip && !$this->sem) {

            return;
        }

        $protocol = $this->getProtocol();

        if (!$protocol) {

            return;
        }

        $bytes = $protocol->serializeMessage($message);

        $this->send($bytes);

        $this->ok && $this->sem = false;
    }

    function send($bytes) {

        $this->queue($bytes);

        $this->ok && $this->drain();
    }

    function queue($bytes) {

        count($this->q) < $this->MAX && $this->q[] = $bytes;
    }

    function drain() {

        for (;;) {

            $bytes = @array_splice($this->q, 0, 1)[0];

            $this->ok = !$bytes || $this->socket->write($bytes);

            if (!$bytes || !$this->ok) {

                return;
            }
        }
    }

    function setProtocol($protocol) {

        $this->protocol = $protocol;
    }

    function getProtocol() {

        return $this->protocol;
    }

    function bufferBytes($bytes) {

        $this->buffer = $bytes;
    }

    function getCleanBufferedBytes() {

        $bytes = $this->buffer;

        $this->buffer = '';

        return $bytes;
    }

    function getProtocolFromInput($bytes) {

        if ($this->keep || $this->hold) {

            return $this->protocol;
        }

        return Protocols::protocol($bytes);
    }

    function keepProtocol() {

        $this->keep = true;
    }

    function holdProtocol() {

        $this->hold = true;
    }

    function releaseProtocol() {

        $this->hold = false;
    }

    function read($bytes) {

        // Get bytes left over from last input
        $bytes = $this->getCleanBufferedBytes() . $bytes;

        while ($bytes) {

            // Get protocol from ongoing transmission or a new one from factory
            $protocol = $this->getProtocolFromInput($bytes);

            if (!$protocol) {

                // Buffer remaining bytes
                $this->bufferBytes($bytes);

            } else {

                // Set most recent protocol
                $this->setProtocol($protocol);

                try {

                    // Create messages from input
                    $messages = $protocol->createMessages($bytes) ?: [];

                } catch (Exception $e) {

                    echo "[" . $e->getMessage() . "]\n";

                    $this->socket->close();

                    break;
                }

                if ($handshake = $protocol->handshake()) {

                    $this->send($handshake);
                }

                if ($upgrade = $protocol->upgrade()) {

                    $this->setProtocol($upgrade);
                    $this->keepProtocol();
                }

                if (!$messages && !$handshake && !$upgrade) {

                    // No full messages arrived yet, store protocol
                    $this->holdProtocol();

                } else {

                    foreach ($messages as $message) {

                        $list[] = $message;
                    }

                    // Get unused bytes
                    $bytes = $protocol->getOverflow();

                    // Release protocol
                    $this->releaseProtocol();

                    continue;
                }
            }

            break;
        }

        if (@$list) {

            $this->sem = true;
        }

        foreach (@$list ?: [] as $message) {

            $this->cb('message', [ $message, $this ]);
        }
    }
}
