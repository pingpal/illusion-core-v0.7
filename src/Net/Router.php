<?php

namespace Illusion\Net;

use Illusion\Core\Util;

class Router {

    protected $message;
    protected $client;
    protected $state;

    protected $db;
    protected $hit;

    function __construct($message, $client, $state = null) {

        $this->message  = $message;
        $this->client   = $client;
        $this->state    = $state;

        $f = function ($data, $db = [], $pre = '') use (&$f) {

            if (Util::isIterable($data)) {

                foreach ($data as $key => $value) {

                    $db = $f($value, $db, ($pre ? "$pre." : '') . $key);
                }

            } else {

                $db[ trim($pre) ] = is_string($data) ? trim($data) : $data;
            }

            return $db;
        };

        $this->db = $f($message->data());
    }

    function on($predicate, $callback) {

        if (!$this->hit) {

            foreach (preg_split('/\s+AND\s+/i', $predicate) as $predicate) {

                list($key, $value) = explode('=', $predicate, 2) + ['', ''];
                $key = trim(implode('.', preg_split('/\s*\\.\s*/', $key)));

                if (!@$this->db[$key] || trim($value) && @$this->db[$key] != trim($value)) {

                    return;
                }
            }

            $this->hit = call_user_func($callback, $this->message, $this->client, $this->state) !== false;
        }
    }
}
