<?php

namespace Illusion\Net;

class Client {

    protected $endpoint;

    protected $id;

    function id($id = null) {

        return $this->id = $id ?: $this->id;
    }

    function endpoint($endpoint = null) {

        return $endpoint === null ? $this->endpoint : $this->endpoint = $endpoint;
    }
}
