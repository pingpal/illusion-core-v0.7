<?php

namespace Illusion\Net;

use Illusion\Core\Serializable;

use Illusion\Core\Util;

class Message implements Serializable {

    protected $public;
    protected $secret;
    protected $nonce;

    protected $data = null;

    function get($key = null, $def = null) {

        return $key ? isset($this->data[$key]) ? $this->data[$key] : $def : $this->data;
    }

    function set($key, $val = null) {

        is_array($key) ? $this->data = $key : $this->data[$key] = $val;
    }

    function del($key = null) {

        if ($key) unset($this->data[$key]); else $this->data = [];
    }

    function unsetData() {

        $this->data = [];
    }

    function unsetPublic() {

        $this->public = null;
    }

    function unsetNonce() {

        $this->nonce = null;
    }

    function clearSecret() {

        $this->secret = null;
    }

    function data($key = null, $val = null, $def = null) {

        if ($key === null) {

            return $this->data;

        } else if (is_string($key)) {

            if ($val === null) {

                return isset($this->data[$key]) ? $this->data[$key] : $def;

            } else {

                $this->data[$key] = $val;
            }

        } else {

            $this->data = is_array($key) ? $key : [];
        }
    }

    function pub($public = null) {

        if ($public === null) {

            return $this->public;

        } else {

            $this->public = $public;
        }
    }

    function nonce($nonce = null) {

        if ($nonce === null) {

            return $this->nonce;

        } else {

            $this->nonce = $nonce;
        }
    }

    function secret($secret = null) {

        if ($secret === null) {

            return $this->secret;

        } else {

            $this->secret = $secret;
        }
    }

    function sign($secret) {

        $this->secret($secret);
    }

    function auth($secret) {

        return null;
    }

    function serialize() {

        return Util::joinParts([ __CLASS__, $this->public, $this->secret, $this->nonce, @json_encode($this->data) ?: '' ]);
    }

    function deserialize($string) {

        $parts = Util::splitIntoParts($string);

        $this->public = @$parts[1] ?: null;
        $this->secret = @$parts[2] ?: null;
        $this->nonce =  @$parts[3] ?: null;

        $this->data = @json_decode(@$parts[4], true) ?: [];
    }
}
