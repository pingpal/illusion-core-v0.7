<?php

namespace Illusion\Net;

use Illusion\Core\Packet;

use Illusion\Core\Util;

use Exception;

class Package extends Message {

    protected $packet;

    function __construct() {

        $this->packet(new Packet());
    }

    function packet($packet = null) {

        return $packet === null ? $this->packet : $this->packet = $packet;
    }

    function get($key = null, $def = null) {

        return $this->packet->get($key, $def);
    }

    function set($key, $val = null) {

        $this->packet->set($key, $val);
    }

    function del($key = null) {

        $this->packet->del($key);
    }

    function unsetBytes() {

        $this->packet->unsetBytes();
    }

    function unsetData() {

        $this->packet->unsetData();
    }

    function unsetPublic() {

        $this->packet->unsetPublic();
    }

    function unsetNonce() {

        $this->packet->unsetNonce();
    }

    function clearSecret() {

        $this->packet->clearSecret();
    }

    function bytes($bytes = null) {

        return $this->packet->bytes($bytes);
    }

    function data($key = null, $val = null, $def = null) {

        return $this->packet->data($key, $val, $def);
    }

    function pub($public = null) {

        return $this->packet->pub($public);
    }

    function nonce($nonce = null) {

        return $this->packet->nonce($nonce);
    }

    function secret($secret = null) {

        return $this->packet->secret($secret);
    }

    function sign($secret) {

        $this->packet->sign($secret);
    }

    function auth($secret) {

        return $this->packet->auth($secret);
    }

    function getSignedBytes($secret = null) {

        return $this->packet->getSignedBytes($secret);
    }

    function serialize() {

        return Util::joinParts([ __CLASS__,  $this->packet->serialize() ]);
    }

    function deserialize($string) {

        $parts = Util::splitIntoParts($string);

        $packet = new Packet();

        $packet->deserialize(@$parts[1]);

        $this->packet($packet);
    }
}
