<?php

namespace Illusion\Access\Sql;

use Illusion\Core\Util;

use Exception;

/**
 * # table keys
 * # engine InnoDB
 *
 * # column id id
 * # column public
 * # column private
 * # column preferred boolean
 *
 * # foreign user user id odc
 */

class Keys {

    static function access($ctx, $verb, $safe, $fields, $filter) {

        if ($ctx->session()->isMemberOf([ 'super' ])) {

            return true;
        }

        if (!$safe) {

            return false;
        }

        switch ($verb) {

            case 'read':

                foreach ($fields as $field) {

                    if (!in_array($field, [ 'id', 'public', 'preferred' ])) {

                        return false;
                    }
                }

                if (t($filter, 'id/positive?digit', -2) != $ctx->session()->id()) {

                    return false;
                }

                break;

            case 'create':
            case 'update':
            case 'delete':

                return false;
        }

        return true;
    }

    // function add($grant = null) {
    //
    //     $sql = sql()->insert()->into('user');
    //
    //     $sql->set('public',		Util::key());
    //     $sql->set('private',	Util::key());
    //
    //     $grant && $sql->set('ok', true);
    //
    //     return $sql->exe();
    // }

    function createNewKeyPair($user, $preferred = null) {

        $user = t($user, 'positive?digit');

        if (!$user) {

            throw new Exception('bad args');
        }

        $public = Util::key();
        $private = Util::key();

        try {

            $act = sql()->beginTransaction();

            $sql = sql()->insert()->into('keys');

            $sql->set('user',		$user);
            $sql->set('public',		$public);
            $sql->set('private',	$private);

            if ($preferred) {

                $sql->set('preferred', true);

                sql()->delete()->from('keys')->where('user = ? AND preferred = ?', $user, true)->exe();
            }

            $sql->exe();

            $act->commitTransaction();

        } catch (Exception $e) {

            @$act && $act->rollbackTransaction();

            throw $e;
        }

        return [ $public, $private ];
    }

    function revokeKeyPair($public) {

        $public = t($public, 'alnum');

		if (!$public) {

			throw new Exception('bad args');
		}

        try {

            $act = sql()->beginTransaction();

            $row = sql()->select()->from('keys')->where('public', $public)->row();

            if (!$row) {

                throw new Exception('bad args');
            }

            if ($row['preferred']) {

                $this->createNewKeyPair($row['user'], true);
            }

            sql()->delete()->from('keys')->where('public', $public)->exe();

            $act->commitTransaction();

        } catch (Exception $e) {

            @$act && $act->rollbackTransaction();

            throw $e;
        }
    }
}
