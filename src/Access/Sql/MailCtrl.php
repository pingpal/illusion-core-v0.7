<?php

namespace Illusion\Access\Sql;

use Illusion\Core\Util;

use Illusion\Config;

/**
 * # alias mail
 * # access mail
 * # context api
 */
class MailCtrl {

    /**
     * # direct mail.signout
     */
    static function signout($args, $in, $out, $ctx) {

        (new Mail())->context($ctx)->logout();
    }

    /**
     * # direct mail.signin
     */
    static function signin($args, $in, $out, $ctx) {

        (new Mail())->context($ctx)->login(@$args['username'], @$args['password']);
    }

    /**
     * # direct mail.signup
     */
    static function signup($args, $in, $out, $ctx) {

        (new Mail())->context($ctx)->register(@$args['username'], @$args['password'], @$args['agree'], @$args['agreed'], @$args['redirect'], $out->get('ok'));
    }

    /**
     * # direct mail.reset
     */
    static function reset($args, $in, $out, $ctx) {

        (new Mail())->context($ctx)->resetPassword(@$args['username'], @$args['password'], @$args['redirect'], $out->get('ok'));
    }

    /**
     * # direct mail.change.username
     */
    static function changeUsername($args, $in, $out, $ctx) {

        (new Mail())->context($ctx)->changeUsername(@$args['username'], @$args['password'], @$args['redirect'], $out->get('ok'));
    }

    /**
     * # direct mail.change.password
     */
    static function changePassword($args, $in, $out, $ctx) {

        (new Mail())->context($ctx)->changePassword(@$args['password'], @$args['repeated'], @$args['previous']);
    }

    /**
     * # direct mail.confirm
     */
    static function confirm($args, $in, $out, $ctx) {

        (new Mail())->context($ctx)->confirm(@$args['code'], @$args['redirect']);
    }

    /**
     * # direct mail.info
     */
    static function info($args, $in, $out, $ctx) {

        $out->end((new Mail())->context($ctx)->getUserInfo());
    }

    /**
     * # direct mail.resend
     */
    static function resend($args, $in, $out, $ctx) {

        (new Mail($ctx))->context($ctx)->resendConfirmMail(@$args['redirect']);
    }
}
