<?php

namespace Illusion\Access\Sql;

use Illusion\Core\Util;

/**
 * # table nonce
 * # engine InnoDB
 *
 * # column id
 * # column nonce
 * # column hash
 */

class Nonce {

	function create() {

		return str_pad(substr(Util::ms(), 0, 13), 13, '0');
	}

	function check($nonce) {

	}

	function clean() {

	}
}
