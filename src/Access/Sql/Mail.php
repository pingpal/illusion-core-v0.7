<?php

namespace Illusion\Access\Sql;

use Illusion\Core\PacketHelp;

use Illusion\Core\Util;

use Illusion\Config;

use Swift_Message;
use Swift_SmtpTransport;
use Swift_Mailer;

use Swift_Validate;

use Exception;

/**
 * # table mail
 * # engine InnoDB
 *
 * # column id id
 * # column ok boolean
 * # column mail
 * # column hash
 * # column code
 *
 * # foreign user user id odc
 */

class Mail {

	static function access($ctx, $verb, $safe, $values, $filter) {

		return true;
	}

	protected $context;

	function context($context = null) {

		if ($context) {

			$this->context = $context;

			return $this;

		} else if ($this->context) {

			return $this->context;
		}

		throw new Exception('got no context');
	}

	protected function checkUsername($username) {

		if (!is_string($username) || 255 < strlen($username) || !Swift_Validate::email($username)) {

			throw new Exception('invalid username, must be an email address');
		}
	}

	protected function checkPassword($password) {

		if (!is_string($password) || 255 < strlen($password) || !preg_match('/^.*(?=.{8,})(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).*$/', $password)) {

			throw new Exception('invalid password, must contain lower/upper case and digits');
		}
	}

	protected function verifyUsername($username, $password = null) {

		$all = sql()->select()->from('mail')->where('mail = ?', $username)->all();

		$result = -1;

		foreach ($all as $row) {

			$ok =	t($row, 'ok/natural?digit', 0);
			$user =	t($row, 'user/natural?digit', -1);
			$hash =	t($row, 'hash/alnum');
			$code =	t($row, 'code/alnum');

			if ($ok) {

				$result = $user;

			} else if ($code) {

				if (is_int($result) && $result < 0) {

					$result = null;
				}

				if ($password) {

					if (password_verify($password, $hash) || password_verify($username.$password, $hash)) {

						return $code;
					}
				}
			}
		}

		return $result;
	}

	protected function verifyPassword($id, $password) {

		$sql = sql()->select()->from('mail');

		$sql->field('mail');
		$sql->field('hash');

		$sql->where('ok = 1 AND user = ?', $id);

		$row = $sql->row();

		if (!$row) {

			throw new Exception('bad access');
		}

		$hash = t($row, 'hash/text');
		$mail = t($row, 'mail/text');
		$pass = t($password, 'text');

		if (!password_verify($password, $hash) && !password_verify($mail.$password, $hash)) {

			throw new Exception('bad access');
		}
	}

	protected function deleteNonOkUser($mail) {

		$sql = sql()->delete()->from('user');

		$sql->where('id IN ?',		sql()->select()->from('mail')->field('user')->where('mail', $mail));
		$sql->where('id NOT IN ?',	sql()->select()->from('mail')->field('user')->where('ok = 1'));

		$sql->exe();
	}

	protected function deleteNonOkMail($mail) {

		sql()->delete()->from('mail')->where('ok = 0 AND mail = ?', $mail)->exe();
	}

	protected function deletePendingMail($user, $mail) {

		sql()->delete()->from('mail')->where('user = ? AND id <> ?', $user, $mail)->exe();
	}

	protected function insertPendingMail($id, $username, $password, $ok = null, $c = null) {

		$sql = sql()->insert()->into('mail');

		$sql->set('mail', $username);
		$sql->set('hash', password_hash($password, PASSWORD_DEFAULT));
		$sql->set('user', $id);
		$sql->set('ok', !!$ok);

		$code = null;

		if (!$ok || $c) {

			$code = Util::key();
			$sql->set('code', $code);
		}

		$sql->exe();

		return $code;
	}

	protected function addNewUser() {

		$id = (new User())->add();

		$sql = sql()->insert()->into('group');

		$sql->set('name', 'mail');
		$sql->set('user', $id);

		try {

			$sql->exe();

		} catch (Exception $e) {

		}

		return $id;
	}

	function register($username, $password, $agree = null, $agreed = null, $redirect = null, $flush = null) {

		$username =	t($username, 'print-l');
		$password =	t($password, 'print');
		$agree =	t($agree, 'text');
		$agreed =	t($agreed, 'text');
		$redirect = t($redirect, 'text');

		try {

			$act = sql()->beginTransaction();

			$this->checkUsername($username);
			$result = $this->verifyUsername($username, $password);

			if (is_int($result) && 0 < $result) {

				throw new Exception('username unavailable');
			}

			$this->checkPassword($password);

			if ($agree && !$agreed) {

				throw new Exception('You must read and agree');
			}

			if ($result === null) {

				$this->deleteNonOkUser($username);
				$this->deleteNonOkMail($username);
			}

			$id = null;

			if (!is_string($result)) {

				$id = $this->addNewUser();

				$result = $this->insertPendingMail($id, $username, $password, true, true);
			}

			$act->commitTransaction();

		} catch (Exception $e) {

			@$act && $act->rollbackTransaction();

			throw $e;
		}

		if ($id) {

			$this->context()->session()->login($id);
		}

		$opts['action']['api'] = 'mail.confirm';
		$opts['action']['mail'] = 'Illusion.Access.verify';

		$opts['api']['code'] = $result;
		$redirect && $opts['api']['redirect'] = $redirect;

		$opts['mail']['title'] = '{brand} account verification';
		$opts['mail']['address'] = $username;

		$this->send($opts);
	}

	function changeUsername($username, $password, $redirect = null, $flush = null) {

		$username =	t($username, 'print-l');
		$password =	t($password, 'print');
		$redirect = t($redirect, 'text');

		try {

			$act = sql()->beginTransaction();

			$id = $this->context()->session()->id();

			if ($id < 0) {

				throw new Exception('bad access');
			}

			$this->checkUsername($username);
			$result = $this->verifyUsername($username);

			if (is_int($result) && 0 < $result) {

				throw new Exception('username unavailable');
			}

			$this->verifyPassword($id, $password);

			if ($result === null) {

				$this->deleteNonOkUser($username);
				$this->deleteNonOkMail($username);
			}

			if (!is_string($result)) {

				$result = $this->insertPendingMail($id, $username, $password);
			}

			$act->commitTransaction();

		} catch (Exception $e) {

			@$act && $act->rollbackTransaction();

			throw $e;
		}

		$opts['action']['api'] = 'mail.confirm';
		$opts['action']['mail'] = 'Illusion.Access.verify';

		$opts['api']['code'] = $result;
		$redirect && $opts['api']['redirect'] = $redirect;

		$opts['mail']['title'] = '{brand} account verification';
		$opts['mail']['address'] = $username;

		$this->send($opts);
	}

	function resetPassword($username, $password, $redirect = null, $flush = null) {

		$username =	t($username, 'print-l');
		$password =	t($password, 'print');
		$redirect = t($redirect, 'text');

		try {

			$act = sql()->beginTransaction();

			$result = $this->verifyUsername($username, $password);

			if (!is_string($result) && $result < 0) {

				throw new Exception('username not found');
			}

			$this->checkPassword($password);

			if (!is_string($result)) {

				$this->deleteNonOkUser($username);
				$this->deleteNonOkMail($username);

				$result = $this->insertPendingMail($result, $username, $password);
			}

			$act->commitTransaction();

		} catch (Exception $e) {

			@$act && $act->rollbackTransaction();

			throw $e;
		}

		$opts['action']['api'] = 'mail.confirm';
		$opts['action']['mail'] = 'Illusion.Access.verify';

		$opts['api']['code'] = $result;
		$redirect && $opts['api']['redirect'] = $redirect;

		$opts['mail']['title'] = '{brand} account verification';
		$opts['mail']['address'] = $username;

		$this->send($opts);
	}

	function changePassword($password, $repeated, $previous) {

		$repeated =	t($repeated, 'print');
		$password =	t($password, 'print');
		$previous =	t($previous, 'print');

		try {

			$act = sql()->beginTransaction();

			$id = $this->context()->session()->id();

			$this->checkPassword($password);

			if ($password != $repeated) {

				throw new Exception('password mismatch');
			}

			$this->verifyPassword($id, $previous);

			$sql = sql()->select()->from('mail');

			$sql->field('id');
			$sql->field('mail');

			$sql->where('user', $id);

			foreach ($sql->all() as $row) {

				$sql = sql()->update()->table('mail');

				$sql->set('hash', password_hash($password, PASSWORD_DEFAULT));
				$sql->where('id', $row['id']);

				$sql->exe();
			}

			$act->commitTransaction();

		} catch (Exception $e) {

			@$act && $act->rollbackTransaction();

			throw $e;
		}
	}

	function confirm($code, $redirect = null) {

		$code =		t($code, 'alnum');
		$redirect =	t($redirect, 'text');

		if (!$code) {

			if ($redirect) {

				$this->context()->out()->redirect($redirect);

				return;
			}

			throw new Exception('bad code');
		}

		try {

			$act = sql()->beginTransaction();

			$sql = sql()->select()->from('mail');

			$sql->field('id');
			$sql->field('user');

			$sql->where('code', $code);

			$row = $sql->row();

			if (!$row) {

				if ($redirect) {

					$this->context()->out()->redirect($redirect);

					return;
				}

				throw new Exception('bad code');
			}

			$id = $row['user'];

			$sql = sql()->update()->table('mail');

			$sql->set('ok', true);
			$sql->set('code', '');

			$sql->where('code', $code);

			$sql->exe();

			$this->deletePendingMail($id, $row['id']);

			$sql = sql()->insert()->into('group');

			$sql->set('name', 'mail-activated');
			$sql->set('user', $id);

			try {

				$sql->exe();

			} catch (Exception $e) {

			}

			$act->commitTransaction();

		} catch (Exception $e) {

			@$act && $act->rollbackTransaction();

			throw $e;
		}

		$this->context()->session()->login($id);

		if ($redirect) {

			$this->context()->out()->redirect($redirect);
		}
	}

	function login($username, $password) {

		$username = t($username, 'print-l');
		$password = t($password, 'print');

		$sql = sql()->select()->from('mail');

		$sql->field('user');
		$sql->field('hash');

		$sql->where('ok = 1');
		$sql->where('mail', $username);

		$row = $sql->row();

		if (!$row || !password_verify($password, $row['hash']) && !password_verify($username.$password, $row['hash'])) {

			throw new Exception('bad login');
		}

		$this->context()->session()->login($row['user']);
	}

	function logout() {

		$this->context()->session()->logout();
	}

	function getUserInfo() {

		$id = $this->context()->session()->id();

		if ($id < 0) {

			throw new Exception('bad access');
		}

		$sql = sql()->select()->from('mail');

		$sql->field('mail');
		$sql->field('user', 'id');
		$sql->where('ok = 1 AND user = ?', $id);

		$row = $sql->row();

		if (!$row) {

			throw new Exception('bad access');
		}

		$row['groups'] = $this->context()->session()->getGroups();

		return $row;
	}

	// needs to be loged in (not good)
	function resendConfirmMail($redirect = null, $flush = null) {

		$id = $this->context()->session()->id();

		if ($id < 0) {

			throw new Exception('bad access');
		}

		$sql = sql()->select()->from('mail');

		$sql->field('mail');
		$sql->field('code');
		$sql->where('user = ?', $id);

		$row = $sql->row();

		if (!@$row['code']) {

			throw new Exception('bad access');
		}

		$opts['action']['api'] = 'mail.confirm';
		$opts['action']['mail'] = 'Illusion.Access.verify';

		$opts['api']['code'] = $row['code'];
		$redirect && $opts['api']['redirect'] = $redirect;

		$opts['mail']['title'] = '{brand} account verification';
		$opts['mail']['address'] = $row['mail'];

		$this->send($opts);
	}

	protected function send($opts) {

		$brand = Config::$BRAND;

		$replace['title']['{brand}'] = $brand;

		$opts['mail']['title'] = str_replace(array_keys($replace['title']), array_values($replace['title']), @$opts['mail']['title']);

		$packet = (new PacketHelp($this->context()->session()))->pack(@$opts['action']['api'], @$opts['api']);

		$vars = $this->context()->in()->vars();
		$url = $vars->url([ 'packet', $packet ], null, null, null, $vars->scheme(), $vars->host(), $vars->port());

		$body = @$opts['action']['mail'];

		$opts['mail']['brand']	= $brand;
		$opts['mail']['link']	= $url;

		$body = l($body, @$opts['mail']);

		$this->mail([ Config::$SMTP_ADDRESS => $brand ], [ @$opts['mail']['address'] ], $opts['mail']['title'], $body);
	}

	protected function mail($from, $to, $subject, $body) {

		$message = Swift_Message::newInstance($subject)->setFrom($from)->setTo($to)->setBody($body, 'text/html');
		$transport = Swift_SmtpTransport::newInstance(Config::$SMTP_HOST, Config::$SMTP_PORT, Config::$SMTP_PROTOCOL);

		$transport->setUsername(Config::$SMTP_USERNAME);
		$transport->setPassword(Config::$SMTP_PASSWORD);

		$mailer = Swift_Mailer::newInstance($transport);

		return $mailer->send($message);
	}
}
