<?php

namespace Illusion\Access\Sql;

use Illusion\Core\Util;

use Exception;

// Borde ta bort ok boolean, för mail-verified gruppen används.

// * # column ok boolean
// * # column public
// * # column private

/**
 * # table user
 * # engine InnoDB
 *
 * # column id id
 */

class User {

    static function access($ctx, $verb, $safe, $fields, $filter) {

        if ($ctx->session()->isMemberOf([ 'super' ])) {

            return true;
        }

        if (!$safe) {

            return false;
        }

        switch ($verb) {

            case 'read':

                foreach ($fields as $field) {

                    if (!in_array($field, [ 'id', /*'ok',*/ 'public' ])) {

                        return false;
                    }
                }

                if (t($filter, 'id/positive?digit', -2) != $ctx->session()->id()) {

                    return false;
                }

                break;

            case 'create':
            case 'update':
            case 'delete':

                return false;
        }

        return true;
    }

    function add(/*$grant = null*/) {

        // $grant && $sql->set('ok', true);

        try {

            $act = sql()->beginTransaction();

            $sql = sql()->insert()->into('user')->set('id', 0);

            $id = $sql->exe();

            (new Keys())->createNewKeyPair($id, true);

            $act->commitTransaction();

        } catch (Exception $e) {

            @$act && $act->rollbackTransaction();

            throw $e;
        }

        return $id;
    }

    function getPublicKeyFromId($id) {

        return sql()->select()->from('keys')->field('public')->where('user', $id)->one();
    }
}
