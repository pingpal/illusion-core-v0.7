<?php

namespace Illusion\Access\Sql;

use Illusion\Core\Util;

/**
 * # table session
 * # engine InnoDB
 *
 * # column id id
 * # column sesid
 *
 * # foreign user user id odc
 */

class Session {

    static function access($ctx, $verb, $safe, $fields, $filter) {

        if ($ctx->session()->isMemberOf([ 'super' ])) {

            return true;
        }

        return null;
    }

    protected $context;

    function __construct($context) {

        $this->context = $context;
    }

    function add($grant = null) {

        $sql = sql()->insert()->into('user');

        $sql->set('public',		Util::key());
        $sql->set('private',	Util::key());

        $grant && $sql->set('ok', true);

        $sql->exe();

        return $sql->db()->lastInsertId();
    }
}
