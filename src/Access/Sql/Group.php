<?php

namespace Illusion\Access\Sql;

/**
 * # table group
 * # engine InnoDB
 *
 * # column id id
 * # column name
 *
 * # foreign user user id odc
 *
 * # custom unique(name, user)
 */

class Group {

    static function access($ctx, $verb, $safe, $filter, $fields) {

        if ($ctx->session()->isMemberOf([ 'super' ])) {

            return true;
        }

        return null;
    }
}
