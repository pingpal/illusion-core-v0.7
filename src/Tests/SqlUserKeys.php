<?php

namespace Illusion\Tests;

use Illusion\Access\Sql\User;

use Illusion\Access\Sql\Keys;

use Exception;

/**
 * # name Illusion SQL User keys
 *
 * # alias tests.illusion.sql-user-keys
 * # access dev
 * # context cli
 */
class SqlUserKeys extends BaseTest {

    /**
     * # alias run
     * # direct tests.illusion.sql-user-keys
     */
    static function _run($args, $in, $out, $ctx) {

        (new self)->run($ctx);
    }

    /**
     * # name user add
     */
    function test_userAdd() {

        $numberOfUsers = count(sql()->select()->from('user')->all());
        $numberOfKeys = count(sql()->select()->from('keys')->all());

        (new User())->add();

        if ($numberOfUsers == count(sql()->select()->from('user')->all())) {

            return 'unable to add user';
        }

        if ($numberOfKeys == count(sql()->select()->from('keys')->all())) {

            return 'no new keys created';
        }

        return true;
    }

    /**
     * # name create new key pair
     */
    function test_createNewKeyPair() {

        $user = new User();
        $keys = new Keys();

        $id = $user->add();

        $public = $keys->createNewKeyPair($id)[0];

        if (!$public || !sql()->select()->from('keys')->where('keys.public', $public)->all()) {

            return 'unable to crate key pair';
        }

        return true;
    }

    /**
     * # name create new preffered key pair
     */
    function test_createNewKeyPair_Preffered() {

        $user = new User();
        $keys = new Keys();

        $id = $user->add();

        $oldPreferred = sql()->select()->from('keys')->field('public')->where('user.id = ? AND keys.preferred = ?', $id, true)->one();

        $newPreffered = $keys->createNewKeyPair($id, true)[0];

        $newPrefferedCtrl = sql()->select()->from('keys')->field('public')->where('user.id = ? AND keys.preferred = ?', $id, true)->one();

        if ($newPreffered != $newPrefferedCtrl) {

            var_dump($newPreffered, $newPrefferedCtrl);

            return 'the returned new preferred did not match the one from the db';
        }

        if ($oldPreferred == $newPrefferedCtrl) {

            return 'the preferred keys was the same after create new key pair preferred';
        }

        return true;
    }

    /**
     * # name create new key pair - intact preffered
     */
    function test_createNewKeyPair_intactPreffered() {

        $user = new User();
        $keys = new Keys();

        $id = $user->add();

        $preferred = sql()->select()->from('keys')->field('public')->where('user.id = ? AND keys.preferred = ?', $id, true)->one();

        $keys->createNewKeyPair($id);

        if ($preferred != sql()->select()->from('keys')->field('public')->where('user.id = ? AND keys.preferred = ?', $id, true)->one()) {

            return 'the preferred keys was not the same after create new key pair';
        }

        return true;
    }

    /**
     * # name revoke key
     */
    function test_revokeKeyPair() {

        $user = new User();
        $keys = new Keys();

        $id = $user->add();

        $public = $keys->createNewKeyPair($id)[0];

        $keys->revokeKeyPair($public);

        if (sql()->select()->from('keys')->where('keys.public', $public)->all()) {

            return 'key is this there after revoke';
        }

        return true;
    }

    /**
     * # name revoke preferred key
     */
    function test_revokeKeyPair_Preffered() {

        $user = new User();
        $keys = new Keys();

        $id = $user->add();

        $preferred = sql()->select()->from('keys')->field('public')->where('user.id = ? AND keys.preferred = ?', $id, true)->one();

        $keys->revokeKeyPair($preferred);

        if (!sql()->select()->from('keys')->field('public')->where('user.id = ? AND keys.preferred = ?', $id, true)->one()) {

            return 'no new preferred key was found';
        }

        return true;
    }

    /**
     * # name get secrets from field
     */
    function test_getSecretsFromField($ctx) {

        $user = new User();
        $id = $user->add();

        try {

            $secrets = $this->getSecretsFromField('id', $id);

            if (!$secrets['public']) {

                throw new Exception();
            }

        } catch (Exception $e) {

            return 'fail to retrive preferred secrets form id from newly created user';
        }

        $public = $secrets['public'];

        try {

            $secrets = $this->getSecretsFromField('public', $public);

            if (!$secrets['public']) {

                throw new Exception();
            }

        } catch (Exception $e) {

            return 'fail to retrive preferred secrets form public from newly created user';
        }

        return true;
    }

    private function getSecretsFromField($field, $value) {

        $sql = sql()->select()->from('user');

        $sql->leftJoin('group:');
        $sql->leftJoin('keys:');

        $sql->field('user.id');
        $sql->field('keys.public');
        $sql->field('keys.private');
        $sql->field('group:name');

        if ($field == 'id') {

            $sql->where('user.id', $value);
            $sql->where('keys.preferred', true);

        } else {

            $sql->where('keys.public', $value);
        }

        // $sql->where("user.$field = ?", $value);

        $all = $sql->all();

        if (!$all) {

            throw new Exception('bad access');
        }

        $data['id']         = t($all, '0.id/natural?digit', -1);
        $data['public']     = t($all, '0.public/text');
        $data['private']    = t($all, '0.private/text');

        $data['groups'] = [];

        foreach ($all as $row) {

            $data['groups'][] = @$row['name'];
        }

        $data['groups'] = t($data, 'groups/text-rn');

        return $data;
    }
}
