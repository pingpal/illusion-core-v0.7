<?php

namespace Illusion\Tests;

/**
 * Queue: exponential uniform single server
 *
 * # alias sims.queue.mu1
 * # access dev
 * # context cli
 */
class QueueMU1 {

    function rnd() {

        return mt_rand(0, PHP_INT_MAX) / PHP_INT_MAX;
    }

    function exprnd($lambda = 1, $tau = 1) {

        return - log(1 - (1 - exp(- $lambda * $tau)) * $this->rnd()) / $lambda;
    }

    /**
     * # alias run
     * # direct sims.queue.mu1
     */
    static function _f($args, $in, $out, $ctx) {

        $result = (new self)->f();

        $box = [ "<bold>Result</bold>", '' ];
        $row = $out->tty()->format($result);
        $box = array_merge($box, explode("\n", $row));
        $box = $out->tty()->box($box);

        $out->end($box);
    }

    function f() {

        $ARRIVAL = 0;
        $COMPLETION = 1;
        $SAMPLE = 2;

        $lambda = 7;        // arrival rate
        $mu = 10;           // service rate
        $endtime = 1000;    // simulation length
        $t = 0;             // current time
        $tstep = 1;         // time between consecutive sample events
        $currcustomers = 0; // current nbr of customers in system

        $event = [0, 0, 0]; // 1. time for next arrival
                            // 2. next service completion
                            // 3. next sample event

        $event[$ARRIVAL] = $this->exprnd($lambda); // next arrival
        //$event[$ARRIVAL] = 0.14; // next arrival (completely deterministic)

        $event[$COMPLETION] =   PHP_INT_MAX;    // no customer in system
        $event[$SAMPLE] =       $tstep;         // next sample event

        $nbrarrived = 0; // nbr of cust that have arrived throughout the simulations
        $arrivedtime = [];

        $T = []; // vector with the job response times
        $N = []; // vector with nbr of customers in system at the sample events

        while ($t < $endtime) {

            $t = PHP_INT_MAX;
            $nextevent = -1;

            // move to the time of the next event and find type of nextevent
            foreach ($event as $key => $time) {

                if ($time < $t) {

                    $t = $time;
                    $nextevent = $key;
                }
            }

            if ($nextevent == $ARRIVAL) { // new arrival

                $event[$ARRIVAL] = $this->exprnd($lambda) + $t; // next arrival
                //$event[$ARRIVAL] = 0.14 + $t; // next arrival (completely deterministic)

                if (!$currcustomers) { // customer arrived to empty system

                    $event[$COMPLETION] = ($this->rnd() - 0.5) / 10 + 1 / $mu + $t; // new customer in server
                    //$event[$COMPLETION] = (1 / $mu) + $t; // new customer in server (deterministic)
                }

                $arrivedtime[$nbrarrived++] = $t; // the new customer arrived at time t
                $currcustomers++;

            } else if ($nextevent == $COMPLETION) { // service completion

                $T[] = $t - $arrivedtime[$nbrarrived - $currcustomers]; // time in system
                $currcustomers--;

                if ($currcustomers) {

                    $event[$COMPLETION] = ($this->rnd() - 0.5) / 10 + 1 / $mu + $t; // new customer in server
                    //$event[$COMPLETION] = (1 / $mu) + $t; // new customer in server (deterministic)

                } else {

                    $event[$COMPLETION] = PHP_INT_MAX; // empty system
                }

            } else { // sample event

                $N[] = $currcustomers;
                $event[$SAMPLE] = $event[$SAMPLE] + exp(1 / $tstep); // set time for new sample event
            }
        }

        return [ 'mean response times' => array_sum($T) / count($T), 'mean customers in system' => array_sum($N) / count($N) ];
    }
}
