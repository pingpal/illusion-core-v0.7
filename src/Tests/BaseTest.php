<?php

namespace Illusion\Tests;

use Illusion\Core\Util;

class BaseTest {

    function run($ctx) {

        $out = $ctx->out();
        $class = get_class($this);

        $fails = [];
        $doc = Util::getDocComments($class);

        $this->setup();

        foreach (get_class_methods($this) as $method) {

            if (Util::startsWith($method, 'test_')) {

                $name = @$doc['method'][$method]['name'][0];
                $result = call_user_func([ $this, $method ], $ctx);
                is_string($result) && $fails[$name] = $result;

                $result = $result === null ? '<yellow>todo</yellow>' : (is_string($result) ? '<red>fail</red>' : '<green>ok</green>');

                $row[] = [ 'test' => $name, 'result' => $result ];
            }
        }

        $this->teardown();

        $name = @$doc['class'][$class]['name'][0];
        $box = [ "<bold>$name</bold>", '' ];

        $row = $out->tty()->format(@$row);
        $box = array_merge($box, explode("\n", $row));

        $out->put($out->tty()->box($box));

        foreach ($fails as $name => $result) {

            $box = [ "<bold>$name</bold>", '' ];
            $box[] = "<red>$result</red>";

            $out->put($out->tty()->box($box));
        }
    }

    function setup() {

    }

    function teardown() {

    }
}
