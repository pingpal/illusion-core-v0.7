<?php

namespace Illusion\Tests;

use Illusion\Core\Launch;

use Illusion\Core\Util;

use Illusion\Config;

use Exception;

/**
 * # access dev
 * # context cli
 */
class All {

    /**
     * # direct tests.all
     */
    static function run($args, $in, $out, $ctx) {

        $tests = [];

        $aliases = Launch::instance()->getAnnotatedActions($ctx);

        foreach ($aliases as $alias => $meta) {

            if ($alias != 'tests.all' && Util::startsWith($alias, 'tests.') && !in_array($meta['callable'], $tests)) {

                $tests[] = $meta['callable'];
            }
        }

        foreach ($tests as $callable) {

            try {

                l($callable, $args, $ctx);

            } catch (Exception $e) {

                $out->put('<red>' . $e->getMessage() . '</red>' . (Config::$DEBUG ? ' <white>in ' . $e->getFile() . ' on '. $e->getLine() . '</white>' : ''));

                //$out->end($e->getTraceAsString());
            }
        }
    }
}
