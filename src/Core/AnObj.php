<?php

namespace Illusion\Core;

class AnObj {

    function __call($name, $args) {

        return is_callable(@$this->$name) ? call_user_func_array(@$this->$name, $args) : null;
    }
}
