<?php

namespace Illusion\Core;

use Illusion\Core\Util;

class Tweak {

    function LIB() {

        return (object) [

            'IS' => [

                'int' => function ($val) {

                    return is_int($val);
                },
                'string' => function ($val) {

                    return is_string($val);
                },
                'float' => function ($val) {

                    return is_float($val);
                },
            ],

            'TO' => [

                'int' => function ($val) {

                    return is_scalar($val) ? (int) $val : null;
                },
                'string' => function ($val) {

                    return is_scalar($val) ? (string) $val : null;
                },
                'float' => function ($val) {

                    return is_scalar($val) ? (float) $val : null;
                },
            ],

            'TWEAK' => [

                'any:any' => function ($val) {

                    return $val;
                },
                'number:int|float' => function ($val) {

                    return $val;
                },
                'natural:int|float' => function ($val) {

                    if (-1 < $val) {

                        return $val;
                    }
                },
                'negative:int|float' => function ($val) {

                    if ($val < 0) {

                        return $val;
                    }
                },
                'positive:int|float' => function ($val) {

                    if (0 < $val) {

                        return $val;
                    }
                },
                'text:string' => function ($val, $flag) {

                    if ($flag('e') || strlen(trim($val))) {

                        return $val;
                    }
                },
                'digit:string' => function ($val, $flag) {

                    if ($flag('e') && !strlen(trim($val))) {

                        return $val;

                    } else if (ctype_digit(trim($val))) {

                        return $val;
                    }
                },
                'alnum:string' => function ($val, $flag) {

                    if ($flag('e') && !strlen(trim($val))) {

                        return $val;

                    } else if (ctype_alnum(trim($val))) {

                        return $val;
                    }
                },
                'exotic:string' => function ($val, $flag) {

                    $allow = '\pL\pN';

                    if ($flag('w')) {

                        $allow .= "\x20";
                    }

                    $val = Util::cleanUnicodeString($val);

                    if ($flag('e') && !strlen(trim($val))) {

                        return $val;

                    } else if (preg_match("/^[$allow]+$/u", trim($val))) {

                        return $val;
                    }
                },
                'label:string' => function ($val, $flag) {

                    $allow = '-_\pL\pN';

                    if ($flag('w')) {

                        $allow .= "\x20";
                    }

                    $val = Util::flattenWhitespaces($val);
                    $val = Util::cleanUnicodeString($val);

                    if ($flag('e') && !strlen(trim($val))) {

                        return $val;

                    } else if (preg_match("/^[$allow]+$/u", trim($val))) {

                        return $val;
                    }
                },
                'print:string' => function ($val, $flag) {

                    $val = Util::cleanUnicodeString($val);

                    if ($flag('e') || strlen(trim($val))) {

                        return $val;
                    }
                },
                'line:string' => function ($val, $flag) {

                    $val = Util::flattenWhitespaces($val);
                    $val = Util::cleanUnicodeString($val);

                    if ($flag('e') || strlen(trim($val))) {

                        return $val;
                    }
                },
                'lines:string' => function ($val, $flag) {

                    $val = Util::normalizeNewlines($val);
                    $val = Util::cleanUnicodeString($val);

                    if ($flag('e') || strlen(trim($val))) {

                        return $val;
                    }
                },
            ],

            'FLAGS' => [

                'm:not' => function ($val, $flag) {

                    if (is_string($val)) {

                        $val = trim($val);
                    }

                    return $val;
                },
                'd:not' => function ($val, $flag, $type) {

                    if (!$type('float') && is_float($val)) {

                        $val = (int) $val;
                    }

                    return $val;
                },
                'l' => function ($val, $flag, $type) {

                    if (is_string($val)) {

                        $val = strtolower($val);
                    }

                    return $val;
                },
                'u' => function ($val, $flag, $type) {

                    if (is_string($val)) {

                        $val = strtoupper($val);
                    }

                    return $val;
                },
                'c' => function ($val, $flag, $type) {

                    if (is_string($val)) {

                        $val = Util::capitalize($val);
                    }

                    return $val;
                },
                's' => function ($val, $flag, $type) {

                    if (is_string($val)) {

                        $val = Util::capitalizeSentence($val);
                    }

                    return $val;
                }
            ],
        ];
    }

    static function factory() {

        return function ($val = null, $uri = null, $def = null) {

            return (new Tweak())->apply($val, $uri, $def);
        };
    }

    static function _($val = null, $uri = null, $def = null) {

        return (new Tweak())->apply($val, $uri, $def);
    }

    protected $value    = null;
    protected $default  = null;

    protected $scalar = true;
    protected $should = true;

    protected $from = [];

    public $res = null;

    function __construct($val = null, $uri = null, $def = null) {

        if (func_num_args()) {

            $this->apply($val, $uri, $def);
        }
    }

    protected function force() {

        if ($this->should != $this->scalar) {

            $this->value = $this->should ? [null] : [];
        }

        return $this;
    }

    protected function tweak($arg, $val, $flags = null, $to = false) {

        if ($val === null) {

            return null;
        }

        $LIB = $this->LIB();

        list($kind, $type) = preg_split('/[=:]/', $arg) + ['', ''];

        foreach ($LIB->TWEAK as $key => $TWEAK) {

            list($_kind, $_types) = explode(':', $key);

            $_types = explode('|', $_types);

            if ($kind == $_kind && (!$type || in_array($type, $_types))) {

                $types = $type ? [$type] : $_types;
                $type = $types[0];

                $f = function ($f) use ($flags) {

                    return strpos($flags, $f) !== false;
                };

                $t = function ($t) use ($type) {

                    return $t == $type;
                };

                if ($to) {

                    $val = $LIB->TO[$type]($val);
                }

                foreach ($types as $type) {

                    if ($type == 'any' || $LIB->IS[$type]($val)) {

                        $val = $TWEAK($val, $f, $t);

                        if ($val !== null) {

                            foreach ($LIB->FLAGS as $key => $FLAG) {

                                list($_flag, $_mod) = explode(':', $key) + ['', ''];

                                if (strpos($flags, $_flag) !== false xor $_mod == 'not') {

                                    $val = $FLAG($val, $f, $t);
                                }
                            }

                            return $val;
                        }
                    }
                }
            }
        }
    }

    function set($val, $path = null, $flags = null) {

        preg_match_all('/(?:!.|[^!.]++)+/', $path, $matches);

        foreach ($matches[0] as $n => $key) {

            $key = preg_replace('/((!!)*)!(?=\\.)/', '$1', $key);
            $key = preg_replace('/!!/', '!', $key);

            if (is_array($val) || is_object($val)) {

                $array = (array) $val;
                $val = @$array[$key];

                if (strpos($flags, 'i') !== false) {

                    foreach ($array as $k => $v) {

                        if (strtolower($key) == strtolower($k)) {

                            $val = $v;
                        }
                    }
                }

            } else {

                $val = null;
            }
        }

        if (is_array($val) || is_object($val)) {

            $this->scalar = false;

            $this->value = (array) $val;

        } else {

            $this->value = [ $val ];
        }

        return $this;
    }

    function get($arg, $def = null) {

        if (func_num_args() != 2) {

            $def = $this->default;
        }

        list($arg, $flags) = preg_split('/[#-]/', $arg) + ['', ''];

        if (strpos($flags, 'r') !== false) {

            $this->row();
        }

        $this->force();

        $args = preg_split('/[+]/', $arg);

        $alone = count($args) == 1;

        $value = [];

        $flatten = strpos($flags, 'f') !== false;
        $nonulls = strpos($flags, 'n') !== false;

        foreach ($args as $arg) {

            list($arg, $f) = preg_split('/[~]/', $arg) + ['', ''];

            $args = preg_split('/[?&|]/', $arg);

            $single = count($args) == 1;

            if ($alone && $single && $this->from) {

                $single = false;

                $args = array_merge($args, $this->from);
            }

            foreach ($this->value as $key => $val) {

                if ($flatten && is_string($key)) {

                    $key = strtolower($key);
                }

                if (@$value[$key] === null && ($this->should || !$nonulls)) {

                    $value[$key] = $def;
                }

                $first = $this->tweak($args[0], $val, $f . $flags, !$single);

                if ($single) {

                    if ($first !== null) {

                        $value[$key] = $first;
                    }

                } else {

                    foreach ($args as $i => $arg) {

                        $second = $this->tweak($arg, $val, $f . $flags);

                        if ($second !== null) {

                            if ($i) {

                                if ($first !== null) {

                                     $value[$key] = $first;
                                }

                            } else {

                                $value[$key] = $second;
                            }

                            break;
                        }
                    }
                }
            }
        }

        $this->res = $this->should ? $value[0] : $value;

        return $this->res;
    }

    function apply($val, $uri = null, $def = null) {

        if (func_num_args() != 3) {

            $def = $this->default;
        }

        $flags = '';

        if (preg_match('~^([a-z]+)://~i', $uri, $matches)) {

            $uri = substr($uri, strlen($matches[0]));

            switch ($matches[1]) {

                case 'row' :

                    $flags .= 'r';

                    break;
            }
        }

        preg_match('~(?:!.|[^!/]++)*~', $uri, $matches);

        $path = $matches[0];

        $rest = substr($uri, strlen($path));

        $path = preg_replace('~((!!)*)!(?=/)~', '$1', $path);
        $path = preg_replace('/!!/', '!', $path);

        if (!$rest && $rest != '/') {

            $rest = $path;
            $path = '';

        } else {

            $rest = ltrim($rest, '/');
        }

        list($rest, $f) = preg_split('/[#-]/', $rest) + ['', ''];

        $flags .= $f;

        $this->set($val, $path, $flags);

        if ($rest) {

            return $this->get($rest . '-' . $flags, $def);
        }

        return $this;
    }

    function val($val = null, $path = null) {

        $this->should = true;

        if (func_num_args()) {

            $this->set($val, $path);
        }

        return $this;
    }

    function row($val = null, $path = null) {

        $this->should = false;

        if (func_num_args()) {

            $this->set($val, $path);
        }

        return $this;
    }

    function from($arg) {

        $this->from = func_num_args() == 1 ? is_array($arg) ? $arg : preg_split('/[?&|]/', $arg) : func_get_args();
    }

    function to($arg, $def = null) {

        if (func_num_args() != 2) {

            $def = $this->default;
        }

        return $this->get($arg, $def);
    }

    function def($val) {

        $this->default = $val;

        return $this;
    }
}
