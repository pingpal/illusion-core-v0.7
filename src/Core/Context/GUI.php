<?php

namespace Illusion\Core\Context;

use Illusion\Session\Backend\Backends;

use Illusion\Session\Store\Stores;

use Illusion\Session\Session;

use Illusion\Core\IO\Request;

use Illusion\Core\IO\HTMLResp;

use Illusion\Config;

use Exception;

class GUI extends Context {

    protected $vars;

    protected $in;
    protected $out;

    protected $store;
    protected $backend;
    protected $session;

    function __construct($vars) {

        $this->vars = $vars;

        $this->in = new Request($vars);
        $this->out = new HTMLResp();
    }

    function id() {

        return 'gui';
    }

    function vars() {

        return $this->vars;
    }

    function in() {

        return $this->in;
    }

    function out() {

        return $this->out;
    }

    function on($verb, $function) {

        $this->out->on($verb, $function);

        return $this;
    }

    function store($store = null) {

        return $this->store = $store ?: $this->store ?: Stores::getDefault($this->in, $this->out);
    }

    function backend($backend = null) {

        return $this->backend = $backend ?: $this->backend ?: Backends::getDefault();
    }

    function session($session = null) {

        return $this->session = $session ?: $this->session ?: new Session($this->store(), $this->backend());
    }

    function escape(Exception $e) {

        echo $e->getMessage() . (Config::$DEBUG ? ' in ' . $e->getFile() . ' on '. $e->getLine() : '') . "\n";

        return true;
    }
}
