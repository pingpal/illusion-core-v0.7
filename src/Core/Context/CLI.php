<?php

namespace Illusion\Core\Context;

use Illusion\Session\Store\CLI as CLISessionStore;

use Illusion\Session\Backend\Backends;

use Illusion\Session\Session;

use Illusion\Core\IO\SysOut;

use Illusion\Core\IO\SysIn;

use Illusion\Config;

use Exception;

class CLI extends Context {

    protected $vars;

    protected $in;
    protected $out;

    protected $store;
    protected $backend;
    protected $session;

    function __construct($vars) {

        $this->vars = $vars;

        $this->out = new SysOut();
        $this->in = new SysIn($vars, $this->out);
    }

    function id() {

        return 'cli';
    }

    function vars() {

        return $this->vars;
    }

    function in() {

        return $this->in;
    }

    function out() {

        return $this->out;
    }

    function store($store = null) {

        return $this->store = $store ?: $this->store ?: new CLISessionStore();
    }

    function backend($backend = null) {

        return $this->backend = $backend ?: $this->backend ?: Backends::getDefault();
    }

    function session($session = null) {

        return $this->session = $session ?: $this->session ?: new Session($this->store(), $this->backend());
    }

    function escape(Exception $e) {

        register_shutdown_function(function () { exit(1); });

        $this->out->put('<red>' . $e->getMessage() . '</red>' . (Config::$DEBUG ? ' <white>in ' . $e->getFile() . ' on '. $e->getLine() . '</white>' : ''));

        //$this->out->put($e->getTraceAsString());

        return true;
    }

    //-- experimental

    function onProcessExit($callback) {

        register_shutdown_function($callback);
    }
}
