<?php

namespace Illusion\Core;

use Exception;

class PacketHelp {

    protected $session;
    protected $auth;

    protected $packet;

    protected $public;
    protected $nonce;
    protected $data;

    protected $action;

    protected $ok = false;

    protected $hasAddedDataToCtxVars;
    protected $hasAuthenticatedPacket;

    function __construct($session, $packet = null) {

        $this->session = $session;
        $this->auth = AuthUtil::instance();

        $this->packet($packet);
    }

    function ok() {

        return $this->ok;
    }

    function data() {

        return $this->data;
    }

    /**
     * Set and decode packet bytes
     */
    function packet($packet = null) {

        if (func_num_args()) {

            $this->packet = $packet;

            $this->public = $this->auth->peekPublic($packet);
            $this->nonce =  $this->auth->peekNonce($packet);
            $this->data =   $this->auth->decode($packet);

            $this->action = t($this->data, 'action/text');
        }

        return $this->packet;
    }

    /**
     * Authenticate currently set packet credentials using provided session
     */
    function auth() {

        $this->hasAuthenticatedPacket = true;

        $this->ok = false;

        if ($this->public) {

            $secrets = $this->session->getSecretsFromPublic($this->public);

            $this->ok = $this->auth->auth($this->packet, $this->public, $secrets['token']);

            if (!$this->ok) {

                $this->ok = $this->auth->auth($this->packet, $this->public, $secrets['private']);
            }
        }

        return $this->ok;
    }

    /**
     * Authenticate and login currently set packet credentials using provided session
     */
    function login($save = null) {

        $this->hasAuthenticatedPacket = true;

        $this->ok = false;

        if ($this->public) {

            $this->session->backend()->checkNonce($this->nonce);

            $secrets = $this->session->getSecretsFromPublic($this->public);

            $secret = @$secrets['token'];

            $this->ok = $this->auth->auth($this->packet, $this->public, $secret);

            if (!$this->ok) {

                $secret = @$secrets['private'];

                $this->ok = $this->auth->auth($this->packet, $this->public, $secret);
            }

            if ($this->ok) {

                $id     = $secrets['id'];
                $public = $secrets['public'];
                $groups = $secrets['groups'];

                $this->session->login($id, $public, $secret, $groups, $save);
            }
        }

        return $this->ok;
    }

    /**
     * Launch action using authenticated credentials and verified data.
     * BOTH signed and non signed packets will be launched. //!!
     *
     * Only safe packet data which has integity is used to launch the action,
     * but it's also added to the context vars which originate from other sources.
     * This is so that components can utilize it for e.g. variants of output formats,
     * but context vars must always be concidererd unsafe to depend on.
     */
    function launch($context) {

        if (!$this->hasAuthenticatedPacket) {

            $this->auth();
        }

//        if ($this->ok) { //!!

            if (!$this->hasAddedDataToCtxVars) {

                $this->hasAddedDataToCtxVars = true;

                $context->vars()->more($this->data);
            }

            return l($this->action, $this->data, $context);
//        }
    }

    /**
     * Sign packet bytes with currently set session secrets
     */
    function sign($packet) {

        $public =	$this->session->getPublicKey();
        $private =	$this->session->getPrivateKey();

        $nonce = $this->session->backend()->newNonce();

        if ($public && $private) {

            $packet = $this->auth->sign($packet, $public, $private, $nonce);
        }

        return $packet;
    }

    /**
     * Pack action and/or data with optional signature into packet bytes
     *
     * The signature will be constructed from currently set session secrets
     */
    function pack($action = null, $data = null, $sign = null) {

        is_array($data) || $data = [];
        $action && $data['action'] = $action;

        $packet = $this->auth->encode($data);
        $sign && $packet = $this->sign($packet);

        return $packet;
    }

    /**
     * Init packet object using action and/or data, returning packet bytes
     */
    function init($action = null, $data = null, $sign = null) {

        $packet = $this->pack($action, $data, $sign);

        return $this->packet($packet);
    }
}
