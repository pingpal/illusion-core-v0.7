<?php

namespace Illusion\Core\Config;

// maybe ConfigImpl
// maybe AbstractConfigImpl
class AbstractConfig {


    /**
     * Gets a config value from (by) default method (prob entry).
     */
    function get($key) {

    }


    /**
     * Gets a config value from (by) scheme entry key.
     */
    function getValueByEntryKey($key) {

    }

    /**
     * Gets a config value from (by) scheme entry.
     */
    function getValueByEntry($key) {

    }
}
