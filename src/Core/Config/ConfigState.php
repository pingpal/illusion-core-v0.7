<?php

namespace Illusion\Core\Config;

class ConfigState {

    protected $array = [];

    /**
     * Sets a value to the state array.
     */
    function set($key, $val) {

    }

    /**
     * Gets a value from the state array.
     *
     * Returns a value.
     */
    function get($key) {

    }

    /**
     * Sets the internal state array.
     *
     * Returns a value.
     */
    function setStateMap() {

    }

    /**
     * Gets the internal state array.
     *
     * Returns the state array.
     */
    function getStateMap() {

    }
}
