<?php

namespace Illusion\Core\Config;

class ConfigScheme {

    static function get() {

        return [

            'categories' => [
                'misc' => [
                    'title' => '',
                    'entries' => [
                        'debug',
                        'install',
                        'brand',
                        'timezone',
                        'command',
                        'executable',
                        'writable',
                        'packet-route',
                        'secure-key',
                        'environments',
                    ],
                ],
                'mysql' => [
                    'title' => 'MySQL',
                    'entries' => [
                        'mysql-server',
                        'mysql-database',
                        'mysql-username',
                        'mysql-password',
                    ],
                ],
                'smtp' => [
                    'title' => 'SMTP',
                    'entries' => [
                        'smtp-address',
                        'smtp-username',
                        'smtp-password',
                        'smtp-protocol',
                        'smtp-host',
                        'smtp-port',
                    ],
                ],
                'cache' => [
                    'title' => 'Cache',
                    'entries' => [
                        'cache-ttl',
                        'cache-chance',
                    ],
                ],
            ],

            'entries' => [
                'debug' => [
                    'title' =>	'Debug',
                    'desc' =>	'Should be off in production environments',
                    'key' =>	'DEBUG',
                    'type' =>	'boolean',
                ],
                'install' => [
                    'title' =>	'Install',
                    'key' =>	'INSTALL',
                    'type' =>	'boolean',
                ],
                'brand' => [
                    'title' =>	'Brand',
                    'key' =>	'BRAND',
                    'type' =>	'string',
                ],
                'timezone' => [
                    'title' =>	'Timezone',
                    'key' =>	'TIMEZONE',
                    'type' =>	'string',
                ],
                'command' => [
                    'title' =>	'Command',
                    'key' =>	'COMMAND',
                    'type' =>	'string',
                ],
                'executable' => [
                    'title' =>	'Executable',
                    'key' =>	'PATH_EXE',
                    'type' =>	'string',
                ],
                'writable' => [
                    'title' =>	'Writable Class Path',
                    'desc' =>	'Relative to vendor/%VENDOR%/',
                    'key' =>	'_PATH_WRITABLE',
                    'type' =>	'string',
                ],
                'packet-route' => [
                    'title' =>	'Packet Route',
                    'key' =>	'ROUTE_PACKET',
                    'type' =>	'string',
                ],
                'secure-key' => [
                    'title' =>	'Secure Store Key',
                    'key' =>	'SECURE_STORE_KEY',
                    'type' =>	'string',
                ],
                'environments' => [
                    'title' =>	'Environments',
                    'key' =>	'ENVIRONMENTS',
                    'type' =>	'string',
                ],
                'cache-ttl' => [
                    'title' =>	'Time To Live',
                    'key' =>	'CACHE_DEFAULT_TTL',
                    'type' =>	'integer',
                ],
                'cache-chance' => [
                    'title' =>	'Clean Chance',
                    'key' =>	'CACHE_CLEAN_CHANCE',
                    'type' =>	'double|float|integer',
                ],
                'mysql-server' => [
                    'title' =>	'Server',
                    'key' =>	'MYSQL_SERVER',
                    'type' =>	'string',
                ],
                'mysql-database' => [
                    'title' =>	'Database',
                    'key' =>	'MYSQL_DATABASE',
                    'type' =>	'string',
                ],
                'mysql-username' => [
                    'title' =>	'Username',
                    'key' =>	'MYSQL_USERNAME',
                    'type' =>	'string',
                ],
                'mysql-password' => [
                    'title' =>	'Password',
                    'key' =>	'MYSQL_PASSWORD',
                    'type' =>	'string',
                ],
                'smtp-address' => [
                    'title' =>	'Address',
                    'key' =>	'SMTP_ADDRESS',
                    'type' =>	'string',
                ],
                'smtp-username' => [
                    'title' =>	'Username',
                    'key' =>	'SMTP_USERNAME',
                    'type' =>	'string',
                ],
                'smtp-password' => [
                    'title' =>	'Password',
                    'key' =>	'SMTP_PASSWORD',
                    'type' =>	'string',
                ],
                'smtp-protocol' => [
                    'title' =>	'Protocol',
                    'key' =>	'SMTP_PROTOCOL',
                    'type' =>	'string',
                ],
                'smtp-host' => [
                    'title' =>	'Host',
                    'key' =>	'SMTP_HOST',
                    'type' =>	'string',
                ],
                'smtp-port' => [
                    'title' =>	'Port',
                    'key' =>	'SMTP_PORT',
                    'type' =>	'integer',
                ],
            ]
        ];
    }
}
