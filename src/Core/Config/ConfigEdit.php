<?php

namespace Illusion\Core\Config;

class ConfigEdit {

    /**
     *
     */

    /**
     * Adds a config implementation.
     */

    // Maybe this implementation must be diffrent than those below
    // Cuz this is sorta global and those below is sorta agile
    // Now i thinking it must be the same, cuz thats what is was before.
    function addConfigImpl($impl) {

    }

    /**
     * Removes a config implementation.
     */
    function removeConfigImpl($impl) {

    }

    /** is confusing read/save vs stash/load
     * Reads a config state from the implementation store.
     *
     * Returns a ConfigState.
     */
    function read($impl) {

    }

    /**
     * Saves a config state to the implementation store.
     */
    // maybe write
    function save($impl, $state) {

    }

    /**
     * Stashes (put away) a config state.
     * If no config state is provided, it will be read.
     */
    function stash($impl, $state = null) {

    }

    /**
     * Loads a config state from the stash.
     * If no config state has been stashed, the config state will be read then stashed.
     *
     * Returns a ConfigState.
     */
    // maybe fetch      i
    // maybe unstash    i
    function load($impl) {

    }

    /**
     * Updates a config implementation with a config state.
     */
    function update($impl, $state) { // previously setup

    }

    /**
     * Sets a config X
     */
    function setConfig($class, $config) {

    }

    /**
     * Gets a config X from a config implementation. X = utrag/slice/extract
     */
    function getConfigX($class, $entries = null, $exclude = null, $fresh = null) {

    }

    // maybe getDecoratedConfigX
    // maybe getGeneratedConfigX
    // maybe getPartlyGeneratedConfigX
    function getConfigXX($class, $entries = null, $exclude = null, $fresh = null) { // previously getDefaultConfig

    }

    // decorationEntryValueByState
    // decorationEntryValueByState normalize = censored/pixlat
    function normalizeEntryValueByState($entry/*, */) { // previously normalizeInfo

    }
}
