<?php

namespace Illusion\Core;

use Illusion\Config;

use Exception;

class DocComments {

    static protected $instance;

    static function instance() {

        return self::$instance ?: self::$instance = new self;
    }

    protected $sources = [];
    protected $classes = [];

    function addSource($libroot, $topname) {

        if (!@$this->sources["$libroot:$topname"]) {

            $this->sources["$libroot:$topname"] = [ $libroot, $topname ];
        }
    }

    function addClass($class) {

        foreach ($class ? is_array($class) ? $class : [ $class ] : [] as $c) {

            @$this->classes[$c] || $this->classes[$c] = [];
        }
    }

    function getClasses() {

        foreach ($this->sources as $key => $source) {

            if (!@$source[2] && $this->sources[$key][2] = 1) {

                $this->addClass(Util::listClasses($source[0], $source[1]));
            }
        }
    }

    function getComments($class = null, $inherited = null) {

        foreach ($class ? is_array($class) ? $class : [ $class ] : array_keys($this->classes) as $c) {

            if (!@$this->classes[$c][ $inherited ? 0 : 1 ]) {

                try {

                    if ($inherited) {

                        $this->classes[$c][0] = Util::getInheritedDocComments($c);

                    } else {

                        $this->classes[$c][1] = Util::getDocComments($c);
                    }

                } catch (Exception $e) {

                }
            }
        }
    }

    function get($class = null, $inherited = null) {

        $this->addClass($class);
        $class || $this->getClasses();
        $this->getComments($class, $inherited);

        foreach ($class ? is_array($class) ? $class : [ $class ] : array_keys($this->classes) as $c) {

            @$this->classes[$c][ $inherited ? 0 : 1 ] && $docs[$c] = $this->classes[$c][ $inherited ? 0 : 1 ];
        }

        return is_string($class) ? @$docs[$class] ?: [] : @$docs ?: [];
    }
}
