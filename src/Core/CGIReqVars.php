<?php

namespace Illusion\Core;

class CGIReqVars {

    function __construct() {

        if (!@$_SERVER['REQVARS'] && php_sapi_name() != 'cli') {

            $_SERVER['REQVARS']['get'] = $_GET;
            $_SERVER['REQVARS']['post'] = $_POST;
            $_SERVER['REQVARS']['headers'] = [];

            $_SERVER['REQBODY'] = @file_get_contents('php://input') ?: '';

            $_SERVER['REQVARS']['body'] =	@$_SERVER['REQBODY'] ?: '';
            $_SERVER['REQVARS']['url'] =    @$_SERVER['REQUEST_URI'] ?: '';
            $_SERVER['REQVARS']['host'] =   strtolower(@$_SERVER['SERVER_NAME']);
            $_SERVER['REQVARS']['port'] =   +@$_SERVER['SERVER_PORT'] ?: -1;
            $_SERVER['REQVARS']['method'] = strtolower(@$_SERVER['REQUEST_METHOD']);
            $_SERVER['REQVARS']['https'] =  @$_SERVER['https'] && strtolower($_SERVER['https']) != 'off' ? 'on' : null;

            foreach($_SERVER as $key => $val) {

                if (strtolower(substr($key, 0, 5)) == 'http_') {

                    $_SERVER['REQVARS']['headers'][str_replace(' ', '-', ucwords(strtolower(str_replace('_', ' ', substr($key, 5)))))] = $val;
                }
            }
        }
    }
}
