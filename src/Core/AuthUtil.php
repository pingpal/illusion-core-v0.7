<?php

namespace Illusion\Core;

class AuthUtil {

    static protected $instance;

    static function instance() {

        return self::$instance ?: self::$instance = new self;
    }

    function hash($string, $secret) {

        return hash_hmac('sha1', $string, $secret);
    }

    function pack($num) {

        $str  = chr((($num & 0xF000) >> 12)	+ 97);
        $str .= chr((($num & 0x0F00) >> 8)	+ 97);
        $str .= chr((($num & 0x00F0) >> 4)	+ 97);
        $str .= chr((($num & 0x000F) >> 0)	+ 97);

        return $str;
    }

    function unpack($str) {

        $num = (ord($str[0]) - 97);
        $num = (ord($str[1]) - 97) | ($num << 4);
        $num = (ord($str[2]) - 97) | ($num << 4);
        $num = (ord($str[3]) - 97) | ($num << 4);

        return $num;
    }

    function JSONStringify($data) {

        is_object($data) && $data = (array) $data;
        is_array($data) || $data = [];

        return @json_encode($data, JSON_UNESCAPED_UNICODE) ?: 'null';
    }

    function JSONParse($data) {

        $data = @json_decode($data, true);

        is_object($data) && $data = (array) $data;
        is_array($data) || $data = [];

        return $data;
    }

    function base64Encode($data) {

        $data = @base64_encode($data) ?: '';
        $data = strtr($data, '+/=', '-_.');

        return $data;
    }

    function base64Decode($data) {

        $data = strtr($data, '-_.', '+/=');
        $data = @base64_decode($data, true) ?: '';

        return $data;
    }

    function encode($packet) {

        $packet = $this->JSONStringify($packet);
        $packet = $this->base64Encode($packet);

        return '2' . $packet;
    }

    function decode($packet) {

        $packet = $this->peekBody($packet);
        $packet = $this->base64Decode($packet);
        $packet = $this->JSONParse($packet);

        return $packet;
    }

    function peekHash($packet) {

        if (1 + 40 <= strlen($packet) && $packet[0] == '1') {

            return substr($packet, 1, 40);
        }
    }

    function peekPublic($packet) {

        if (1 + 40 + 32 <= strlen($packet) && $packet[0] == '1') {

            return substr($packet, 1 + 40, 32);
        }
    }

    function peekNonce($packet) {

        if (1 + 40 + 32 + 13 <= strlen($packet) && $packet[0] == '1') {

            return substr($packet, 1 + 40 + 32, 13);
        }
    }

    function peekBody($packet) {

        if (1 + 40 + 32 + 13 < strlen($packet) && $packet[0] == '1') {

            $packet = substr($packet, 1 + 40 + 32 + 13);
        }

        if (1 < strlen($packet) && $packet[0] == '2') {

            $packet = substr($packet, 1);
        }

        return $packet;
    }

    function sign($packet, $public, $private, $nonce) {

        $packet = '2' . $this->peekBody($packet);
        $nonce = str_pad(substr($nonce, 0, 13), 13, '0');

        $packet = $public . $nonce. $packet;
        $packet = $this->hash($packet, $private) . $packet;

        return '1' . $packet;
    }

    function auth($packet, $public, $private) {

        if (strlen($packet) < 1 + 40 + 32) {

            return false;
        }

        $hash =     substr($packet, 1, 40);
        $test =     substr($packet, 1 + 40, 32);
        $packet =   substr($packet, 1 + 40);

        return $public && $private && $public == $test && $hash == $this->hash($packet, $private);
    }
}
