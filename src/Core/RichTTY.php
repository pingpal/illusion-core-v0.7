<?php

namespace Illusion\Core;

class RichTTY {

	static $STYLE = [

		'1'  => "\033[1m%s\033[0m",
		'2'  => "\033[2m%s\033[0m",
		'3'  => "\033[3m%s\033[0m",
		'4'  => "\033[4m%s\033[0m",
		'5'  => "\033[5m%s\033[0m",
		'7'  => "\033[7m%s\033[0m",
		'8'  => "\033[8m%s\033[0m",
		'30' => "\033[30m%s\033[0m",
		'31' => "\033[31m%s\033[0m",
		'32' => "\033[32m%s\033[0m",
		'33' => "\033[33m%s\033[0m",
		'34' => "\033[34m%s\033[0m",
		'35' => "\033[35m%s\033[0m",
		'36' => "\033[36m%s\033[0m",
		'37' => "\033[37m%s\033[0m",
		'40' => "\033[40m%s\033[0m",
		'41' => "\033[41m%s\033[0m",
		'42' => "\033[42m%s\033[0m",
		'43' => "\033[43m%s\033[0m",
		'44' => "\033[44m%s\033[0m",
		'45' => "\033[45m%s\033[0m",
		'46' => "\033[46m%s\033[0m",
		'47' => "\033[47m%s\033[0m",

		'bold'      => "\033[1m%s\033[0m",
		'dark'      => "\033[2m%s\033[0m",
		'italic'    => "\033[3m%s\033[0m",
		'underline' => "\033[4m%s\033[0m",
		'blink'     => "\033[5m%s\033[0m",
		'reverse'   => "\033[7m%s\033[0m",
		'concealed' => "\033[8m%s\033[0m",
		'black'     => "\033[30m%s\033[0m",
		'red'       => "\033[31m%s\033[0m",
		'green'     => "\033[32m%s\033[0m",
		'yellow'    => "\033[33m%s\033[0m",
		'blue'      => "\033[34m%s\033[0m",
		'magenta'   => "\033[35m%s\033[0m",
		'cyan'      => "\033[36m%s\033[0m",
		'white'     => "\033[37m%s\033[0m",
		'bg_black'   => "\033[40m%s\033[0m",
		'bg_red'     => "\033[41m%s\033[0m",
		'bg_green'   => "\033[42m%s\033[0m",
		'bg_yellow'  => "\033[43m%s\033[0m",
		'bg_blue'    => "\033[44m%s\033[0m",
		'bg_magenta' => "\033[45m%s\033[0m",
		'bg_cyan'    => "\033[46m%s\033[0m",
		'bg_white'   => "\033[47m%s\033[0m",
	];

	static $CMD = [

		'reset' =>	"\x1B[0G",
		'moveup' =>	"\x1B[%sA",
	];

	static protected $instance;

	static function instance() {

		return self::$instance ?: self::$instance = new self;
	}

	function reset() {

		return self::$CMD['reset'];
	}

	function moveUp($n = 1) {

		return sprintf(self::$CMD['moveup'], $n);
	}

	function isTTY() {

		return php_sapi_name() == 'cli' && @posix_isatty(1);
	}

	function apply($style, $text) {

		$style = strtolower($style);

		if ($this->isTTY() && @self::$STYLE[$style]) {

			$text = sprintf(self::$STYLE[$style], $text);
		}

		return $text;
	}

	function clean($text) {

		return preg_replace("/\033\[\d+m/", '', $text);
	}

	function strip($text) {

		return preg_replace_callback('#<([A-z0-9_-]+)>(.*?)<\\\\?/\\1?>#s', function ($matches) {

			return $this->strip($matches[2]);

		}, $text);
	}

	function erase($text) {

		return $this->strip($this->clean($text));
	}

	function colorize($text) {

		return preg_replace_callback('#<([A-z0-9_-]+)>(.*?)<\\\\?/\\1?>#s', function ($matches) {

			return $this->apply($matches[1], $this->colorize($matches[2]));

		}, $text);
	}

	function format($object) {

		$string = '';
		$lengths = [];
		$rows = [];

		foreach (Util::isList($object) ? $object : [$object] as $n => $row) {

			if (/*is_string($row) ||*/ is_int($row) || is_float($row)) {

				$row = "$row";

			} else if (is_bool($row)) {

				$row = $row ? '<32>YES</32>' : '<31>NO</31>';
			}

			if (is_string($row)) {

				$string == '' || $string .= "\n";
				$string .= $row;

			} else if (Util::hasArrayAccess($row) && !Util::isList($row)) {

				foreach ($row as $key => $val) {

					if (/*is_string($val) ||*/ is_int($val) || is_float($val)) {

						$val = "$val";

					} else if (is_bool($val) ) {

						$val = $val ? '<32>YES</32>' : '<31>NO</31>';
					}

					if (is_string($val)) {

						if (!$n) {

							$lengths[$key] = strlen($this->erase($key));
							$rows[$n][$key] = strtoupper($key);
						}

						if (isset($lengths[$key])) {

							if ($lengths[$key] < strlen($this->erase($val))) {

								$lengths[$key] = strlen($this->erase($val));
							}

							$rows[$n + 1][$key] = $val;
						}
					}
				}
			}
		}

		foreach ($rows as $n => $row) {

			$string == '' || $string .= "\n";

			foreach ($lengths as $key => $val) {

				if (isset($row[$key])) {

					$string .= $row[$key];
					$val -= strlen($this->erase($row[$key]));
				}

				$string .= str_repeat(' ', $val + 4);
			}

			$string = substr($string, 0, -4);
		}

		return $string;
	}

	function box($lines) {

		foreach ($lines as $line) {

			$max = max(@$max, strlen($this->erase($line)));
		}

		$box = $end = " <37>" . str_repeat('-', $max + 2) . "</37>\n";

		foreach ($lines as $line) {

			$box .= "<37>|</37> " . $line . str_repeat(' ', $max - strlen($this->erase($line))) . " <37>|</37>\n";
		}

		return $box . $end;
	}
}
