<?php

namespace Illusion\Core;

use Illusion\Config;

use ReflectionClass;
use ReflectionException;
use Exception;

class ConfigEdit {

    protected $BP = [

        'categories' => [
            'misc' => [
                'title' => '',
                'entries' => [
                    'debug',
                    'install',
                    'brand',
                    'timezone',
                    'command',
                    'executable',
                    'writable',
                    'packet-route',
                    'secure-key',
                    'environments',
                ],
            ],
            'mysql' => [
                'title' => 'MySQL',
                'entries' => [
                    'mysql-server',
                    'mysql-database',
                    'mysql-username',
                    'mysql-password',
                ],
            ],
            'smtp' => [
                'title' => 'SMTP',
                'entries' => [
                    'smtp-address',
                    'smtp-username',
                    'smtp-password',
                    'smtp-protocol',
                    'smtp-host',
                    'smtp-port',
                ],
            ],
            'cache' => [
                'title' => 'Cache',
                'entries' => [
                    'cache-ttl',
                    'cache-chance',
                ],
            ],
        ],

        'entries' => [
            'debug' => [
                'title' =>	'Debug',
                'desc' =>	'Should be off in production environments',
                'key' =>	'DEBUG',
                'type' =>	'boolean',
            ],
            'install' => [
                'title' =>	'Install',
                'key' =>	'INSTALL',
                'type' =>	'boolean',
            ],
            'brand' => [
                'title' =>	'Brand',
                'key' =>	'BRAND',
                'type' =>	'string',
            ],
            'timezone' => [
                'title' =>	'Timezone',
                'key' =>	'TIMEZONE',
                'type' =>	'string',
            ],
            'command' => [
                'title' =>	'Command',
                'key' =>	'COMMAND',
                'type' =>	'string',
            ],
            'executable' => [
                'title' =>	'Executable',
                'key' =>	'PATH_EXE',
                'type' =>	'string',
            ],
            'writable' => [
                'title' =>	'Writable Class Path',
                'desc' =>	'Relative to vendor/%VENDOR%/',
                'key' =>	'_PATH_WRITABLE',
                'type' =>	'string',
            ],
            'packet-route' => [
                'title' =>	'Packet Route',
                'key' =>	'ROUTE_PACKET',
                'type' =>	'string',
            ],
            'secure-key' => [
                'title' =>	'Secure Store Key',
                'key' =>	'SECURE_STORE_KEY',
                'type' =>	'string',
            ],
            'environments' => [
                'title' =>	'Environments',
                'key' =>	'ENVIRONMENTS',
                'type' =>	'string',
            ],
            'cache-ttl' => [
                'title' =>	'Time To Live',
                'key' =>	'CACHE_DEFAULT_TTL',
                'type' =>	'integer',
            ],
            'cache-chance' => [
                'title' =>	'Clean Chance',
                'key' =>	'CACHE_CLEAN_CHANCE',
                'type' =>	'double|float|integer',
            ],
            'mysql-server' => [
                'title' =>	'Server',
                'key' =>	'MYSQL_SERVER',
                'type' =>	'string',
            ],
            'mysql-database' => [
                'title' =>	'Database',
                'key' =>	'MYSQL_DATABASE',
                'type' =>	'string',
            ],
            'mysql-username' => [
                'title' =>	'Username',
                'key' =>	'MYSQL_USERNAME',
                'type' =>	'string',
            ],
            'mysql-password' => [
                'title' =>	'Password',
                'key' =>	'MYSQL_PASSWORD',
                'type' =>	'string',
            ],
            'smtp-address' => [
                'title' =>	'Address',
                'key' =>	'SMTP_ADDRESS',
                'type' =>	'string',
            ],
            'smtp-username' => [
                'title' =>	'Username',
                'key' =>	'SMTP_USERNAME',
                'type' =>	'string',
            ],
            'smtp-password' => [
                'title' =>	'Password',
                'key' =>	'SMTP_PASSWORD',
                'type' =>	'string',
            ],
            'smtp-protocol' => [
                'title' =>	'Protocol',
                'key' =>	'SMTP_PROTOCOL',
                'type' =>	'string',
            ],
            'smtp-host' => [
                'title' =>	'Host',
                'key' =>	'SMTP_HOST',
                'type' =>	'string',
            ],
            'smtp-port' => [
                'title' =>	'Port',
                'key' =>	'SMTP_PORT',
                'type' =>	'integer',
            ],
        ]
    ];

    protected $UR = [ 'mysql-database', 'mysql-username' ];
    protected $PW = [ 'secure-key', 'smtp-password', 'mysql-password' ];

    protected $CUSTOM;

    protected $stash = [];

    static protected $instance;

    static function instance() {

        return self::$instance ?: self::$instance = new self;
    }

    function __construct() {

        $this->CUSTOM['executable'] = function () {

            return defined('PHP_BINARY') ? PHP_BINARY : null;
        };
    }

    function setRuntimeConfig($class, $config, $force = false) {

        try {

            if ($force) {

                foreach ($config as $key => $value) {

                    $class::$$key = $value;
                }

                return;
            }

            foreach ($this->BP['entries'] as $name => $info) {

                if (isset($config[$name])) {

                    $class::$$info['key'] = $this->checkValue($config[$name], $info['type']);
                }
            }

        } catch (Exception $e) {

        }
    }

    function setup($class, $config) {

        $this->override($class, $config);
    }

    function stash($class, $config = null) {

        if ($config) {

            $this->stash[$class] = $config;

        } else {

            $this->load($class);
        }
    }

    function read($class) {

        $config = [];

        try {

            $reflection = new ReflectionClass($class);

            foreach ($reflection->getProperties() as $property) {

                $config[$property->getName()] = $property->getValue();
            }

        } catch (ReflectionException $e) {

        }

        return $config;
    }

    function load($class) {

        if (!@$this->stash[$class]) {

            $this->stash[$class] = $this->read($class);
        }

        return $this->stash[$class];
    }

    function save($class, $config) {

        $path       = Resolve::namespaceToFilePath($class);
        $namespace  = Resolve::stripClassFromNamespace($class);
        $class      = Resolve::stripNamespaceFromClass($class);

        $code = "<?php\n\nnamespace $namespace;\n\nclass $class {\n\n";

        foreach ($config as $key => $value) {

            $value = var_export($value, true);
            $code.= "\tstatic \$$key = $value;\n";
        }

        $code.= "}\n";

        Util::write($path, $code);
    }

    function setConfig($class, $config) {

        $result = [];

        foreach ($this->BP['entries'] as $name => $info) {

            if (isset($config[$name])) {

                $result[$info['key']] = $this->checkValue($config[$name], $info['type']);
            }
        }

        $this->save($class, $result);

        return $result;
    }

    function getConfig($class, $entries = null, $exclude = null, $fresh = null) {

        $result = [ 'categories' => $this->BP['categories'], 'entries' => [] ];

        $config = $fresh ? $this->read($class) : $this->load($class);

        $entries || $entries = array_keys($this->BP['entries']);

        foreach ($this->BP['entries'] as $name => $info) {

            if (isset($config[$info['key']])) {

                if ($exclude xor in_array($name, $entries)) {

                    $result['entries'][$name] = $this->normalizeInfo($info, $config[$info['key']], $config);
                }
            }
        }

        foreach ($result['categories'] as $key => $category) {

            foreach ($category['entries'] as $i => $name) {

                if (!($exclude xor in_array($name, $entries))) {

                    unset($result['categories'][$key]['entries'][$i]);
                }
            }

            if (!$result['categories'][$key]['entries']) {

                unset($result['categories'][$key]);

            } else {

                $result['categories'][$key]['entries'] = array_values($result['categories'][$key]['entries']);
            }
        }

        return $result;
    }

    function getDefaultConfig($class, $entries = null, $exclude = null, $fresh = null) {

        // $entries || $entries = [];

        $result = $this->getConfig($class, $entries, $exclude, $fresh);

        foreach ($this->PW as $name) {

            // makes no sense
            // if ($exclude xor in_array($name, $entries)) {

                $result['entries'][$name]['value'] = $this->generatePassword();
            // }
        }

        foreach ($this->UR as $name) {

            // makes no sense
            // if ($exclude xor in_array($name, $entries)) {

                $result['entries'][$name]['value'] = $this->generateUsername();
            // }
        }

        foreach ($this->CUSTOM as $name => $closure) {

            $value = $closure();

            if ($value) {

                $result['entries'][$name]['value'] = $value;
            }
        }

        return $result;
    }

    function override($class, $config) {

        foreach ($config as $key => $value) {

            try {

                $class::$$key = $value;

            } catch (Exception $e) {

            }
        }
    }

    function generateUsername($length = null) {

        return $this->generatePassword(16);
    }

    function generatePassword($length = null) {

        return substr(str_shuffle('abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789'), 0, $length ?: 32);
    }

    function checkValue($value, $type) {

        $defaults = [ 'boolean' => false, 'integer' => 0, 'double' => 0, 'float' => 0, 'string' => '' ];

        $types = explode('|', $type);
        $type = $types[0];

        $default = $defaults[$type];

        $ok = false;

        foreach ($types as $x) {

            switch ($x) {

                case 'boolean':
                    $ok = is_bool($value) || $value === 'true' || $value === 'yes';
                    break;
                case 'integer':
                    $ok = is_int($value) || is_numeric($value);
                    break;
                case 'double':
                    $ok = is_double($value) || is_numeric($value);
                    break;
                case 'float':
                    $ok = is_float($value) || is_numeric($value);
                    break;
                case 'string':
                    $ok = is_string($value);
                    break;
            }

            if ($ok) {

                break;
            }
        }

        if (!$ok) {

            return $default;
        }

        switch ($type) {

            case 'boolean':
                return (bool) $value;
            case 'integer':
                return (int) $value;
            case 'double':
                return (double) $value;
            case 'float':
                return (float) $value;
            case 'string':
                return (string) $value;
        }
    }

    function normalizeInfo($info, $value, $config) {

        $info['value'] = $this->checkValue($value, $info['type']);
        $info['type'] = explode('|', $info['type'])[0];

        if (@$info['desc']) {

            $search = [];
            $replace = [];

            foreach ($config as $key => $val) {

                $search[] = "%$key%";
                $replace[] = $val;
            }

            $info['desc'] = str_replace($search, $replace, $info['desc']);
        }

        unset($info['key']);

        return $info;
    }

    function mergeConfigs($to, $from) {

        try {

            $reflection = new ReflectionClass($from);

            foreach ($reflection->getProperties() as $property) {

                $name = $property->getName();
                $to::$$name = $property->getValue();
            }

        } catch (Exception $e) {

        }
    }

    function resolvePaths($class) {

        try {

            $reflection = new ReflectionClass($class);

            foreach ($reflection->getProperties() as $property) {

                if (!strncasecmp($property->getName(), '_path_', 6)) {

                    $name = $property->getName();
                    $path = $property->getValue();

                    $real = Util::resolvePath(Config::$LIBROOT . '/' . $path);

                    $class::$$name = $real ? $real : $path;
                }
            }

        } catch (ReflectionException $e) {

        }

        try {

            $reflection = new ReflectionClass($class);

            foreach ($reflection->getProperties() as $property) {

                if (!strncasecmp($property->getName(), '_wpath_', 6)) {

                    $name = $property->getName();
                    $path = $property->getValue();

                    $class::$$name = $class::$_PATH_WRITABLE . '/' . $path;
                }
            }

        } catch (ReflectionException $e) {

        }
    }

    function getWithPrefix($class, $prefix) {

        $array = [];

        try {

            $reflection = new ReflectionClass($class);

            foreach ($reflection->getProperties() as $property) {

                if (!strncasecmp($property->getName(), $prefix, strlen($prefix))) {

                    $name = $property->getName();
                    $path = $property->getValue();

                    $array[strtolower(substr($name, strlen($prefix)))] = $path;
                }
            }

        } catch (ReflectionException $e) {

        }

        return $array;
    }

    function dealWithScanList($class, $scan = null) {

        $sources = $this->getWithPrefix($class, '_scan_');

        $class::$SCAN = [];

        foreach ($scan ?: [] as $topname => $libroot) {

            $class::$SCAN[$topname] = $class::$DOCROOT . '/' . $libroot;
        }

        foreach ($sources as $source) {

            strpos($source, ':') === false && $source = ":$source";
            list($topname, $libroot) = explode(':', $source);

            $class::$SCAN[$topname] = $class::$DOCROOT . '/' . $libroot;
        }

        $class::$SCAN[$class::$TOPNAME] = $class::$LIBROOT;
    }
}
