<?php

namespace Illusion\Core;

use Illusion\Config;

use Exception;

class Launch {

    static protected $instance;

    static function instance() {

        return self::$instance ?: self::$instance = new self;
    }

    protected $inception = 0;
    protected $stashed = 0;

    function reality() {

        $this->stashed = $this->inception;
        $this->inception = 0;
    }

    function resetInception() {

        $this->inception = $this->stashed;
        $this->stashed = 0;
    }

    protected $helpers = [];

    function getHelper($context = null) {

        $key = $context ?: '*';

        if (!isset($this->helpers[$key])) {

            $this->helpers[$key] = new LaunchHelp($context);
        }

        return $this->helpers[$key];
    }

    protected $environments = [];
    protected $debug;
    protected $current;

    function __construct() {

        $this->environments = array_merge([ 'public' ], Config::$ENVIRONMENTS ? explode('|', Config::$ENVIRONMENTS) : []);
        $this->debug = Config::$DEBUG;
    }

    function escape() {

        throw new LaunchException("bad action: $this->current", 2);
    }

    function apply($action, $args = null, $context = null) {

        $args && is_array($args) || $args = $context ? $context->vars()->args() : [];

        $help = $this->getHelper($context ? $context->id() : null);

        $session = $context ? $context->session() : null;

        $callable =	Resolve::actionToCallable($action);
        $filePath = Resolve::actionToFilePath($action, 'tmpl.html');

        $callable =	$help->getCallable($action) ?: $callable;
        $public =	$help->isPublic($callable, $this->environments, $this->debug);

        $allow =    $help->getAllowed($callable) ?: [ 'nobody', 'someone' ];
        $deny =		$help->getDenied($callable);

        $affinity = !$context || $help->inContext($callable, $context->id());
        $access =   !$session || $session->isMemberOf($allow) && !$session->isMemberOf($deny);

        $isCallable = Util::isCallable($callable);
        $isFilePath = Util::isFile($filePath);

        if (!$this->inception && !$public || !$this->inception && (!$affinity || !$access) || !$isCallable && !$isFilePath) {

            throw new LaunchException("bad action: $action", 2);
        }

        $this->inception || $this->current = $action;
        $this->inception++;

        try {

            if ($isCallable) {

                $in = $context ? $context->in() : null;
                $out = $context ? $context->out() : null;

                $object = Util::call($callable, $args, $in, $out, $context);

            } else if ($isFilePath) {

                $object = inflate($filePath, null, $args, '');
            }

        } catch (Exception $e) {

            $this->inception && $this->inception--;
            $this->inception || $this->current = null;

            throw $e;
        }

        $this->inception && $this->inception--;
        $this->inception || $this->current = null;

        return $object;
    }

    function internal($action, $args = null, $session = null) {

        $args && is_array($args) || $args = [];

        $help = $this->getHelper('internal');

        $callable =	$help->getCallable($action);

        $public =	$help->isPublic($callable, $this->environments);

        $allow =    $help->getAllowed($callable) ?: [ 'nobody', 'someone' ];
        $deny =		$help->getDenied($callable);

        $access =   !$session || $session->isMemberOf($allow) && !$session->isMemberOf($deny);

        $isCallable = Util::isCallable($callable);

        if ($public && $access && $isCallable) {

            return Util::call($callable, $args, $session);
        }
    }

    function trigger($action, $args = null, $session = null) {

        $help = $this->getHelper('internal');
        $info = $help->getInfo();

        $actions = [];

        if (Util::startsWith($action, '*')) {

            $action = ltrim($action, '*');
            $action = ltrim($action, '.');
            $action = ".$action";

            foreach ($info['alias'] as $alias => $callable) {

                if (Util::endsWith($alias, $action)) {

                    $callable = $help->getCallable($alias);

                    $actions[$callable] = $alias;
                }
            }

        } else if (Util::endsWith($action, '*')) {

            $action = rtrim($action, '*');
            $action = rtrim($action, '.');
            $action = "$action.";

            foreach ($info['alias'] as $alias => $callable) {

                if (Util::startsWith($alias, $action)) {

                    $callable = $help->getCallable($alias);

                    $actions[$callable] = $alias;
                }
            }

        } else {

            $callable = $help->getCallable($action);

            $actions[$callable] = $action;
        }

        foreach ($actions as $callable => $action) {

            $this->internal($action, $args, $session);
        }
    }

    function exists($action, $context = null, $inside = true, $debug = null) {

        $inside = $inside   !== null ? $inside  : !!$this->inception;
        $debug  = $debug    !== null ? $debug   : $this->debug;

        $help = $this->getHelper($context ? $context->id() : null);

        $session = $context ? $context->session() : null;

        $callable =	Resolve::actionToCallable($action);
        $filePath = Resolve::actionToFilePath($action, 'tmpl.html');

        $callable =	$help->getCallable($action) ?: $callable;
        $public =	$help->isPublic($callable, $this->environments, $debug);

        $allow =    $help->getAllowed($callable) ?: [ 'nobody', 'someone' ];
        $deny =		$help->getDenied($callable);

        $affinity = !$context || $help->inContext($callable, $context->id());
        $access =   !$session || $session->isMemberOf($allow) && !$session->isMemberOf($deny);

        $isCallable = Util::isCallable($callable);
        $isFilePath = Util::isFile($filePath);

        return !(!$inside && !$public || !$inside && (!$affinity || !$access) || !$isCallable && !$isFilePath);
    }

    function getAnnotatedActions($context = null, $inside = true, $debug = null) {

        $inside = $inside   !== null ? $inside  : !!$this->inception;
        $debug  = $debug    !== null ? $debug   : $this->debug;

        $help = $this->getHelper($context ? $context->id() : null);
        $info = $help->getInfo();

        $session = $context ? $context->session() : null;

        $aliases = [];

        foreach ($info['alias'] as $alias => $callable) {

            $public =	$help->isPublic($callable, $this->environments, $debug);

            $allow =    $help->getAllowed($callable) ?: [ 'nobody', 'someone' ];
            $deny =		$help->getDenied($callable);

            $affinity = !$context || $help->inContext($callable, $context->id());
            $access =   !$session || $session->isMemberOf($allow) && !$session->isMemberOf($deny);

            $isCallable = Util::isCallable($callable);

            if (!(!$inside && !$public || !$inside && (!$affinity || !$access) || !$isCallable)) {

                $aliases[$alias]['callable'] = $callable;
                $aliases[$alias]['contexts'] = @$info['context'][$callable] ?: [];
            }
        }

        return $aliases;
    }

    function getAnnotatedCallables($context = null, $inside = true, $debug = null) {

        $inside = $inside   !== null ? $inside  : !!$this->inception;
        $debug  = $debug    !== null ? $debug   : $this->debug;

        $help = $this->getHelper($context ? $context->id() : null);
        $info = $help->getInfo();

        $session = $context ? $context->session() : null;

        $aliases = [];

        foreach ($info['alias'] as $alias => $callable) {

            isset($aliases[$callable]) || $aliases[$callable] = [];

            $aliases[$callable][] = $alias;
        }

        $publics = [];

        foreach ($info['access'] as $callable => $access) {

            $public =	$help->isPublic($callable, $this->environments, $debug);

            $allow =    $help->getAllowed($callable) ?: [ 'nobody', 'someone' ];
            $deny =		$help->getDenied($callable);

            $affinity = !$context || $help->inContext($callable, $context->id());
            $access =   !$session || $session->isMemberOf($allow) && !$session->isMemberOf($deny);

            $isCallable = Util::isCallable($callable);

            if (!(!$inside && !$public || !$inside && (!$affinity || !$access) || !$isCallable)) {

                $publics[$callable]['aliases'] = @$aliases[$callable] ?: [];
                $publics[$callable]['aliases'][] = Resolve::callableToAction($callable);
                $publics[$callable]['contexts'] = @$info['context'][$callable] ?: [];
            }
        }

        return $publics;
    }
}
