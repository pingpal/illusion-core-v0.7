<?php

namespace Illusion\Core;

use Illusion\Config;

use ReflectionClass;

class LaunchHelp {

    protected $info = [];

    function __construct($context = null) {

        $this->info = Cache::instance()->get('illusion.launch.' . $context ?: '*');

        if (!$this->info) {

            $this->info = [ 'alias' => [], 'allow' => [], 'deny' => [], 'access' => [], 'context' => [] ];

            foreach (DocComments::instance()->get() as $class => $doc) {

                $reflection = new ReflectionClass($class);
                $aliases = @$doc['class'][$class]['alias'] ?: [];

                // $classCtxOk = !$context || in_array($context, @$doc['class'][$class]['context'] ?: [ $context ]); 2015-10-5
                $classCtxOk = !$context || in_array($context, @$doc['class'][$class]['context'] ?: []);

                if ($classCtxOk) {

                    foreach ($aliases as $alias) {

                        foreach ($reflection->getMethods() as $method) {

                            $methodCtxOk = !$context || in_array($context, @$doc['method'][$method->name]['context'] ?: [ $context ]);

                            if ($methodCtxOk) {

                                if ($method->isStatic() && $method->isPublic()) {

                                    $this->info['alias'][strtolower("$alias.$method->name")] = "$class::$method->name";
                                }
                            }
                        }
                    }
                }

                foreach (@$doc['class'][$class]['context'] ?: [] as $ctx) {

                    foreach ($reflection->getMethods() as $method) {

                        if ($method->isStatic() && $method->isPublic()) {

                            $this->info['context']["$class::$method->name"][] = $ctx;
                        }
                    }
                }

                foreach (@$doc['class'][$class]['access'] ?: [] as $access) {

                    foreach ($reflection->getMethods() as $method) {

                        if ($method->isStatic() && $method->isPublic()) {

                            $this->info['access']["$class::$method->name"][] = $access;
                        }
                    }
                }

                foreach (@$doc['class'][$class]['deny'] ?: [] as $deny) {

                    foreach ($reflection->getMethods() as $method) {

                        if ($method->isStatic() && $method->isPublic()) {

                            $this->info['deny']["$class::$method->name"][] = $deny;
                        }
                    }
                }

                foreach (@$doc['class'][$class]['allow'] ?: [] as $allow) {

                    foreach ($reflection->getMethods() as $method) {

                        if ($method->isStatic() && $method->isPublic()) {

                            $this->info['allow']["$class::$method->name"][] = $allow;
                        }
                    }
                }

                foreach ($doc['method'] as $name => $method) {

                    $ok = $classCtxOk;

                    if (isset($doc['method'][$name]['context'])) {

                        $ok = !$context || in_array($context, @$doc['method'][$name]['context']);
                    }

                    if ($ok) {

                        if (@$method['direct']) {

                            foreach ($method['direct'] as $direct) {

                                $this->info['alias'][strtolower($direct)] = "$class::$name";
                            }
                        }

                        if (@$method['alias']) {

                            $this->info['alias'][strtolower(str_replace('\\', '.', $class) . '.' . end($method['alias']))] = "$class::$name";

                            foreach ($method['alias'] as $alias) {

                                foreach ($aliases as $parent) {

                                    $this->info['alias'][strtolower($parent . '.' . $alias)] = "$class::$name";
                                }
                            }
                        }
                    }

                    $LS = 1; // list
                    $OR = 2; // override
                    $AC = 4; // all context

                    foreach ([ 'allow' => $LS | $OR, 'deny' => $LS | $OR, 'access' => $LS | $OR, 'context' => $LS | $OR | $AC ] as $key => $ops) {

                        if (($ok || ($ops & $AC)) && @$method[$key]) {

                            if ($ops & $OR) {

                                unset($this->info[$key]["$class::$name"]);
                            }

                            if (($ops & $LS) && !isset($this->info[$key]["$class::$name"])) {

                                $this->info[$key]["$class::$name"] = [];
                            }

                            foreach ($method[$key] as $value) {

                                ($ops & $LS) && $this->info[$key]["$class::$name"][] = strtolower($value);
                                ($ops & $LS) || $this->info[$key]["$class::$name"] =   strtolower($value);
                            }
                        }
                    }
                }
            }

            Cache::instance()->put('illusion.launch.' . $context ?: '*', $this->info);
        }
    }

    function getInfo() {

        return $this->info;
    }

    function getCallable($alias) {

        return @$this->info['alias'][strtolower($alias)];
    }

    function getAllowed($callable) {

        return @$this->info['allow'][$callable] ?: [];
    }

    function getDenied($callable) {

        return @$this->info['deny'][$callable] ?: [];
    }

    function isPublic($callable, $strings, $debug = null) {

        if (@$this->info['access'][$callable]) {

            foreach ($this->info['access'][$callable] as $value) {

                if ($debug || in_array($value, $strings)) {

                    return true;
                }
            }
        }

        return false;
    }

    function inContext($callable, $context) {

        if (@$this->info['context'][$callable]) {

            foreach ($this->info['context'][$callable] as $value) {

                if ($value == $context) {

                    return true;
                }
            }

            return false;
        }

        return true;
    }
}
