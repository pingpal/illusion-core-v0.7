<?php

namespace Illusion\Core;

use Illusion\Config;

use Exception;

class RouteHelp {

    protected $router;

    protected $info = [];

    function __construct($router) {

        $this->router = $router;

        $this->info = Cache::instance()->get('illusion.route');

        if (!$this->info) {

            $this->info['route'] =	[];

            foreach (DocComments::instance()->get() as $class => $doc) {

                foreach (@$doc['class'][$class]['route'] ?: [] as $route) {

                    $this->info['route'][] = preg_split('/\s+/', $route);
                }
            }

            Cache::instance()->put('illusion.route', $this->info);
        }
    }

    function on($callback) {

        foreach (@$this->info['route'] ?: [] as $rule) {

            $this->router->on(@$rule[0], function () use ($rule, $callback) {

                $keys = @$rule[3];
                $data = func_get_args();

                $args = $this->router->vars()->args();
                $keys = $keys ? explode(',', $keys) : [];

                for ($i = 0; $i < min(count($keys), count(@$data)); $i++) {

                    $args[trim($keys[$i])] = $data[$i];
                }

                $callback(@$rule[1], @$rule[2], $args);
            });
        }
    }

    function setup($contexts) {

        foreach (@$this->info['route'] ?: [] as $args) {

            if (@$contexts[@$args[1]]) {

                $context = $contexts[$args[1]]();

                $this->router->on(@$args[0], $context, @$args[2], @$args[3]);
            }
        }
    }
}
