<?php

namespace Illusion\Core;

class HttpUtil {

    static protected $instance;

    static function instance() {

        return self::$instance ?: self::$instance = new self;
    }

    function urlEncode($bytes) {

        $bytes = addslashes($bytes);
        $bytes = base64_encode($bytes);
        $bytes = strtr($bytes, '+/=', '-_.');

        return $bytes;
    }

    function urlDecode($bytes) {

        $bytes = strtr($bytes, '-_.', '+/=');
        $bytes = base64_decode($bytes);
        $bytes = stripslashes($bytes);

        return $bytes;
    }

    function parseRequest($bytes) {

        if (preg_match("/^(GET|POST|PUT|DELETE) ([^?]+?)(?:\\?(.*?))? HTTP\\/?([^\r\n]+)\r?\n((?:[^:]+:[^\r\n]+\r?\n)*)\r?\n([\s\S]*)$/", $bytes, $hit)) {

            $map['length'] = strlen($hit[0]) - strlen($hit[6]);

            $map['method'] =	$hit[1];
            $map['path'] =		$hit[2];
            $map['query'] =		$hit[3];
            $map['version'] =	$hit[4];
            $map['body'] =		$hit[6];

            $map['headers'] = [];

            if ($hit[5]) {

                foreach (preg_split("/\r?\n/", trim($hit[5])) as $header) {

                    list($key, $val) = explode(':', $header, 2) + [0, ''];
                    $map['headers'][strtolower($key)] = trim($val);
                }
            }

            return $map;
        }
    }

    function parseResponse($bytes) {

        if (preg_match("/^HTTP\\/?([^ ]+) (\d+) (\w+)\r?\n((?:[^:]+:[^\r\n]+\r?\n)*)\r?\n([\s\S]*)$/", $bytes, $hit)) {

            $map['length'] = strlen($hit[0]) - strlen($hit[5]);

            $map['version'] =			$hit[1];
            $map['status']['code'] =    +$hit[2];
            $map['status']['text'] =	$hit[3];
            $map['body'] =				$hit[5];

            $map['headers'] = [];

            if ($hit[4]) {

                foreach (preg_split("/\r?\n/", trim($hit[4])) as $header) {

                    list($key, $val) = explode(':', $header, 2) + [0, ''];
                    $map['headers'][strtolower($key)] = trim($val);
                }
            }

            return $map;
        }
    }

    function renderRequest($body, $path, $host, $method = null, $version = null, $headers = null) {

        $method = $method ?: 'GET';
        $version = $version ?: '1.1';

        $length = strlen($body);

        $request[] = "$method $path HTTP/$version";
        $request[] = "Host: $host";
        $request[] = "Content-Length: $length";

        foreach ($headers ?: [] as $key => $val) {

            $request[] = str_replace(' ', '-', ucwords(strtolower(str_replace('_', ' ', $key)))) . ": $val";
        }

        $request[] = '';
        $request[] = $body;

        return implode("\r\n", $request);
    }

    function renderResponse($body, $type = null, $status = null, $version = null, $headers = null) {

        $type = $type ?: 'text/plain';
        $status = $status ?: '200 OK';
        $version = $version ?: '1.1';

        $length = strlen($body);

        $response[] = "HTTP/$version $status";
        $response[] = "Content-Type: $type";
        $response[] = "Content-Length: $length";

        foreach ($headers ?: [] as $key => $val) {

            $response[] = str_replace(' ', '-', ucwords(strtolower(str_replace('_', ' ', $key)))) . ": $val";
        }

        $response[] = '';
        $response[] = $body;

        return implode("\r\n", $response);
    }

    function decodeTransferEncodingChunked($bytes) {

        is_string($bytes) || $bytes = '';

        for ($result = ''; trim($bytes); $bytes = trim($bytes)) {

            $position =	strpos($bytes, "\r\n");
            $length =	hexdec(substr($bytes, 0, $position));
            $result .=	substr($bytes, $position + 2, $length);
            $bytes =	substr($bytes, $position + 2 + $length);
        }

        return $result;
    }
}
