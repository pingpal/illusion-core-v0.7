<?php

namespace Illusion\Core;

class Packet implements Serializable {

    protected $auth;

    protected $public;
    protected $secret;
    protected $nonce;

    protected $data = []; // seems that data must not be invalid at construct (cuz set public´s and nonces will be overwriten at first data set when decodeIfInvalid)
    protected $bytes = null;

    function __construct() {

        $this->auth = AuthUtil::instance();
    }

    function get($key = null, $def = null) {

        $this->decodeIfInvalid();

        return $key ? isset($this->data[$key]) ? $this->data[$key] : $def : $this->data;
    }

    function set($key, $val = null) {

        $this->decodeIfInvalid();

        is_array($key) ? $this->data = $key : $this->data[$key] = $val;

        $this->invalidateBytes();
    }

    function del($key = null) {

        $this->decodeIfInvalid();

        if ($key) unset($this->data[$key]); else $this->data = [];

        $this->invalidateBytes();
    }

    function unsetBytes() {

        $this->invalidateBytes();
    }

    function unsetData() {

        $this->invalidateData();
    }

    function unsetPublic() {

        $this->public = null;

        $this->invalidateBytes();
    }

    function unsetNonce() {

        $this->nonce = null;

        $this->invalidateBytes();
    }

    function clearSecret() {

        $this->secret = null;

        $this->invalidateBytes();
    }

    function bytes($bytes = null) {

        if ($bytes === null) {

            $this->encodeIfInvalid();

            return $this->bytes;
        }

        $this->bytes = is_string($bytes) ? $bytes : '';

        $this->invalidateData();
    }

    function data($key = null, $val = null, $def = null) {

        if ($key === null) {

            $this->decodeIfInvalid();

            return $this->data;

        } else if (is_string($key)) {

            if ($val === null) {

                $this->decodeIfInvalid();

                return isset($this->data[$key]) ? $this->data[$key] : $def;

            } else {

                $this->decodeIfInvalid();

                $this->data[$key] = $val;

                $this->invalidateBytes();
            }

        } else {

            $this->data = is_array($key) ? $key : [];

            $this->invalidateBytes();
        }
    }

    function pub($public = null) {

        if ($public === null) {

            $this->decodeIfInvalid();

            return $this->public;

        } else {

            $this->public = $public;

            $this->invalidateBytes();
        }
    }

    function nonce($nonce = null) {

        if ($nonce === null) {

            $this->decodeIfInvalid();

            return $this->nonce;

        } else {

            $this->nonce = $nonce;

            $this->invalidateBytes();
        }
    }

    function secret($secret = null) {

        if ($secret === null) {

            return $this->secret;

        } else {

            $this->secret = $secret;

            $this->invalidateBytes();
        }
    }

    function sign($secret) {

        $this->secret($secret);
    }

    function auth($secret) {

        $this->encodeIfInvalid();

        return $this->auth->auth($this->bytes, $this->public, $secret);
    }

    // make sure you not sign/secret/getSignBytes something you have not auted.
    function getSignedBytes($secret = null) {

        $bytes = $this->bytes();

        if ($secret !== null) {

            if ($this->public) {

                $bytes = $this->auth->sign($bytes, $this->public, $secret, $this->nonce ?: Util::ms());
            }
        }

        return $bytes;
    }

    protected function invalidateBytes() {

        $this->bytes = null;
    }

    protected function invalidateData() {

        $this->data =   null;
        $this->public = null;
        $this->secret = null;
        $this->nonce =  null;
    }

    protected function encodeIfInvalid() {

        if ($this->bytes === null) {

            $this->bytes = $this->auth->encode($this->data);

            if ($this->public && $this->secret) {

                $this->bytes = $this->auth->sign($this->bytes, $this->public, $this->secret, $this->nonce ?: Util::ms());
            }
        }
    }

    protected function decodeIfInvalid() {

        if ($this->data === null) {

            $this->data =   $this->auth->decode($this->bytes);
            $this->public = $this->auth->peekPublic($this->bytes);
            $this->nonce =  $this->auth->peekNonce($this->bytes);
        }
    }

    function serialize() {

        return Util::joinParts([ __CLASS__,  $this->bytes() ]);
    }

    function deserialize($string) {

        $this->bytes(@Util::splitIntoParts($string)[2] ?: '');

        $this->secret = @$parts[1] ?: null;
    }
}
