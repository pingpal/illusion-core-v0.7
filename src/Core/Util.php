<?php

namespace Illusion\Core;

use Closure;
use ArrayAccess;
use Traversable;

use Reflection;
use ReflectionClass;
use ReflectionMethod;

use ReflectionException;
use Exception;

class Util {

	static function bool($p = .5) {

		return mt_rand(0, 99) / 100 < (is_numeric($p) && 0 <= $p && $p <= 1 ? (float) $p : 0);
	}

	static function key($size = 32) {

		return bin2hex(openssl_random_pseudo_bytes($size / 2));
	}

	static function ms() {

		return round(microtime(true) * 1000);
	}

	static function flush($string) {

     	session_id() && session_write_close();

		ob_end_clean();
		header("Connection: close");
		header('Content-Length: ' . strlen($string));

		echo $string;

		flush();
	}

	static function dump($object) {

		ob_start();
		var_dump($object);

		return ob_get_clean();
	}

	static function trimWhitespaces($string) {

		return preg_replace('/^\s+|\s+$/', '', $string);
	}

	static function stripWhitespaces($string) {

		return preg_replace('/\s+/', '', $string);
	}

	static function flattenWhitespaces($string) {

		return preg_replace('/\s+/', "\x20", $string);
	}

	static function normalizeNewlines($string) {

		return preg_replace("/\r?\n|\r/", "\n", $string);
	}

	static function cleanUnicodeString($string) {

		$string = self::cleanUTF8Bytes($string);
		$string = self::normalizeNewlines($string);

		return preg_replace('/[^\r\n\pL\pM\pN\pP\pS\pZ]/u', '', $string);
	}

	static function stringContains($haystack, $needle, $sensitive = false) {

		if ($sensitive) {

			return stripos($haystack, $needle) !== false;

		} else {

			return strpos($haystack, $needle) !== false;
		}
	}

	static function startsWith($haystack, $needle, $sensitive = false) {

		if (!strlen($needle)) {

			return false;
		}

		if ($sensitive) {

			return !strncmp($haystack, $needle, strlen($needle));
		}

		return !strncasecmp($haystack, $needle, strlen($needle));
	}

	static function endsWith($haystack, $needle, $sensitive = false) {

		if (!strlen($needle) || !strlen($haystack)) {

			return false;
		}

		$haystack = substr($haystack, -strlen($needle));

		if (!$sensitive) {

			$haystack =	strtolower($haystack);
			$needle =	strtolower($needle);
		}

		return $haystack === $needle;
	}

	static function joinParts($parts) {

		$string = '';

        foreach ($parts as $part) {

            $string .= strlen($part) . '|';
        }

        $string && $string .= '|';

        foreach ($parts as $part) {

            $string .= $part;
        }

        return $string;
	}

	static function splitIntoParts($string) {

		list($header, $string) = explode('||', $string, 2) + [ '', '' ];

        $result = [];
		$previous = 0;

        foreach (explode('|', $header) as $length) {

            if (!ctype_digit($length) || strlen($string) < $length) {

                break;
            }

			$result[] = substr($string, $previous, $length);

		    $previous += $length;
        }

		return $result;
	}

	static function peekFirstPart($string) {

		return substr($string, strpos($string, '||') + 2, substr($string, 0, strpos($string, '|'))) . '';
	}

	static function tidyHTML($string) {

		$string = self::cleanUnicodeString($string);

		return $string;
	}

	static function escapeHTML($string){

		return htmlspecialchars($string);
	}

	static function stripHTML($string){

		return strip_tags($string);
	}

	static function capitalize($string) {

		$string = strtolower($string);
		$string = ucwords($string);

		return $string;
	}

	static function capitalizeSentence($string) {

		return preg_replace_callback('/([.!?])\s*(\w)/', function ($matches) {

			return strtoupper($matches[1] . ' ' . $matches[2]);

		}, ucfirst(strtolower($string)));
	}

	static function isArray($object) {

		return is_array($object);
	}

	static function isObject($object) {

		return is_object($object);
	}

	static function isCallable($callable) {

		return is_callable($callable);
	}

	static function isAnonymous($anonymous) {

		return $anonymous instanceof Closure;
	}

	static function isIterable($object) {

		return is_array($object) || is_object($object);
	}

	static function hasArrayAccess($object) {

		return is_array($object) || $object instanceof ArrayAccess;
	}

	static function isList($object) {

		if (is_object($object)) {

			return $object instanceof Traversable;

		} else if (is_array($object)) {

			$i = 0;

			foreach ($object as $key => $value) {

				if ($key !== $i++) {

					return false;
				}
			}

			return true;
		}

		return false;
	}

	static function classExists($class) {

		return class_exists($class);
	}

	static function isClass($class, $parent) {

		if (is_string($class)) {

			return strtolower(ltrim($class, '\\')) === strtolower(ltrim($parent, '\\')) || self::isSubclass($class, $parent);
		}

		return is_a($class, $parent);
	}

	static function isSubclass($class, $parent) {

		return is_subclass_of($class, $parent);
	}

	static function isInterface($class, $interface) {

		$interfaces = class_implements($class, true);

		return $interfaces && isset($interfaces[$interface]);
	}

	static function getInheritanceChain($class) {

		if (is_object($class)) {

			$class = get_class($class);
		}

		while ($class) {

			$chain[] = $class;
			$class = get_parent_class($class);
		}

		return $chain;
	}

	static function instantiate($class) {

		$args = array_slice(func_get_args(), 1);

		return self::instantiateArgs($class, $args);
	}

	static function instantiateArgs($class, array $args = []) {

		try {

			$reflection = new ReflectionClass($class);

			return $reflection->newInstanceArgs($args);

		} catch (ReflectionException $e) {

		}

		throw new Exception('bad instantiate');
	}

	static function invoke($object, $method) {

		$args = array_slice(func_get_args(), 2);

		return self::invokeArgs($object, $method, $args);
	}

	static function invokeArgs($object, $method, array $args = []) {

		return self::callArgs([$object, $method], $args);
	}

	static function call($callable) {

		$args = array_slice(func_get_args(), 1);

		return self::callArgs($callable, $args);
	}

	static function callArgs($callable, array $args = []) {

		if (self::isCallable($callable)) {

			return call_user_func_array($callable, $args);
		}

		throw new Exception('bad callable');
	}

	static function listClasses($libroot, $topname = null, $parent = null, $exclude = null) {

		foreach (self::listDirectory($libroot) as $entry) {

			if (preg_match('~/[A-Z][^/]+\\.php$~', $entry)) {

				$entry = ($topname ? $topname . '\\' : '') . str_replace('/', '\\', substr($entry, strlen($libroot) + 1, -4));

				if (!$parent or $exclude ? self::isSubclassOf($entry, $parent) : self::isClass($entry, $parent)) {

					$entries[] = $entry;
				}
			}
		}

		return @$entries ?: [];
	}

	static function getDocComments($class) {

		if (!is_string($class)) {

			throw new Exception('bad class');
		}

		$class = ltrim($class, '\\');

		try {

			$reflection = new ReflectionClass($class);

		} catch (Exception $e) {

			throw new Exception('bad class');
		}

		$array['class'] =		[ $reflection ];
		$array['method'] =		$reflection->getMethods();
		$array['property'] =	$reflection->getProperties();

		foreach ($array as $key => $reflections) {

			$array[$key] = [];

			foreach ($reflections as $reflection) {

				$comment = $reflection->getDocComment();

				if ($comment !== false) {

					preg_match_all('/#\s*(\w+)[ \t]+([^\n]+)/', $comment, $hits, PREG_SET_ORDER);

					foreach ($hits as $hit) {

						$array[$key][ $reflection->getName() ][$hit[1]][] = trim($hit[2]);
					}
				}
			}
		}

		return $array;
	}

	static function getInheritedDocComments($class) {

		$r = self::getInheritanceChain($class);

		for ($i = count($r) -1; $i >= 0; $i--) {

			$b = self::getDocComments($r[$i]);

			if (count($b['class'])) {

				$b['class'] = [$r[0] => current($b['class'])];
			}

			foreach ($b as $type => $array) {

				if (!isset($a[$type])) {

					$a[$type] = [];
				}

				foreach ($array as $name => $array) {

					foreach ($array as $key => $array) {

						if (!isset($a[$type][$name][$key])) {

							$a[$type][$name][$key] = [];
						}

						foreach ($array as $value) {

							if (!in_array($value, $a[$type][$name][$key])) {

								$a[$type][$name][$key][] = $value;
							}
						}
					}
				}
			}
		}

		return $a;
	}

	static function resolvePath($path) {

		// Expands all symbolic links and resolves references to
		// '/./', '/../' and extra '/' characters in the input
		// path and returns the canonicalized absolute pathname.
		//
		// Neither file nor full path must exist.

		$chunks = trim($path, '/');
		$chunks = explode('/', $chunks);

		for ($i = count($chunks); $i > 0; $i--) {

			$real = array_slice($chunks, 0, $i);
			$real = '/' . implode('/', $real);
			$real = @realpath($real);

			if ($real !== false) {

				$fake = array_slice($chunks, $i);

				if (count($fake)) {

					$real .= '/' . implode('/', $fake);
				}

				return $real;
			}
		}

		return $path;
	}

	static function listDirectory($path, $deep = false) {

		$entries = [];
		$handle = @opendir($path);

		if (!$handle) {

			return $entries;
		}

		while (true) {

			$entry = @readdir($handle);

			if (!$entry) {

				break;
			}

			if (!preg_match('/^\.+|_(\.\w+)?$/', $entry)) {

				$full = "$path/$entry";

				if (@is_dir($full)) {

					$list = self::listDirectory($full, $deep);

					if ($deep) {

						$entries[$entry] = $list;

					} else {

						foreach ($list as $entry) {

							$entries[] = $entry;
						}
					}

				} else {

					$entries[] = $deep ? $entry : $full;
				}
			}
		}

		@closedir($handle);

		return $entries;
	}

	static function getFileContents($base, $file = null) {

		$path = $file ? "$base/$file" : $base;

		$string = @file_get_contents($file ? "$base/$file" : $base);

		return is_string($string) ? $string : null;
	}

	static function dir($base, $dir = null, $deep = false) {

		return self::listDirectory($dir ? "$base/$dir" : $base, $deep);
	}

	static function file($base, $file = null) {

		return self::getFileContents($file ? "$base/$file" : $base);
	}

	static function isDir($base, $file = null) {

		$path = $file ? "$base/$file" : $base;

		return @is_dir($path) && @is_readable($path);
	}

	static function isDirWritable($base, $file = null) {

		$path = $file ? "$base/$file" : $base;

		return self::isDir($path) && @is_writable($path);
	}

	static function isFile($base, $file = null) {

		$path = $file ? "$base/$file" : $base;

		return @is_file($path) && @is_readable($path);
	}

	static function isFileWritable($base, $file = null) {

		$path = $file ? "$base/$file" : $base;

		return self::isFile($path) && @is_writable($path);
	}

	static function isExecutable($base, $file = null) {

		$path = $file ? "$base/$file" : $base;

		return self::isFile($path) && @is_executable($path);
	}

	static function open($file, $mode = 'wb', $permissions = 0644) {

		$handle = @fopen($file, $mode) ?: null;

		@chmod($file, $permissions);

		return $handle;
	}

	static function read($file, $length = 255, $mode = 'rb', $permissions = 0644) {

		$handle = self::open($file, $mode, $permissions);

		if (!$handle) {

			return null;
		}

		$string = @fread($handle, $length);

		@fclose($handle);

		return $string;
	}

	static function write($file, $string, $mode = 'wb', $permissions = 0644) {

		$handle = self::open($file, $mode, $permissions);

		if (!$handle) {

			return false;
		}

		$success = @fwrite($handle, $string) === strlen($string);

		@fclose($handle);

		return $success;
	}

	static function unlink($file) {

		return @unlink($file);
	}

	static function rmdir($path) {

		return @rmdir($path);
	}

	static function mkdir($path, $permissions = 0755) {

		$success = @mkdir($path, $permissions);
		$success = @chmod($path, $permissions) && $success;

		return $success;
	}

	static function chmod($path, $permissions = 0644) {

		return @chmod($path, $permissions);
	}

	static function parseHTTPGetRequest($string) {

		if (!preg_match('/^GET\s+(\S+)\s+HTTP/', $string, $hit)) {

			return [];
		}

		list($url, $query) = preg_split('/\?|$/', $hit[1]);

		$url = trim($url, '/');
		$url = strlen($url) ? explode('/', $url) : [];

		$data = self::queryToArray($query);
		$data['&'] = $url;

		return $data;
	}

	static function queryToArray($query, $divide = '&', $assign = '=') {

		$array = [];
		$pairs = explode($divide, $query);

		foreach ($pairs as $pair) {

			$pair = trim($pair);

			if (empty($pair)) {

				continue;
			}

			$pair = explode($assign, $pair, 2);

			if (!isset($pair[1])) {

				$pair[] = '';
			}

			list($key, $value) = $pair;

			$key =		trim($key);
			$value =	trim($value);

			if (!strlen($key)) {

				continue;
			}

			$array[$key] = $value;
		}

		return $array;
	}

	static function arrayToQuery(array $array, $divide = '&', $assign = '=') {

		$query = [];

		foreach ($array as $key => $value) {

			if (is_string($key) && is_string($value)) {

				$key =		trim($key);
				$value =	trim($value);

				$value = str_replace($divide, '', $value);

				if (strlen($value)) {

					$value = $assign.$value;
				}

				if (strlen($key)) {

					$query[] = $key.$value;
				}
			}
		}

		return implode($divide, $query);
	}

	static function arrayToAttributes(array $array) {

		$query = [];

		foreach ($array as $key => $value) {

			if (is_string($key) && is_string($value)) {

				$key =		trim($key);
				$value =	trim($value);

				$value = str_replace('"', '', $value);

				if (strlen($value)) {

					$value = "=\"$value\"";
				}

				if (strlen($key)) {

					$query[] = $key.$value;
				}
			}
		}

		return implode(' ', $query);
	}

	static function cliFormatJSON($json) {

		$json = self::prettyPrintJSON($json);

		$string		= '';
		$outside	= true;

		for ($esc = false, $i = 0; $i <= strlen($json); $i++) {

			$char = substr($json, $i, 1);

			if ($char == '"' && !$esc) {

				$outside = !$outside;
			}

			if (!$outside || strpos(',', $char) === false) {

				$string .= $char;
			}

			$esc = $char == '\\' ? $esc = !$esc : false;
		}

		$string = trim($string, "{}[]");

		$string = preg_replace("/\n\s*\n/", "\n", $string);
		$string = preg_replace("/\n\x20\x20/", "\n", $string);

		$string = preg_replace('/(?<!\\\\)((\\\\\\\\)*)"/', '$1', $string);
		$string = preg_replace('/((\\\\\\\\)*)\\\\(?=")/', '$1', $string);
		$string = preg_replace('/\\\\\\\\/', '\\', $string);

		$string = preg_replace('#((\\\\\\\\)*)\\\\(?=/)#', '$1', $string);
		$string = preg_replace('/\\\\\\\\/', '\\', $string);

		$string = trim($string);

		return $string;
	}

	static function prettyPrintJSON($json) {

		is_string($json) || $json = '';

		$result     = '';
		$pos        = 0;
		$indent  	= "\x20\x20";
		$break		= "\n";
		$outside	= true;

		for ($esc = false, $i = 0; $i <= strlen($json); $i++) {

			$char = substr($json, $i, 1);

			if ($char == '"' && !$esc) {

				$outside = !$outside;

			} else if (($char == '}' || $char == ']') && $outside) {

				$result .= $break;
				$pos--;

				for ($j = 0; $j < $pos; $j++) {

					$result .= $indent;
				}
			}

			$result .= $char;

			if ($outside && $char === ':') {

				$result .= "\x20";
			}

			if (($char == ',' || $char == '{' || $char == '[') && $outside) {

				$result .= $break;

				if ($char == '{' || $char == '[') {

					$pos++;
				}

				for ($j = 0; $j < $pos; $j++) {

					$result .= $indent;
				}
			}

			$esc = $char == '\\' ? $esc = !$esc : false;
		}

		return $result;
	}

	static function pack($num) {

		$str  = chr((($num & 0xF000) >> 12)	+ 97);
		$str .= chr((($num & 0x0F00) >> 8)	+ 97);
		$str .= chr((($num & 0x00F0) >> 4)	+ 97);
		$str .= chr((($num & 0x000F) >> 0)	+ 97);

		return $str;
	}

	static function unpack($str) {

		$num = (ord($str[0]) - 97);
		$num = (ord($str[1]) - 97) | ($num << 4);
		$num = (ord($str[2]) - 97) | ($num << 4);
		$num = (ord($str[3]) - 97) | ($num << 4);

		return $num;
	}

	static function setBitInByteArray($i, $bytes) {

		$bytes[$i >> 3] = $bytes[$i >> 3] | 1 << ($i & 7);

		return $bytes;
	}

	static function unsetBitInByteArray($i, $bytes) {

		$bytes[$i >> 3] = $bytes[$i >> 3] ^ 1 << ($i & 7);

		return $bytes;
	}

	static function getLowestUnsetBitInByteArray($bytes) {

		for ($i = 0; $i < count($bytes) * 8; $i++) {

			if (1 & ~$bytes[$i >> 3] >> ($i & 7)) {

				return ($i >> 3) * 8 + ($i & 7);
			}
		}

		return -1;
	}

	static function numberToBytes($number) {

		for ($bytes = ''; $number; $number >>= 8) {

			$bytes = chr($number & 0xff) . $bytes;
		}

		return $bytes;
	}

	static function bytesToNumber($bytes) {

		for ($number = 0, $i = 0; $i < strlen($bytes); $i++) {

			$number = $number << 8 | ord($bytes[$i]);
		}

		return $number;
	}


	static function testUTF8Bytes($string) {

		// Be aware that when using the "/u" modifier, if your
		// input text contains any bad UTF-8 code sequences, then
		// preg_replace will return an empty string, regardless
		// of whether there were any matches.
		//
	 	// This is due to the PCRE library returning an error
		// code if the string contains bad UTF-8.
		//
		// http://php.net/manual/en/function.preg-replace.php#74037

		return !!preg_match('/.*/u', $string);
	}

	static function cleanUTF8Bytes($string, $printBadUTF8Bytes = false) {

		$output = '';
		$length = strlen($string);

		$x["\xEF\xBF\xBF"] = 1; // U-FFFF
		$x["\xEF\xBF\xBE"] = 1; // U-FFFE

		$x["\xC0\xAF"] =             1;
		$x["\xC1\xBF"] =             1;
		$x["\xC0\x80"] =             1;

		$x["\xE0\x80\xAF"] =         1;
		$x["\xE0\x9F\xBF"] =         1;
		$x["\xE0\x80\x80"] =         1;
		$x["\xED\xA0\x80"] =         1;
		$x["\xED\xAD\xBF"] =         1;
		$x["\xED\xAE\x80"] =         1;
		$x["\xED\xAF\xBF"] =         1;
		$x["\xED\xB0\x80"] =         1;
		$x["\xED\xBE\x80"] =         1;
		$x["\xED\xBF\xBF"] =         1;

		$x["\xF7\xBF\xBF\xBF"] =     1;
		$x["\xF4\x8F\xBF\xBF"] =     1;
		$x["\xF4\x90\x80\x80"] =     1;
		$x["\xF0\x80\x80\xAF"] =     1;
		$x["\xF0\x8F\xBF\xBF"] =     1;
		$x["\xF0\x80\x80\x80"] =     1;

		$x["\xF8\x88\x80\x80\x80"] = 1;
		$x["\xFB\xBF\xBF\xBF\xBF"] = 1;
		$x["\xF8\x80\x80\x80\xAF"] = 1;
		$x["\xF8\x87\xBF\xBF\xBF"] = 1;
		$x["\xF8\x80\x80\x80\x80"] = 1;

		for ($i = 0; $i < $length; $i++) {

			$offset = $i;
			$c = ord($string[$i]);

			if		 ($c < 0x80)			$n=0;	// 0bbbbbbb
			else if	(($c & 0xE0) === 0xC0)	$n=1;	// 110bbbbb
			else if	(($c & 0xF0) === 0xE0)	$n=2;	// 1110bbbb
			else if	(($c & 0xF8) === 0xF0)	$n=3;	// 11110bbb
			else if	(($c & 0xFC) === 0xF8)	$n=4;	// 111110bb

			else	continue;						// no match

			for ($j = ++$n; --$j;) { // n bytes matching 10bbbbbb follow ?

				if ((++$i === $length) || ((ord($string[$i]) & 0xC0) != 0x80)) {

					continue 2;
				}
			}

			$match = substr($string, $offset, $n);

			if (isset($x[$match])) {

				continue;
			}

			if (self::utf8ToUnicode($match) === false) {

				if ($printBadUTF8Bytes) {

					static $hexes = [];

					for ($hex = '', $k = 0; $k < strlen($match); $k++) {

						$hex .= '\x'.strtoupper(substr('0'.dechex(ord($match[$k])), -2));
					}

					if (!isset($hexes[$hex])) {

						$hexes[$hex] = 1;
						$d = str_repeat("\x20", (5 - $n) * 4);

						echo "\$x[\"$hex\"] =$d 1;\n";
					}
				}

				continue;
			}

			$output .= $match;
		}

		return $output;
	}

	/*
	* For the original C++ code, see
	* http://lxr.mozilla.org/seamonkey/source/intl/uconv/src/nsUTF8ToUnicode.cpp
	* http://lxr.mozilla.org/seamonkey/source/intl/uconv/src/nsUnicodeToUTF8.cpp
	*
	* The latest version of this file can be obtained from
	* http://iki.fi/hsivonen/php-utf8/
	*
	* Version 1.0, 2003-05-30
	*/

	/**
	* Takes an UTF-8 string and returns an array of ints representing the
	* Unicode characters. Astral planes are supported ie. the ints in the
	* output can be > 0xFFFF. Occurrances of the BOM are ignored. Surrogates
	* are not allowed.
	*
	* Returns false if the input string isn't a valid UTF-8 octet sequence.
	*/
	static function utf8ToUnicode($str) {

		$mState = 0;     // cached expected number of octets after the current octet
						 // until the beginning of the next UTF8 character sequence
		$mUcs4  = 0;     // cached Unicode character
		$mBytes = 1;     // cached expected number of octets in the current sequence

		$out = array();

		$len = strlen($str);

		for($i = 0; $i < $len; $i++) {

			$in = ord($str{$i});

			if (0 == $mState) {

				// When mState is zero we expect either a US-ASCII character or a
				// multi-octet sequence.

				if (0 == (0x80 & ($in))) {

					// US-ASCII, pass straight through.
					$out[] = $in;
					$mBytes = 1;

				} else if (0xC0 == (0xE0 & ($in))) {

					// First octet of 2 octet sequence
					$mUcs4 = ($in);
					$mUcs4 = ($mUcs4 & 0x1F) << 6;
					$mState = 1;
					$mBytes = 2;

				} else if (0xE0 == (0xF0 & ($in))) {

					// First octet of 3 octet sequence
					$mUcs4 = ($in);
					$mUcs4 = ($mUcs4 & 0x0F) << 12;
					$mState = 2;
					$mBytes = 3;

				} else if (0xF0 == (0xF8 & ($in))) {

					// First octet of 4 octet sequence
					$mUcs4 = ($in);
					$mUcs4 = ($mUcs4 & 0x07) << 18;
					$mState = 3;
					$mBytes = 4;

				} else if (0xF8 == (0xFC & ($in))) {

					/* First octet of 5 octet sequence.
					*
					* This is illegal because the encoded codepoint must be either
					* (a) not the shortest form or
					* (b) outside the Unicode range of 0-0x10FFFF.
					* Rather than trying to resynchronize, we will carry on until the end
					* of the sequence and let the later error handling code catch it.
					*/
					$mUcs4 = ($in);
					$mUcs4 = ($mUcs4 & 0x03) << 24;
					$mState = 4;
					$mBytes = 5;

				} else if (0xFC == (0xFE & ($in))) {

					// First octet of 6 octet sequence, see comments for 5 octet sequence.
					$mUcs4 = ($in);
					$mUcs4 = ($mUcs4 & 1) << 30;
					$mState = 5;
					$mBytes = 6;

				} else {

					/* Current octet is neither in the US-ASCII range nor a legal first
					* octet of a multi-octet sequence.
					*/
					return false;
				}

			} else {

				// When mState is non-zero, we expect a continuation of the multi-octet
				// sequence
				if (0x80 == (0xC0 & ($in))) {

					// Legal continuation.
					$shift = ($mState - 1) * 6;
					$tmp = $in;
					$tmp = ($tmp & 0x0000003F) << $shift;
					$mUcs4 |= $tmp;

					if (0 == --$mState) {

						/* End of the multi-octet sequence. mUcs4 now contains the final
						* Unicode codepoint to be output
						*
						* Check for illegal sequences and codepoints.
						*/

						// From Unicode 3.1, non-shortest form is illegal
						if (((2 == $mBytes) && ($mUcs4 < 0x0080)) ||
							((3 == $mBytes) && ($mUcs4 < 0x0800)) ||
							((4 == $mBytes) && ($mUcs4 < 0x10000)) ||
							(4 < $mBytes) ||
							// From Unicode 3.2, surrogate characters are illegal
							(($mUcs4 & 0xFFFFF800) == 0xD800) ||
							// Codepoints outside the Unicode range are illegal
							($mUcs4 > 0x10FFFF)) {

							return false;
						}

						if (0xFEFF != $mUcs4) {
							// BOM is legal but we don't want to output it
							$out[] = $mUcs4;
						}

						//initialize UTF8 cache
						$mState = 0;
						$mUcs4  = 0;
						$mBytes = 1;
					}

				} else {

					/* ((0xC0 & (*in) != 0x80) && (mState != 0))
					*
					* Incomplete multi-octet sequence.
					*/
					return false;
				}
			}
		}

		return $out;
	}

	/**
	* Takes an array of ints representing the Unicode characters and returns
	* a UTF-8 string. Astral planes are supported ie. the ints in the
	* input can be > 0xFFFF. Occurrances of the BOM are ignored. Surrogates
	* are not allowed.
	*
	* Returns false if the input array contains ints that represent
	* surrogates or are outside the Unicode range.
	*/
	static function unicodeToUtf8($arr) {

		$dest = '';

		foreach ($arr as $src) {

			if ($src < 0) {

				return false;

			} else if ( $src <= 0x007f) {

				$dest .= chr($src);

			} else if ($src <= 0x07ff) {

				$dest .= chr(0xc0 | ($src >> 6));
				$dest .= chr(0x80 | ($src & 0x003f));

			} else if($src == 0xFEFF) {

				// nop -- zap the BOM

			} else if ($src >= 0xD800 && $src <= 0xDFFF) {

				// found a surrogate
				return false;

			} else if ($src <= 0xffff) {

				$dest .= chr(0xe0 | ($src >> 12));
				$dest .= chr(0x80 | (($src >> 6) & 0x003f));
				$dest .= chr(0x80 | ($src & 0x003f));

			} else if ($src <= 0x10ffff) {

				$dest .= chr(0xf0 | ($src >> 18));
				$dest .= chr(0x80 | (($src >> 12) & 0x3f));
				$dest .= chr(0x80 | (($src >> 6) & 0x3f));
				$dest .= chr(0x80 | ($src & 0x3f));

			} else {

				// out of range
				return false;
			}
		}

		return $dest;
	}
}
