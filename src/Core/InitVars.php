<?php

namespace Illusion\Core;

class InitVars {

    protected $vars = [ 'raw' => '', 'args' => [] ];

    function __construct($args = null, $raw = null) {

        $args && $this->vars['args'] = $args;
        $raw && $this->vars['raw'] = $raw;
    }

    function parse($string) {

        $this->vars['args'] = OptParse::instance()->apply($string);
        $this->vars['raw'] = $string;
    }

    function raw() {

        return $this->vars['raw'];
    }

    function args($uri = null) {

        if ($uri) {

            return t($this->vars['args'], $uri);
        }

        return $this->vars['args'];
    }

    function more($args) {

        $this->vars['args'] = array_merge($this->vars['args'], $args);
    }
}
