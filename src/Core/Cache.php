<?php

namespace Illusion\Core;

use Illusion\Config;

class Cache {

    static protected $instance;

    static function instance() {

        return self::$instance ?: self::$instance = new self;
    }

    protected $cache = [];

    protected function __construct() {

        $this->clean();

        if (!Util::isDirWritable(Config::$_WPATH_CACHE)) {

            Util::mkdir(Config::$_WPATH_CACHE);
        }
    }

    function put($key, $val, $ttl = 0) {

        $this->clean();

        $ttl || $ttl = Config::$CACHE_DEFAULT_TTL;

        $key = md5($key);
        $ttl = time() + $ttl;
        $var = var_export($val, true);

        $this->cache[$key] = $val;

        $code = "<?php return $ttl < time() ? null : $var;";

        Util::write(Config::$_WPATH_CACHE . "/$key.php", $code);
    }

    function get($key) {

        $this->clean();
        $key = md5($key);

        return @$this->cache[$key] ?: (@include Config::$_WPATH_CACHE . "/$key.php") ?: null;
    }

    function clean() {

        if (Util::bool(Config::$CACHE_CLEAN_CHANCE)) {

            $this->force();
        }
    }

    function force() {

        foreach (Util::listDirectory(Config::$_WPATH_CACHE) as $file) {

            if (preg_match('/^<\\?php return (\d{10})/', Util::read($file), $hit)) {

                $hit[1] < time() && Util::unlink($file);
            }
        }
    }
}
