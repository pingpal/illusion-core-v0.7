<?php

namespace Illusion\Core\IO;

class Request extends Input {

	protected $vars;

	function __construct($vars) {

		$this->vars = $vars;
	}

	function vars() {

		return $this->vars;
	}

	function args($uri = null) {

		return $this->vars->args($uri);
	}

	function header($key = null) {

		return $this->vars->header($key);
	}

	function method() {

		return $this->vars->method();
	}

	function path() {

		return $this->vars->path();
	}
}
