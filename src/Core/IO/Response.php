<?php

namespace Illusion\Core\IO;

use Illusion\Core\CookieUtil;

class Response extends Output {

    static $STATUS_CODES = [

        100 => 'Continue',
        101 => 'Switching Protocols',
        200 => 'OK',
        201 => 'Created',
        202 => 'Accepted',
        203 => 'Non-Authoritative Information',
        204 => 'No Content',
        205 => 'Reset Content',
        206 => 'Partial Content',
        300 => 'Multiple Choices',
        301 => 'Moved Permanently',
        302 => 'Found',  // 1.1
        303 => 'See Other',
        304 => 'Not Modified',
        305 => 'Use Proxy',
        307 => 'Temporary Redirect',
        400 => 'Bad Request',
        401 => 'Unauthorized',
        402 => 'Payment Required',
        403 => 'Forbidden',
        404 => 'Not Found',
        405 => 'Method Not Allowed',
        406 => 'Not Acceptable',
        407 => 'Proxy Authentication Required',
        408 => 'Request Timeout',
        409 => 'Conflict',
        410 => 'Gone',
        411 => 'Length Required',
        412 => 'Precondition Failed',
        413 => 'Request Entity Too Large',
        414 => 'Request-URI Too Long',
        415 => 'Unsupported Media Type',
        416 => 'Requested Range Not Satisfiable',
        417 => 'Expectation Failed',
        500 => 'Internal Server Error',
        501 => 'Not Implemented',
        502 => 'Bad Gateway',
        503 => 'Service Unavailable',
        504 => 'Gateway Timeout',
        505 => 'HTTP Version Not Supported',
        509 => 'Bandwidth Limit Exceeded',
    ];

    protected $statusCode = 200;
    protected $headersSent = false;

    protected $headers = [];

    protected $write;
    protected $close;
    protected $flush;

    protected $cookie;

    function __construct() {

        $this->cookie = CookieUtil::instance();
    }

    function setStatusCode($code) {

        $this->statusCode = +$code ?: 200;
    }

    function setContentType($type) {

        $this->header('content-type', $type);
    }

    function headersSent($sent = null) {

        if ($sent !== null) {

            $this->headersSent = !!$sent;
        }

        return $this->headersSent;
    }

    function formatHeaderKey($key) {

        return str_replace(' ', '-', ucwords(strtolower(str_replace('-', ' ', $key))));
    }

    function header($key = null, $value = null) {

        if ($key === null) {

            return $this->headers;
        }

        $key = $this->formatHeaderKey($key);

        if ($value === null) {

            return @$this->headers[$key];
        }

        $this->headers[$key][] = t($value, 'text?number');
    }

    function cookie($key, $val, $expires = null, $meta = null) {

        $this->header('Set-Cookie', $this->cookie->build($key, $val, $expires, $meta));
    }

    function status() {

        $code = $this->statusCode;
        $codes = self::$STATUS_CODES;

        $status = "$code";

        if (isset($codes[$code])) {

            $status .= " $codes[$code]";
        }

        return $status;
    }

    function on($verb, $function) {

        'write' == $verb && $this->write = $function;
        'flush' == $verb && $this->flush = $function;
        'close' == $verb && $this->close = $function;
    }

    function write($string) {

        $write = $this->write;
        $write && $write($this, $string);
    }

    function flush() {

        $flush = $this->flush;
        $flush && $flush($this);
    }

    function close() {

        $close = $this->close;
        $close && $close();
    }

    //-- experimental

    function download($bytes, $filename) {

        $this->header('content-type', 'application/octet-stream');
        $this->header('content-disposition', 'attachment; filename="' . $filename . '"');
        $this->header('content-length', strlen($bytes));

        $this->raw($bytes);
    }

    function redirect($location) {

        $this->setStatusCode(302);

        $this->header('location', $location);
    }

    function hangup() {

        // end request (not necessary connection) and continue run
    }
}
