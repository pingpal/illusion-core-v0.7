<?php

namespace Illusion\Core\IO;

class SysIn extends Input {

    protected $vars;
    protected $out;

    function __construct($vars, $out) {

        $this->vars = $vars;
        $this->out = $out;
    }

    function vars() {

        return $this->vars;
    }

    function args($uri = null) {

        return $this->vars->args($uri);
    }

    function more($text = null) {

        if (!defined('STDIN') || feof(STDIN)) {

            return null;
        }

        if ($text) {

            $this->out->put($text);
        }

        $line = fgets(STDIN);

        $this->out->moveUp();

        echo str_repeat(' ', strlen($line)) . "\r";

        return trim($line);
    }
}
