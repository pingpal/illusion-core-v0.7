<?php

namespace Illusion\Core\IO;

/**
* Abstract output
*/
class Output {

	// prepare/validate/format data for output
	function get($object) {

		return $object;
	}

	// buffer data to be send at any moment
	function put($object) {

		$string = $this->get($object);

		$this->write($string);
	}

	// optional put then flush and wrap things up
	function end($object = null) {

		if ($object !== null) {

			$this->put($object);
		}

		$this->flush();
		$this->close();
	}

	// buffer data to be send at any moment
	function raw($string) {

		$this->write($string);
	}

	// actual write to external capabilities (this implementation does nothing)
	function write($string) {

	}

	// flush internal buffers (this implementation does nothing)
	function flush() {

	}

	// close current transfer (this implementation does nothing)
	function close() {

	}
}
