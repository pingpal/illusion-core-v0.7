<?php

namespace Illusion\Core\IO;

/**
* Abstract input
*/
class Input {

	// provide arguments or initial data
	function args() {

		return [];
	}

	// read/request additional data (this implementation does nothing)
	function more() {

	}
}
