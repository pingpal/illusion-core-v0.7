<?php

namespace Illusion\Core\IO;

use Illusion\Core\PacketHelp;

use Illusion\Core\Util;

class JSONResp extends Response {

    protected $vars;
    protected $factory;

    protected $result = true;
    protected $code = 0;

    function __construct($vars, $factory) {

        parent::__construct();

        $this->vars = $vars;
        $this->factory = $factory;

        $this->setContentType('application/json');
    }

    function success($object) {

        $this->end($object);
    }

    function failure($object, $code = 1) {

        $this->result = false;
        $this->code = +$code ?: 1;

        $this->end($object);
    }

    function get($object) {

        $this->result || $data['code'] = $this->code;
        $data['result'] = $this->result ? 'success' : 'failure';
        $data['response'] = $this->tidy($object);

        return $this->makeToOutput($data);
    }

    protected function tidy($object) {

        if (is_string($object)) {

            $object = t($object, 'print');

        } else if (is_array($object) || is_object($object)) {

            foreach ($object as $key => $value) {

                if (is_array($object)) {

                    $object[$key] = $this->tidy($value);

                } else {

                    $object->$key = $this->tidy($value);
                }
            }

        } else if (!is_scalar($object)) {

            $object = null;
        }

        return $object;
    }

    protected function makeToOutput($data) {

        $format = $this->vars->args('format/text');

        if ($format == 'packet') {

            return $this->makeToPacket($data);
        }

        return $this->makeToJson($data);
    }

    protected function makeToPacket($data) {

        $myseq = $this->vars->args('myseq/text+number');

        if ($myseq) {

            $data['myseq'] = $myseq;
        }

        $factory = $this->factory;
        $session = $factory();

        $help = new PacketHelp($session);

        $packet = $help->sign($data);

        $reference = $this->vars->args('reference/text');

        if ($reference) {

            $json = [ $reference => $packet ];
            $json = @json_encode($json);
            $json = Util::prettyPrintJSON($json);

            return $json;
        }

        return $packet;
    }

    protected function makeToJson($data) {

        $data = @json_encode($data);

        if (is_string($data)) {

            $data = Util::prettyPrintJSON($data);

        } else {

            $data = '';
        }

        $callback = $this->vars->args('callback/text');

        if ($callback) {

            $data = "window['$callback'] && window['$callback']($data)";
        }

        return $data;
    }
}
