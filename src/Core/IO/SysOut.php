<?php

namespace Illusion\Core\IO;

use Illusion\Core\RichTTY;

class SysOut extends Output {

    protected $tty;
    protected $exitStatus = 0;

    function __construct() {

        $this->tty = RichTTY::instance();
    }

    function tty() {

        return $this->tty;
    }

    function reset() {

        $this->raw($this->tty->reset());
    }

    function moveUp($n = 1) {

        $this->raw($this->tty->moveUp());
    }

    function setExitStatus($status) {

        $this->exitStatus = +$status ?: 0;
    }

    // function raw($string) {
    //
    //     $this->write($string);
    // }

    function get($object) {

        $object = $this->tty->format($object);
        $object = $this->tty->colorize($object);

        return $object . "\n";
    }

    function write($string) {

        echo $string;
    }

    function close() {

        exit($this->exitStatus);
    }

    function exitOnSigInt($text = null) {

        defined('SIGINT') && pcntl_signal(SIGINT, function () use ($text) { die ("\r$text\n"); }, 0);
    }
}
