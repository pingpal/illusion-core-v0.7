<?php

namespace Illusion\Core\IO;

use Illusion\Core\Util;

class HTMLResp extends Response {

    function get($object) {

        return is_string($object) ? Util::tidyHTML($object) : '';
    }
}
