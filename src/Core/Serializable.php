<?php

namespace Illusion\Core;

interface Serializable {

    public function serialize();

    public function deserialize($string);
}
