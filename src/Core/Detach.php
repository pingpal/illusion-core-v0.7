<?php

namespace Illusion\Core;

use Illusion\Config;

use Exception;

class Detach {

    static protected $instance;

    static function instance() {

        return self::$instance ?: self::$instance = new self;
    }

    function detach($action, $args = null, $log = null, $aid = false) {

        $args && is_array($args) || $args = [];

        $action = Resolve::normalizeAction($action);

        $command =  Config::$PATH_EXE . ' ' . Config::$_PATH_DETACH . ' ' . escapeshellarg($action);

        $log && $defs['log'] = $log;
        $aid && $defs['aid'] = 'true';

        $args = array_merge(@$defs ?: [], $args);

        foreach ($args as $key => $val) {

            $key = preg_replace('/\s+/', '', $key);
            $val = preg_replace('/\s+/', '', $val);

            if ($key && $val) {

                $command .= ' ' . escapeshellarg("--$key=$val");
            }
        }

        return `$command`;
    }

    function apply($log = null, $aid = false) {

        if (php_sapi_name() !== 'cli') {

            throw new Exception('not cli');
        }

        if (!function_exists('posix_setsid')) {

            throw new Exception('no posix');
        }

        if (!defined('SIGHUP')) {

            throw new Exception('no process control');
        }

        if (ini_get('safe_mode')) {

            throw new Exception('safe mode is on');
        }

        if ($log) {

            Util::isFile($log) || @fclose(Util::open($log));

            if (!@is_writable($log)) {

                throw new Exception('log dir not writable');
            }
        }

        @chdir('/');
        @umask(0);

        $pid = @pcntl_fork();

        if ($pid < 0) {

            throw new Exception('could not fork once');

        } else if ($pid) {

            exit;
        }

        $sid = @posix_setsid();

        if ($sid < 0) {

            throw new Exception('could not become session leader');
        }

        $pid = @pcntl_fork();

        if ($pid < 0) {

            throw new Exception('could not fork twice');

        } else if ($pid) {

            exit;
        }

        foreach ([ SIGTSTP, SIGTTOU, SIGTTIN, SIGHUP ] as $signal) {

            if (!@pcntl_signal($signal, SIG_IGN)) {

                throw new Exception('could not ignore signal');
            }
        }

        if (!@pcntl_signal(SIGTERM, function () { exit; })) {

            throw new Exception('could not handle terminate');
        }

        if (!@fclose(STDIN) || !@fclose(STDOUT) || !@fclose(STDERR)) {

            throw new Exception('could not close file descriptors');
        }

        static $in, $out, $err;

        $in =	@fopen('/dev/null', 'r');
        $out =	@fopen($log ?: '/dev/null', 'wb');
        $err =	@fopen('php://stdout', 'wb');

        if (!$in || !$out || !$err) {

            throw new Exception('could not mute file descriptors');
        }

        sleep(1);

        while ($aid) {

            $pid = @pcntl_fork();

            if ($pid < 0) {

                throw new Exception('could not fork thrice');

            } else if ($pid) {

                echo "angel " . getmypid() . " with parent " . posix_getppid() . " active!\n";

                pcntl_wait($status);

                $return = pcntl_wexitstatus($status);
                $signal = pcntl_wtermsig($status);

                if ($status) {

                    echo "angel: child crashed (return code: $return, kill signal: $signal), reviving..\n";

                    continue;

                } else {

                    echo "angel: child died gracefully!\n";

                    exit;
                }
            }

            break;
        }

        register_shutdown_function(function () {

            echo "shutdown..\n";
        });

        pcntl_signal(16, function () {

            echo "OUCH!!\n";
        });

        echo "child " . getmypid() . " detached with parent " . posix_getppid() . "\n";

        sleep(1);
    }
}
