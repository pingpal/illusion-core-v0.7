<?php

namespace Illusion\Core;

use DateTime;

class CookieUtil {

    static protected $instance;

    static function instance() {

        return self::$instance ?: self::$instance = new self;
    }

    function build($key, $val, $expires = null, $meta = null) {

        $expires || $expires = strtotime('+1 week');

        if (is_numeric($expires)) {

            $dt = new DateTime();
            $dt->setTimestamp($expires);
            $expires = $dt->format(DateTime::COOKIE);
        }

        $key = t($key, 'text');
        $val = t($val, 'text');
        $val = urlencode($val);

        $s = "$key" . ($val ? "=$val" : '') . "; Expires=$expires";

        @$meta['path'] || $meta['path'] = '/';

        foreach ($meta ?: [] as $key => $val) {

            $key = str_replace(' ', '', ucwords(strtolower(preg_replace('/[-_]/', ' ', trim($key)))));
            $val = t($val, 'text?number');
            $s .= "; $key" . ($val ? "=$val" : '');
        }

        return $s;
    }

    function parse($cookie) {

        foreach (explode(';', $cookie) as $chunk) {

            $var = explode('=', trim($chunk), 2);

            if (count($var) == 1) {

                $val = true;

            } else {

                $val = urldecode(trim($var[1], " \n\r\t\0\x0B\""));
            }

            $cookies[ trim($var[0]) ] = $val;
        }

        return @$cookies ?: [];
    }
}
