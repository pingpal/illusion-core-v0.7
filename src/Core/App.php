<?php

namespace Illusion\Core;

use Illusion\Config;

use ReflectionClass;
use ReflectionException;
use ErrorException;
use Exception;

class App {

	protected $scan;
	protected $context;

	function __construct() {

		$this->init();
	}

	function init() {

		// Find root, document root & top level namespace
		$docroot = preg_replace('~/(vendor)/.*$~', '', __DIR__);
		$libroot = preg_replace('~/(src|lib)/.*$~', '/$1', __DIR__);
		$topname = preg_replace('~^.*/(src|lib)/~', '', __DIR__);
		$topname = preg_replace('~\\\\~', '\\\\\\\\', $topname);
		$topname = preg_replace("~\\\\?$topname$~", '', __NAMESPACE__);

		$this->scan = [ $topname => $libroot ];

		$json = (array) @json_decode(@file_get_contents($docroot . '/illusion.json'));

		$scan = [];

		foreach (@$json['locations'] ?: [] as $key => $val) {

			is_string($val) && $scan[$key] = $val;
		}

		include_once "$docroot/vendor/autoload.php";

		// Register handlers
		set_error_handler			([ $this, 'error'		]);
		set_exception_handler		([ $this, 'exception'	]);
		register_shutdown_function	([ $this, 'shutdown'	]);
		spl_autoload_register		([ $this, 'autoload'	], true, true);

		// Ensures shutdown function get called on sigint
		defined('SIGINT') && @pcntl_signal(SIGINT, function () { exit; });

		// Error reporting
		// error_reporting(-1);
		// error_reporting(E_ALL | E_STRICT);
		error_reporting(E_ALL & ~ E_ERROR);

		ini_set('html_errors', 0);
		ini_set('display_errors', 0);
		ini_set('log_errors', 0);
		ini_set('error_log', 'syslog');
		ini_set('xdebug.max_nesting_level', 999);
		ini_set('xdebug.overload_var_dump', 0);

		$CWD = getcwd();

		set_include_path(".:$libroot");
		//chdir($libroot);

		// Set timezone
		date_default_timezone_set(Config::$TIMEZONE);

		// Start session if not in command line
		php_sapi_name() == 'cli' || session_start();

		$configEdit = ConfigEdit::instance();

		// Complement config
		$configEdit->setup('Illusion\Config', [

			'IF' => php_sapi_name() == 'cli' ? 'cli' : 'web',

			'ROOT' => $libroot,
			'LIBROOT' => $libroot,
			'DOCROOT' => $docroot,
			'TOPNAME' => $topname,
			'INITDIR' => $CWD,
		]);

		// Stash pure config
		$configEdit->stash('Illusion\Config');

		$configEdit->mergeConfigs('Illusion\Config', 'Illusion\Writable\Custom');
		$configEdit->resolvePaths('Illusion\Config');

		$configEdit->dealWithScanList('Illusion\Config', $scan);

		$this->scan = Config::$SCAN;

		$files = $configEdit->getWithPrefix('Illusion\Config', '_load_');

		foreach ($files as $path) {

			$this->insert($path);
		}

		$doc = DocComments::instance();

		foreach ($this->scan as $topname => $libroot) {

			$doc->addSource($libroot, $topname);
		}

		// Create writable directories
		foreach ([ 'CACHE', 'TEMP', 'DATA', 'LOG' ] as $dir) {

			Util::isDirWritable(Config::${"_WPATH_$dir"}) || Util::mkdir(Config::${"_WPATH_$dir"});
		}

		new CGIReqVars();
	}

	/*
	* Set or get runtime context
	*/
	function context($context = null) {

		return func_num_args() ? $this->context = $context : $this->context;
	}

	/*
	 * Error handler, throws error exception unless fatal
	 */
	function error($code, $text, $file, $line) {

		if (error_reporting() > 0 && $code > 1) {

			throw new ErrorException($text, /*$code*/ 0, 0, $file, $line);
		}
	}

	/*
	 * Exception handler, escapes on all uncatched exceptions
	 */
	function exception(Exception $e) {

		$this->escape($e);
	}

	/*
	 * Shutdown handler, escapes on fatal errors
	 */
	function shutdown() {

		$e = error_get_last();

		if ($e !== null && $e['type'] & (1 | 4 | 16 | 64)) {

			$this->escape(new ErrorException($e['message'], 1, 0, $e['file'], $e['line']));
		}
	}

	/*
	 * Escapes via context if set, else print error message
	 */
	function escape(Exception $e) {

		if (!$this->context || !$this->context->escape($e)) {

			echo $e->getMessage() . (Config::$DEBUG ? ' in ' . $e->getFile() . ' on '. $e->getLine() : '') . "\n";
		}
	}

	/*
	* Include files
	*/
	function insert($file) {

		try {

			include_once $file;

		} catch (Exception $e) {

			$trace = @$e->getTrace()[1];

			if (@$trace['class'] != 'Illusion\Core\App' || @$trace['function'] != 'insert') {

				throw $e;
			}
		}
	}

	/*
	 * Class loader
	 */
	function autoload($class) {

		foreach ($this->scan as $topname => $libroot) {

			if (!strncmp(ltrim($class, '\\'), $topname, strlen($topname))) {

				$path = preg_replace('~\\\\~', '\\\\\\\\', $topname);
				$path = preg_replace("~^\\\\?$path\\\\?~", '', $class);
				$path = preg_replace('~\\\\~', '/', $path);
				$path = $libroot . '/' . $path . '.php';

				$this->insert($path);

				break;
			}
		}

		try {

			$reflection = new ReflectionClass($class);

		} catch (ReflectionException $e) {

			return;
		}

		try {

			$reflection->getMethod('initialize')->invoke(null);

		} catch (ReflectionException $e) {

		}
	}
}
