<?php

namespace Illusion\Core;

use Illusion\Config;

Class Resolve {

    static function normalizeAction($string) {

        $string = trim($string);
        $string = str_replace(':', '.', $string);

        return $string;
    }

    static function namespaceToAction($string) {

        $string = trim($string);
        $string = trim($string, '\\');
        $string = str_replace('\\', '.', $string);

        return $string;
    }

    static function actionToNamespace($string) {

        $string = self::normalizeAction($string);
        $string = str_replace('.', '\\', $string);

        return $string;
    }

    static function callableToAction($string) {

        $string = ltrim($string, '\\');
        $string = preg_replace('/(\\\\|::)/', '.', $string);

        return $string;
    }

    static function actionToCallable($string) {

        $string = self::callableToAction($string);
        $string = self::normalizeAction($string);
        $length = substr_count($string, '.') -1;
        $string = preg_replace('/\./', '\\\\', $string, $length);
        $string = str_replace('.', '::', $string);

        return $string;
    }

    static function stripNamespaceFromClass($class) {

        $class = ltrim($class, '\\');
        $path = explode('\\', $class);
        $class = array_slice($path, -1);

        return $class[0];
    }

    static function stripClassFromNamespace($class) {

        $class = ltrim($class, '\\');
        $path = explode('\\', $class);
        $base = array_slice($path, 0, -1);
        $base = implode('\\', $base);

        return $base;
    }

    static function namespaceToFilePath($class) {

        foreach (Config::$SCAN as $topname => $libroot) {

            if (!strncmp(ltrim($class, '\\'), $topname, strlen($topname))) {

                $path = preg_replace('~\\\\~', '\\\\\\\\', $topname);
                $path = preg_replace("~^\\\\?$path\\\\?~", '', $class);
                $path = preg_replace('~\\\\~', '/', $path);

                return $libroot . '/' . $path . '.php';
            }
        }
    }

    static function namespaceToBasePath($class) {

        $class = ltrim($class, '\\');
        $class = explode('\\', $class);
        $class = array_slice($class, 0, -1);
        $class = implode('\\', $class);

        foreach (Config::$SCAN as $topname => $libroot) {

            if (!strncmp(ltrim($class, '\\'), $topname, strlen($topname))) {

                $path = preg_replace('~\\\\~', '\\\\\\\\', $topname);
                $path = preg_replace("~^\\\\?$path\\\\?~", '', $class);
                $path = preg_replace('~\\\\~', '/', $path);

                return rtrim($libroot . '/' . $path, '/');
            }
        }
    }

    static function actionToFilePath($string, $ext = null) {

        $string = trim($string);
        $string = preg_replace('/(?<!!)((!!)*)\\./', '$1/', $string);
        $string = preg_replace('/((!!)*)!(?=\\.)/', '$1', $string);
        $string = preg_replace('/!!/', '!', $string);

        foreach (Config::$SCAN as $topname => $libroot) {

            $topname = str_replace('\\', '/', $topname);

            if (!strncmp(ltrim($string, '/'), $topname, strlen($topname))) {

                $string = preg_replace("~^/?$topname/?~", '', $string);
                $string = $libroot . '/' . $string;

                if ($ext && strtolower(".$ext") != strtolower(substr($string, -strlen(".$ext")))) {

                    $string .= ".$ext";
                }

                return $string;
            }
        }
    }
}
