<?php

namespace Illusion\Core;

use Illusion\Config;

use Exception;

class DataStore {

    static protected $instance;

    static function instance() {

        return self::$instance ?: self::$instance = new self;
    }

    function resolve($name = null) {

        return Config::$_WPATH_DATA . '/' . md5('illusion.datastore') . ($name ? '/' . md5($name) : '');
    }

    function save($name, $data) {

        $json = @json_encode($data);

        if ($data === null) {

            return $this->delete($name);
        }

        if (!is_string($json)) {

            return false;
        }

        Util::isDirWritable($this->resolve()) || Util::mkdir($this->resolve());

        if (!Util::write($this->resolve($name), $json)) {

            return false;
        }

        return true;
    }

    function load($name) {

        $path = $this->resolve($name);
        $json = Util::file($path);

        if (!is_string($json)) {

            return null;
        }

        return @json_decode($json, true);
    }

    function delete($name) {

        $path = $this->resolve($name);

        return Util::unlink($path);
    }
}
