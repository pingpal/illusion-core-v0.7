<?php

namespace Illusion\Core;

use Illusion\Config;

class ReqVars extends InitVars {

    protected $vars;

    function __construct($vars) {

        $url = @$vars['url'];

        $exts = Config::$REQUEST_EXTS;

        $exts = $exts != 'any' ? $exts ? explode('|', $exts) : [] : ['[^\/]+'];

        $pattern = '/[^\/]+\.(' . implode('|', $exts) . ')(\/|$)/';

        list($url, $query) = preg_split('/\\?/', $url, 2) + ['', ''];
        list($path, $info) = preg_split($pattern, $url, 2) + ['', ''];

        preg_match($pattern, $url, $matches);

        $file = @$matches[0] . '';

        $path = trim($path, '/');
        $file = trim($file, '/');
        $info = trim($info, '/');
        $info && $info = '/' . $info;

        $vars['path'] = $path;
        $vars['file'] = $file;
        $vars['info'] = $info;
        $vars['query'] = $query;

        $vars['headers'] = t($vars, 'headers/text+number?digit-rfn');
        $vars['method'] = t($vars, 'method/text-l');
        $xmethod = t($vars, 'headers.x-http-method/text-l');

        if ($vars['method'] == 'post') {

            switch ($xmethod) {

                case 'delete' : $vars['method'] = $xmethod; break;
                case 'put' :    $vars['method'] = $xmethod; break;
            }
        }

        $vars['body'] = t($vars, 'body/text-m');

        $json = @json_decode($vars['body']);

        if (!@$vars['post']) {

            if (is_array($json) || is_object($json)) {

                $vars['post'] = (array) $json;

            } else {

                parse_str($vars['body'], $vars['post']);
            }
        }

        if (!@$vars['get']) {

            parse_str($vars['query'], $vars['get']);
        }

        $vars['args'] = array_merge($vars['post'], $vars['get']);

        $this->vars = $vars;
    }

    // Scheme name (protocol) e.g. http://
    function scheme($include = false) {

        $proto = t($this->vars, 'headers.x-forwarded-proto/text');
        $proto = strtolower(trim(explode(',', $proto)[0]));

        if ($proto) {

            $scheme = $proto == 'https' ? 'https' : 'http';

        } else {

            $https = @$this->vars['https'];
            $scheme = $https && $https != 'off' ? 'https' : 'http';
        }

        $include && $scheme .= '://';

        return $scheme;
    }

    // Host name e.g. sub.example.com
    function host() {

        $hhost = t($this->vars, 'headers.host/text');
        $xhost = t($this->vars, 'headers.x-forwarded-host/text');
        $xhost = strtolower(trim(explode(',', $xhost)[0]));

        $host           = explode(':', $xhost)[0];
        $host || $host  = t($this->vars, 'host/text');
        $host || $host  = explode(':', $hhost)[0];

        return $host;
    }

    // Port segment e.g. :1337 (nothing if standard port)
    function port($include = false) {

        $host   = t($this->vars, 'headers.x-forwarded-host/text');
        $port   = t($this->vars, 'headers.x-forwarded-port/text?number');
        $proto  = t($this->vars, 'headers.x-forwarded-proto/text');

        $host   = strtolower(trim(explode(',', $host)[0]));
        $port   = strtolower(trim(explode(',', $port)[0]));
        $proto  = strtolower(trim(explode(',', $proto)[0]));

        $port           = t($port, 'positive?digit');
        $port || $port  = t(@explode(':', $host)[1], 'positive?digit');

        $scheme = $proto == 'https' ? 'https' : 'http';

        if ($port) {

            if ($port == 80 && $scheme == 'http' || $port == 443 && $scheme == 'https') {

                return null;
            }

            $include && $port = ':' . $port;

            return $port;
        }

        $host = t($this->vars, 'headers.host/text');

        $port           = t($this->vars, 'port/positive?digit');
        $port || $port  = t($this->vars, 'headers.port/positive?digit');
        $port || $port  = t(@explode(':', $host)[1], 'positive?digit');

        $https = strtolower(@$this->vars['https']);
        $scheme = $https && $https != 'off' ? 'https' : 'http';

        if ($port) {

            if ($port == 80 && $scheme == 'http' || $port == 443 && $scheme == 'https') {

                return null;
            }

            $include && $port = ':' . $port;

            return $port;
        }
    }

    // File path, relative to document root e.g. /file/path
    function path($include = false, $array = false) {

        $path = $this->vars['path'];

        if ($array) {

            $path = $path ? explode('/', $path) : [];

        } else if ($include) {

            $path = '/' . $path;
        }

        return $path;
    }

    // File name e.g. index.php
    function file() {

        return $this->vars['file'];
    }

    // Path info e.g. /path/info in index.php/path/info
    function pathInfo() {

        return $this->vars['info'];
    }

    // Query string e.g. ?query=string
    function queryString($include = false, $array = false) {

        $query = $this->vars['query'];

        if ($array) {

            $query = Util::queryToArray($query);

        } else if ($include) {

            $query = '?' . $query;
        }

        return $query;
    }

    // Http request method
    function method() {

        return $this->vars['method'];
    }

    // Http request headers
    function header($key = null) {

        if ($key === null) {

            return $this->vars['headers'];
        }

        return @$this->vars['headers'][$key];
    }

    // Http request body
    function body() {

        return $this->vars['body'];
    }

    function post() {

        return $this->vars['post'];
    }

    function get() {

        return $this->vars['get'];
    }

    // Request file path and query
    function self() {

        return $this->url($this->path(), $this->file(), $this->queryString(), $this->pathInfo());
    }

    // Request home url
    function home() {

        return $this->url(null, null, null, null, $this->scheme(), $this->host(), $this->port());
    }

    // Full request url
    function full() {

        $path = $this->path();
        $file = $this->file();
        $query = $this->queryString();
        $info = $this->pathInfo();
        $host = $this->host();
        $port = $this->port();
        $scheme = $this->scheme();

        return $this->url($path, $file, $query, $info, $scheme, $host, $port);
    }

    // Build URL from current request
    function current($path = null, $file = null, $query = null, $info = null, $scheme = null, $host = null, $port = null) {

        $path   || $path = $this->path();
        $file   || $file = $this->file();
        $query  || $query = $this->queryString();
        $info   || $info = $this->pathInfo();
        $host   || $host = $this->host();
        $port   || $port = $this->port();
        $scheme || $scheme = $this->scheme();

        return $this->url($path, $file, $query, $info, $scheme, $host, $port);
    }

    // Build URL
    function url($path = null, $file = null, $query = null, $info = null, $scheme = null, $host = null, $port = null) {

        is_array($path) && $path = implode('/', $path);
        is_array($info) && $info = implode('/', $info);
        is_array($query) && $query = Util::arrayToQuery($query);

        $scheme && $scheme = rtrim($scheme, ':/') . '://';

        $path = trim($path, '/');
        $path && $path .= '/';

        $info = trim($info, '/');
        $info && $info = '/' . $info;

        $port && $port = ':' . ltrim($port, ':');
        $query && $query = '?' . ltrim($query, '?');

        $url = $scheme . $host . $port;
        ($path || $file) && $url .= '/';
        $url .= $path . $file . $info . $query;

        return $url;
    }

    function infoOrPath() {

        return $this->pathInfo() ?: $this->path(true);
    }

    function packet() {

        preg_match('~^/packet/(.+)~', $this->infoOrPath(), $match);

        return $this->args('packet/text') ?: @$match[1];
    }
}
