<?php

function t() {

    static $t;

    $t || $t = \Illusion\Core\Tweak::factory();

    return call_user_func_array($t, func_get_args());
}

function l() {

    $l = \Illusion\Core\Launch::instance();

    return call_user_func_array([ $l, 'apply' ], func_get_args());
}

function d() {

    $d = \Illusion\Core\Detach::instance();

    return call_user_func_array([ $d, 'detach' ], func_get_args());
}
