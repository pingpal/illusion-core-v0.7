<?php

namespace Illusion\Core;

class Router {

    protected $vars;

    protected $hit;
    protected $pre;

    function __construct($vars) {

        $this->vars = $vars;
    }

    function vars() {

        return $this->vars;
    }

    function prefix($pre = null) {

        return !func_num_args() ? $this->pre : $this->pre = $pre ? '/' . trim($pre, '/') : null;
    }

    function on($pattern, $context, $callback = null, $keys = null) {

        if ($this->hit) {

            return;
        }

        if (!$callback) {

            $callback = $context;
            $context = null;
        }

        $path = $this->vars->infoOrPath();
        $method = $this->vars->method();

        if (preg_match('~^([a-z|]+):(.*)$~', $pattern, $hits)) {

            $methods = explode('|', $hits[1]);
            $pattern = $hits[2];
        }

        if ($this->pre && !strncasecmp($path, $this->pre, strlen($this->pre))) {

            $path = substr($path, strlen($this->pre));
        }

        if (!$pattern || preg_match("~^$pattern~i", $path, $hits)) {

            if (!@$methods || in_array(strtolower($method), $methods)) {

                $data = array_slice($hits, 1);

                if (is_string($callback)) {

                    $formater = function ($h) use ($hits) {

                        return isset($hits[$h[1]]) ? $hits[$h[1]] : $h[0];
                    };

                    $callback = preg_replace_callback('/\\$(\d+)/', $formater, $callback);
                }

            } else {

                return;
            }

        } else {

            return;
        }

        $this->hit = true;

        if (!is_string($callback) && Util::isCallable($callback)) {

            Util::callArgs($callback, @$data ?: []);

        } else {

            $args = $this->vars->args();
            $keys = $keys ? explode(',', $keys) : [];

            for ($i = 0; $i < min(count($keys), count(@$data)); $i++) {

                $args[trim($keys[$i])] = $data[$i];
            }

            l($callback, $args, $context);
        }
    }
}
