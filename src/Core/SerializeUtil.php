<?php

namespace Illusion\Core;

use Illusion\Core\Util;

class SerializeUtil {

    static function spawn($string) {

        $class = Util::peekFirstPart($string);

        if (Util::isClass($class, 'Illusion\Core\Serializable')) {

            $object = new $class;

            $object->deserialize($string);

            return $object;
        }
    }
}
