<?php

namespace Illusion\Markup;

class Engine {

	const S_TYPE =		1;
	const S_HEAD =		2;
	const S_KEY =		3;
	const S_VALUE =		4;
	const S_SINGLE =	5;
	const S_DOUBLE =	6;
	const S_CLOSE =		7;
	const S_MATCH =		8;
	const S_MISS =		9;

	const T_LEFT =		'{';
	const T_RIGHT =		'}';
	const T_START =		':';
	const T_CLOSE =		'/';
	const T_ASSIGN =	'=';
	const T_SINGLE =	"'";
	const T_DOUBLE =	'"';
	const T_DIVIDE =	'._';

	static protected $instance;

	static function instance() {

		return self::$instance ?: self::$instance = new self;
	}

	function parse($string) {

		is_string($string) || $string = '';

		$list = new Nodes();
		$string = $this->scan($string, $list);

		$node = new Node($string);
		$node->position = strlen($string) + 1;

		$list->add($node);
		$this->build($list->get());

		return $node;
	}

	function render($node, $inflate) {

		$node = $node->copy();

		foreach ($node->kids as $kid) {

			$kid = $kid->copy();

			foreach ($kid->args as $key => $value) {

				$kid->args[$key] = $this->render($value, $inflate);
			}

			$string = $inflate->render($kid);

			if (is_string($string) && strlen(trim($string))) {

				$node->text = substr_replace($node->text, $string, $kid->position, 0);
			}
		}

		return $node->text;
	}

	protected function build($nodes) {

		for ($i = 0; $i < count($nodes) -1; $i++) {

			$offset = 0;

			for ($j = $i +1; $j < count($nodes); $j++) {

				$offset += $nodes[$j -1]->length;

				if ($nodes[$i]->position < $nodes[$j]->position + $offset) {

					$nodes[$i]->position -= $nodes[$j]->offset + $offset;
					$nodes[$j]->kids[] = $nodes[$i];

					break;
				}
			}
		}
	}

	protected function scan($string, $list, $i = 0) {

		if (strlen($string) <= $i) {

			return $string;
		}

		$head = self::T_LEFT;
		$i = strpos($string, $head, $i);

		if ($i === false) {

			return $string;
		}

		$type = '';
		$key =	null;
		$name =	null;
		$text =	null;
		$args =	[];

		$state = self::S_TYPE;

		for ($j = $i + strlen($head); $j < strlen($string); $j++) {

			$char = $string[$j];

			switch ($state) {

				case self::S_TYPE :

					if ($char === self::T_START && $type !== '') {

						$state = self::S_HEAD;

					} else if (ctype_alpha($char)) {

						$type .= $char;

					} else {

						$state = self::S_MISS;
					}

					break;

				case self::S_HEAD :

					if (strpos(self::T_DIVIDE, $char) !== false || ctype_alpha($char)){

						$state = self::S_KEY;
						$key = $char;

					} else if ($key !== null && $char === self::T_ASSIGN) {

						$state = self::S_VALUE;

					} else if ($name !== null && $char === self::T_CLOSE) {

						$state = self::S_CLOSE;

					} else if ($name !== null && $char === self::T_RIGHT) {

						$state = self::S_MATCH;
						$text = '';

					} else if (!ctype_space($char)){

						$state = self::S_MISS;
					}

					break;

				case self::S_KEY :

					if (strpos(self::T_DIVIDE, $char) !== false || ctype_alpha($char)) {

						$key .= $char;

					} else {

						if ($name === null) {

							$name = $key;
							$key = null;

						} else {

							$args[$key] = '';
						}

						$state = self::S_HEAD;
						$j--;
					}

					break;

				case self::S_VALUE :

					if ($char === self::T_SINGLE) {

						$state = self::S_SINGLE;

					} else if ($char === self::T_DOUBLE) {

						$state = self::S_DOUBLE;

					} else if (!ctype_space($char)){

						$state = self::S_MISS;
					}

					break;

				case self::S_SINGLE :

					if ($char !== self::T_SINGLE) {

						$args[$key] .= $char;

					} else {

						$state = self::S_HEAD;
						$key = null;
					}

					break;

				case self::S_DOUBLE :

					if ($char !== self::T_DOUBLE) {

						$args[$key] .= $char;

					} else {

						$state = self::S_HEAD;
						$key = null;
					}

					break;

				case self::S_CLOSE :

					if ($char === self::T_RIGHT) {

						$state = self::S_MATCH;

					} else if (!ctype_space($char)) {

						$state = self::S_MISS;
					}

					break;

				default :

					break 2;
			}
		}

		if ($state !== self::S_MATCH) {

			return $this->scan($string, $list, $i +1);
		}

		$string = $this->scan($string, $list, $j);
		$offset = $j;

		if ($text !== null) {

			if (strlen($string) <= $i) {

				return $string;
			}

			$back = self::T_LEFT.self::T_CLOSE.$type.self::T_RIGHT;
			$k = strpos($string, $back, $j);

			if ($k === false) {

				return $string;
			}

			$text = substr($string, $j, $k - $j);
			$j = $k + strlen($back);
		}

		foreach ($args as $key => $value) {

			$args[$key] = $this->parse($value);
		}

		$node = new Node($text, $type, $name, $args);

		$node->position = $j;
		$node->offset = $offset;
		$node->length = $j - $i;

		$list->add($node);

		$result = substr($string, 0, $i);

		if ($j < strlen($string)) {

			$result .= substr($string, $j);
		}

		return $result;
	}
}
