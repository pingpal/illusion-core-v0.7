<?php

function inflate() {

    $m = \Illusion\Markup\Inflate::instance();

    return call_user_func_array([ $m, 'file' ], func_get_args());
}
