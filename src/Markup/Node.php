<?php

namespace Illusion\Markup;

class Node {

	public $text;
	public $type;
	public $name;
	public $args;

	public $position =	0;
	public $offset =	0;
	public $length =	0;

	public $kids = [];

	function __construct($text, $type = null, $name = null, $args = null) {

		$this->text = $text;
		$this->type = $type;
		$this->name = $name;
		$this->args = $args;
	}

	function copy() {

		$node = new Node($this->text, $this->type, $this->name, $this->args);

		$node->position =	$this->position;
		$node->offset =		$this->offset;
		$node->length =		$this->length;
		$node->kids =		$this->kids;

		return $node;
	}
}
