<?php

namespace Illusion\Markup;

use Illusion\Core\Util;

class Logics {

	protected $engine;

	protected $data;
	protected $set = [];

	function __construct($engine, $data = null, $set = null) {

		$this->engine = $engine;
		$this->data = $data;

		if ($set) {

			$this->set = $set;

		} else if (Util::isIterable($data)) {

			$array[] = $data;

			while (list($key, $value) = each($array)) {

				foreach ($value as $key => $value) {

					if (!empty($value)) {

						$this->set[$key] = true;

					} else if (Util::isIterable($value)) {

						$array[] = $value;
					}
				}
			}
		}
	}

	function copy($data = null) {

		return new Logics($this->engine, $data ?: $this->data, $this->set);
	}

	function render($node) {

		$name = $node->name;
		$args = $node->args;
		$text = $node->text;

		switch ($node->type) {

			case 'tmpl' :

				if ($name == '.') {

					if (Util::isAnonymous($this->data)) {

						return Util::call($this->data, $args);
					}

					return t($this->data, @$args['tweak'] ?: 'text-m', '');
				}

				$context = null;

				$data =		Util::isIterable($this->data) ? (array) $this->data : [];
				$methods =	Util::isObject($this->data) ? get_class_methods($this->data) : [];

				if (isset($args['exists'])) {

					if (isset($this->set[$name]) xor isset($args['invert'])) {

						return $this->engine->render($node, $this->copy());
					}

					return '';
				}

				if (isset($data[$name])) {

					$context = $data[$name];

				} else if (in_array($name, $methods, true)) {

					$context = $this->data->$name($args);
				}

				if ((isset($args['strict']) ? $context === null : empty($context)) xor isset($args['invert'])) {

					return '';
				}

				if (isset($args['invert'])) {

					$context = $data;
				}

				if ($text === null) {

					if (Util::isAnonymous($context)) {

						return Util::call($context, $args);
					}

					$string = t($context, @$args['tweak'] ?: 'text-m', '');

					if (isset($args['escape'])) {

						$string = Util::escapeHTML($string);
					}

					return $string;
				}

				if (Util::isList($context)) {

					$string = '';

					foreach ($context as $value) {

						$string .= $this->engine->render($node, $this->copy($value));
					}

					return $string;
				}

				if (Util::isIterable($context)) {

					return $this->engine->render($node, $this->copy($context));
				}

				return $this->engine->render($node, $this->copy());

			case 'launch' :

				$args['body'] = $this->engine->render($node, $this->copy());

				if ($name == '.') {

					$name = t($args, 'me/text');

					if (!$name) {

						return '';
					}
				}

				$string = l($name);

				return t($string, 'text', '');
		}

		return '';
	}
}
