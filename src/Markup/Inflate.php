<?php

namespace Illusion\Markup;

use Illusion\Core\Util;

class Inflate {

    static protected $instance;

    static function instance() {

        return self::$instance ?: self::$instance = new self;
    }

    function string($string, $data = null, $default = null) {

        $string = t($string, 'text');

        if (!$string) {

            return $default;
        }

        $engine = Engine::instance();
        $logics = new Logics($engine, $data);

        $node = $engine->parse($string);
        $text = $engine->render($node, $logics);

        return $text;
    }

    function file($base, $file = null, $data = null, $default = null) {

        $string = Util::file($base, $file);
        $string = $this->string($string, $data, $default);

        return $string;
    }
}
