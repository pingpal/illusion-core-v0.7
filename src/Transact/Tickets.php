<?php

namespace Illusion\Transact;

class Tickets {

	public $ticket;

	function __construct($size = null) {

		if (!$size) {

			$size = 64;
		}

		$size += 8 - --$size % 8;

		$this->ticket = [];

		for ($i = 0; $i < $size / 8 + 1; $i++) {

			$this->ticket[] = 0;
		}
	}

	function encode() {

		$data = '';

		foreach ($this->ticket as $byte) {

			$data .= chr($byte);
		}

		$base = base64_encode($data);

		return $base;
	}

	function decode($base) {

		$this->ticket = [];

		$data = base64_decode($base);

		for ($i = 0; $i < strlen($data); $i++) {

			$this->ticket[] = ord($data[$i]);
		}
	}

	function setIndex($index) {

		$i = $index + 8;

		$this->ticket[$i >> 3] = $this->ticket[$i >> 3] | (1 << ($i & 7));
	}

	function clearIndex($index) {

		$i = $index + 8;

		$this->ticket[$i >> 3] = $this->ticket[$i >> 3] & ~(1 << ($i & 7));
	}

	function getFreeIndex() {

		$length = (count($this->ticket) -1) * 8;

		for ($j = 0; $j < $length; $j++) {

			$i = 8 + ($j + $this->ticket[0]) % $length;

			if (1 & ~($this->ticket[$i >> 3] >> ($i & 7))) {
					
				$index = ($i >> 3) * 8 + ($i & 7) - 8;

				$this->ticket[0] = $index + 1;

				return $index;
			}
		}

		return -1;
	}

	function getSetIndexes() {

		$indexes = [];

		for ($i = 8; $i < count($this->ticket) * 8; $i++) {

			if (1 & ($this->ticket[$i >> 3] >> ($i & 7))) {

				$indexes[] = ($i >> 3) * 8 + ($i & 7) - 8;
			}
		}

		return $indexes;
	}

	function clearConfirmedIndexes($tickets) {

		for ($i = 8; $i < count($tickets->ticket) * 8; $i++) {

			if (1 & ($tickets->ticket[$i >> 3] >> ($i & 7))) {

				$this->ticket[$i >> 3] = $this->ticket[$i >> 3] & ~(1 << ($i & 7));
			}
		}
	}

	function dump($n = 99) {

		$dump = '';

		for ($i = 1; $i < min($n + 1, count($this->ticket)); $i++) {

			$binary = decbin($this->ticket[$i]);
			$binary = str_pad($binary, 8, '0', STR_PAD_LEFT);

			$dump .= $binary . "\n";
		}

		return $dump;
	}
}