<?php

	namespace test;

	use Illusion\Transact\Tokens;

	use Illusion\Transact\Tickets;

	use Illusion\Core\Boot;

	use Exception;

	include '../Core/Boot.php';

	Boot::strap();

	class Node {

		public $name;
		public $node;

		function __construct($name) {

			$this->name = $name;
		}

		public function registerNode($node) {

			$this->node = $node;
		}

		public function add() {

		}

		public function commit() {

		}
	}

	function test_setIndex() {

		$tickets = new Tickets();

		$tickets->setIndex(0);
		$tickets->setIndex(15);
		$tickets->setIndex(15);
		$tickets->setIndex(63);

		$ok = $tickets->dump() === "00000001\n10000000\n00000000\n00000000\n00000000\n00000000\n00000000\n10000000\n";

		echo "setIndex\t" . ($ok ? 'PASS' : 'FAIL') . "\n";
	}

	function test_clearIndex() {

		$tickets = new Tickets();

		$tickets->setIndex(0);
		$tickets->setIndex(15);
		$tickets->setIndex(63);

		$tickets->clearIndex(14);
		$tickets->clearIndex(15);
		$tickets->clearIndex(15);
		$tickets->clearIndex(63);

		$ok = $tickets->dump() === "00000001\n00000000\n00000000\n00000000\n00000000\n00000000\n00000000\n00000000\n";

		echo "clearTickets\t" . ($ok ? 'PASS' : 'FAIL') . "\n";
	}

	function test_storeIndex() {

		$tickets = new Tickets();

		for ($i = 0; $i < 65; $i++) {

			$index = $tickets->getFreeIndex();
			$tickets->setIndex($index);

			$i == 0 && $tickets->clearIndex(0);
			$i == 2 && $tickets->clearIndex(2);
		}

		$ok = $tickets->dump() === "11111011\n11111111\n11111111\n11111111\n11111111\n11111111\n11111111\n11111111\n";

		echo "storeIndex\t" . ($ok ? 'PASS' : 'FAIL') . "\n";
	}

	function test_getSetIndexes() {

		$tickets = new Tickets();

		$tickets->setIndex(0);
		$tickets->setIndex(15);
		$tickets->setIndex(63);

		$ok = $tickets->getSetIndexes() === [0, 15, 63];

		echo "getSetIndexes\t" . ($ok ? 'PASS' : 'FAIL') . "\n";
	}

	function test_clearConfirmedIndexes() {

		$tickets1 = new Tickets();
		$tickets2 = new Tickets();
		$tickets3 = new Tickets();

		for ($i = 0; $i < 64; $i += 3) {

			$tickets1->setIndex($i);
		}

		for ($i = 0; $i < 64; $i++) {

			$index = $tickets2->getFreeIndex();
			$tickets2->setIndex($index);
		}

		$tickets2->clearConfirmedIndexes($tickets1);
		$tickets2->clearConfirmedIndexes($tickets1);
		$tickets2->clearConfirmedIndexes($tickets3);

		$ok = $tickets2->dump() === "10110110\n01101101\n11011011\n10110110\n01101101\n11011011\n10110110\n01101101\n";

		echo "clearConfirme..\t" . ($ok ? 'PASS' : 'FAIL') . "\n";
	}

	function test_xxcode() {

		$edge =		123;
		$tokseq =	456;
		$ticseq =	789;
		$index =	55;

		$indexes = [0, 9, 16, 17, 26, 32, 34, 41, 42, 48, 49, 50, 59];

		$tickets = new Tickets();

		foreach ($indexes as $i) {

			$tickets->setIndex($i);
		}

		$tokens = new Tokens();

		$string =	$tokens->encode($edge, $tokseq, $ticseq, $index, $tickets);
		$array =	$tokens->decode($string);
		$array[5] =	$array[5]->getSetIndexes();

		$ok = substr($string, 33) === "7bg1c8g315g37gAAECAwQFBgcI";

		$ok = $ok && $array === [$tokens->getIdentity(), $edge, $tokseq, $ticseq, $index, $indexes];

		echo "xxcode\t\t" . ($ok ? 'PASS' : 'FAIL') . "\n";
	}

	function test_sequence() {

		$tokens = new Tokens();

		$ok = true;

		for ($i = 1; $i < 9; $i++) {

			$ok = $ok && $i === $tokens->getNextSequence();
		}

		echo "sequence\t" . ($ok ? 'PASS' : 'FAIL') . "\n";
	}

	function test_edge() {

		$tokens = new Tokens();

		for ($i = 0; $i < 9; $i++) {

			$tokens->getNextSequence();
		}

		for ($i = 0; $i < 9; $i++) {

			$tokens->queueMessage('');
		}

		$ok = $tokens->getEdgeSequence() === 10;

		echo "edge\t\t" . ($ok ? 'PASS' : 'FAIL') . "\n";
	}

	function test_filter() {

		$tokens = new Tokens();

		for ($i = 1; $i < 9; $i++) {

			$tokens->addSequenceToFilter($i);
		}

		$tokens->cleanSequenceFilter(6);

		$ok = count($tokens->filter) === 3;

		$ok = $ok && $tokens->isSequenceRelevant(5) === false;
		$ok = $ok && $tokens->isSequenceRelevant(6) === false;
		$ok = $ok && $tokens->isSequenceRelevant(9) === true;

		echo "filter\t\t" . ($ok ? 'PASS' : 'FAIL') . "\n";
	}

	function test_confirm() {

		$tickets = new Tickets();

		for ($i = 1; $i < 9; $i += 2) {

			$tickets->setIndex($i);
		}

		$tokens = new Tokens();

		for ($i = 0; $i < 8; $i++) {

			$tokens->queueMessage('');
		}

		$tokens->clearConfirmedMessages($tickets);

		$ok = array_keys($tokens->getQueuedMessages()) === [0, 2, 4, 6];
		$ok = $ok && $tokens->mine->dump(1) === "01010101\n";

		echo "confirm\t\t" . ($ok ? 'PASS' : 'FAIL') . "\n";
	}

	function test_easy() {

		$ok = true;

		$tokens1 = new Tokens();
		$tokens2 = new Tokens();

		for ($i = 0; $i < 9; $i++) {

			$string = $tokens1->output('');

			if ($i % 2 == 0) {

				list($ok, $re) = array_values((array) $tokens2->input($string));

				if (count($re)) {

					$ok = false;
				}
			}
		}

		$string = $tokens2->output('');

		list($ok, $re) = array_values((array) $tokens1->input($string));

		$tokens1->input($string);

		foreach ($re as $row) {
			
			$tokens2->input($row[1]);
		}

		foreach ($re as $row) {
			
			$tokens2->input($row[1]);
		}

		$ok = $ok && $tokens1->dump() === "Sent: 9\nReceived: 1\nDoubles: 1\nResents: 4\nTicket deny: 1\n";
		$ok = $ok && $tokens2->dump() === "Sent: 1\nReceived: 9\nDoubles: 4\nResents: 0\nTicket deny: 7\n";

		echo "easy\t\t" . ($ok ? 'PASS' : 'FAIL') . "\n";
	}

	function test_hard() {

		class Dude {

			public $name;
			public $tokens;
			public $dude;

			function __construct($name) {

				$this->name = $name;

				$this->tokens = new Tokens();
			}

			function registerReceiver($dude) {

				$this->dude = $dude;
			}

			function put($message, $force = null) {

				$token = $this->tokens->output($message);

				//echo "\nSEND: [$message] ";

				$this->dude->get($message, $token, $force);
			}

			function get($message, $token, $force = null) {

				if ($force === true) {

				} else if ($force === false) {

					return;
				
				} else if (!mt_rand(0, 9)) {

					return;
				}

				$result = $this->tokens->input($token);

				if ($result->ok) {

					global $input;
					global $double;

					if (@$input[$message]) {

						echo "BASDASD";

						$double[] = $message;
					
					} else if ($message !== "[xtra]"){

						$input[$message] = 1;
					}

					//map[$z] = $message
					//echo "OK ($message)";
				}

				if ($result->resend) {

					// echo "\n";
				}

				global $max;

				if ($max < count($result->resend)) {

					$max = count($result->resend);
				}

				foreach ($result->resend as $row) {

					// echo ".";

					//echo "\n- - |$this->name| ";

					list($message, $token) = $row;

					$this->dude->get($message, $token);
				}
			}
		}

		global $input;
		global $double;

		$input['just to make sure'] = 1;
		$double = ['just to make sure'];

		$dude1 = new Dude('Dude 1');
		$dude2 = new Dude('Dude 2');

		$dude1->registerReceiver($dude2);
		$dude2->registerReceiver($dude1);

		$rounds = 99;

		$total = 0;

		ob_start();

		for ($i = 0; $i < $rounds; $i++) {

			$rand = mt_rand(0, 1);

			// $rand = $i % 2;

			$dude = $rand ? $dude2 : $dude1;
			$othe = !$rand ? $dude2 : $dude1;

			$rand++;

			//echo "\n\nRound $i (Dude $rand)\n-----------------------";

			for ($j = 0; $j < mt_rand(1, 10); $j++) {

				$total++;

				$msg = "round $i dude $rand put $j";

				$msgs[] = $msg;

				$dude->put($msg);
			}

			//echo "\n";
			//echo "\nDude $rand - Mine: " . trim($dude->tokens->mine->dump(1)) . " Your: " . trim($dude->tokens->your->dump(1));
			//echo     "\nOthe r - Mine: " . trim($othe->tokens->mine->dump(1)) . " Your: " . trim($othe->tokens->your->dump(1));
		}

		for ($i = 0; $i < $rounds; $i++) {

			$total++;
	
			$dude1->put("[xtra]", true);
			$dude2->put("[xtra]", true);
		}

		$echo = ob_get_clean();

		foreach ($msgs as $msg) {

			unset($input[$msg]);
		}

		echo "\n";

		echo $dude1->tokens->dump() . "\n";
		echo $dude2->tokens->dump() . "\n";

		global $max;

		echo "Total sent messages: $total\n\n";

		echo "Max size of resend list: $max\n\n";

		echo "Number of dropped: ".(count($input)-1)." ".($input === ['just to make sure' => 1] ? 'PASS' : 'FAIL')."\n";
		echo "Number of doubles: ".(count($double)-1)." ".($double === ['just to make sure'] ? 'PASS' : 'FAIL')."\n";

		echo "\n";

		$recv = $dude1->tokens->realReceived;
		$sent = $dude2->tokens->actualSent;

		echo "Node 1 recv: ".($recv)." of ".($sent)." ".($recv === $sent ? 'PASS' : 'FAIL').($sent < $recv ? ' BAD! ' : '')."\n";

		$recv = $dude2->tokens->realReceived;
		$sent = $dude1->tokens->actualSent;

		echo "Node 2 recv: ".($recv)." of ".($sent)." ".($recv === $sent ? 'PASS' : 'FAIL').($sent < $recv ? ' BAD! ' : '')."\n";

		echo "\n";

		$redo = $dude1->tokens->resendAppeals;
		$sent = $dude1->tokens->actualSent;

		echo "Node 1 redo: ".($redo)." of ".($sent)." = ".((int)(($redo/$sent)*100))."%\n";

		$redo = $dude2->tokens->resendAppeals;
		$sent = $dude2->tokens->actualSent;

		echo "Node 2 redo: ".($redo)." of ".($sent)." = ".((int)(($redo/$sent)*100))."%\n";

		echo "\n";

		$dbls = $dude1->tokens->doubleReceived;
		$sent = $dude2->tokens->actualSent;

		echo "Node 1 dbls: ".($dbls)." of ".($sent)." = ".((int)(($dbls/$sent)*100))."%\n";

		$dbls = $dude2->tokens->doubleReceived;
		$sent = $dude1->tokens->actualSent;

		echo "Node 2 dbls: ".($dbls)." of ".($sent)." = ".((int)(($dbls/$sent)*100))."%\n";

		echo "\n";

		echo "Node 1 queue size: ".(count($dude1->tokens->queue))."\n";
		echo "Node 2 queue size: ".(count($dude2->tokens->queue))."\n";

		echo "\n";

		echo "Node 1 filter size: ".(count($dude1->tokens->filter))."\n";
		echo "Node 2 filter size: ".(count($dude2->tokens->filter))."\n";

		echo "\n\n";

		echo $echo;
	}

	test_setIndex();
	test_clearIndex();
	test_storeIndex();
	test_getSetIndexes();
	test_clearConfirmedIndexes();

	test_xxcode();
	test_sequence();
	test_edge();
	test_filter();
	test_confirm();

	test_easy();
	test_hard();
