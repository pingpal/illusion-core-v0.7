<?php

namespace Illusion\Transact;

use Illusion\Core\Util;

class Tokens {

	public $identity;

	public $mine;
	public $your;

	public $sequence = 1;
	public $ticseq = 0;

	public $queue = [];
	public $filter = [];
	public $edge = 1;

	public $actualSent = 0;
	public $realReceived = 0;
	public $doubleReceived = 0;
	public $resendAppeals = 0;
	public $deniedByTicketSequence = 0;

	function __construct() {

		$this->identity = Util::key();

		$this->mine = new Tickets();
		$this->your = new Tickets();
	}

	static function parseIdentity($token) {

		return substr($token, 1, 32);
	}

	function getIdentity() {

		return $this->identity;
	}

	function encode($edge, $tokseq, $ticseq, $index, $tickets) {

		$ob = '0' . $this->identity;

		$ob.= dechex($edge) .	'g';
		$ob.= dechex($tokseq) .	'g';
		$ob.= dechex($ticseq) .	'g';
		$ob.= dechex($index) .	'g';

		$ob.= $tickets->encode();

		return $ob;
	}

	function decode($token) {

		$identity = substr($token, 1, 32);

		$g0 = 32;

		$g1 = strpos($token, 'g', $g0 + 1);
		$g2 = strpos($token, 'g', $g1 + 1);
		$g3 = strpos($token, 'g', $g2 + 1);
		$g4 = strpos($token, 'g', $g3 + 1);

		$edge =		hexdec(substr($token, $g0 + 1, $g1 - $g0 - 1));
		$tokseq =	hexdec(substr($token, $g1 + 1, $g2 - $g1 - 1));
		$ticseq =	hexdec(substr($token, $g2 + 1, $g3 - $g2 - 1));
		$index =	hexdec(substr($token, $g3 + 1, $g4 - $g3 - 1));

		$tickets = new Tickets();

		$tickets->decode(substr($token, $g4 + 1));

		return [ $identity, $edge, $tokseq, $ticseq, $index, $tickets ];
	}

	function getNextSequence() {

		return $this->sequence++;
	}

	function getEdgeSequence() {

		$edge = $this->getNextSequence();

		foreach ($this->queue as $row) {

			if ($row[0] < $edge) {

				$edge = $row[0];
			}
		}

		return $edge;
	}

	function addSequenceToFilter($sequence) {

		$this->filter["$sequence"] = $sequence;
	}

	function cleanSequenceFilter($edge) {

		if ($this->edge < $edge) {

			$this->edge = $edge;
		}

		foreach ($this->filter as $key => $sequence) {

			if ($sequence < $edge) {

				unset($this->filter[$key]);
			}
		}
	}

	function isSequenceRelevant($sequence) {

		return $this->edge <= $sequence && !isset($this->filter["$sequence"]);
	}

	function queueMessage($message) {

		$sequence = $this->getNextSequence();

		$index = $this->mine->getFreeIndex();
		$this->mine->setIndex($index);

		$this->queue[$index] = [$sequence, $message];

		return [ $index, $sequence ];
	}

	function clearConfirmedMessages($tickets) {

		foreach ($tickets->getSetIndexes() as $index) {

			if (@$this->queue[$index]) {

				unset($this->queue[$index]);
			}
		}

		$this->mine->clearConfirmedIndexes($tickets);
	}

	function getQueuedMessages() {

		return $this->queue;
	}

	function registerYourIndex($index) {

		$this->your->setIndex($index);
	}

	function getAndResetYourTickets() {

		$tickets = $this->your;

		$this->your = new Tickets();

		$sequence = $this->getNextSequence();

		return [ $tickets, $sequence ];
	}

	function output($message) {

		$edge = $this->getEdgeSequence();

		list($index, $tokseq) =		$this->queueMessage($message);
		list($tickets, $ticseq) =	$this->getAndResetYourTickets();

		$this->actualSent++;

		return $this->encode($edge, $tokseq, $ticseq, $index, $tickets);
	}

	function input($token) {

		$ok = false;
		$resend = [];

		list(, $edge, $tokseq, $ticseq, $index, $tickets) = $this->decode($token);

		$this->registerYourIndex($index);
		$this->cleanSequenceFilter($edge);

		if ($this->isSequenceRelevant($tokseq)) {

			$this->addSequenceToFilter($tokseq);

			$ok = true;

			$this->realReceived++;

		} else {

			$this->doubleReceived++;
		}

		if ($this->ticseq < $ticseq) {

			$this->ticseq = $ticseq;

			$this->clearConfirmedMessages($tickets);

			$tickets = null;

			$edge = $this->getEdgeSequence();

			foreach ($this->getQueuedMessages() as $index => $row) {

				if (!$tickets) {

					list($tickets, $ticseq) = $this->getAndResetYourTickets();
				}

				$resend[] = [$row[1], $this->encode($edge, $row[0], $ticseq, $index, $tickets)];

				$this->resendAppeals++;
			}

		} else {

			$this->deniedByTicketSequence++;
		}

		return (object) [ 'ok' => $ok, 'resend' => $resend ];
	}

	function dump() {

		$ob = "Sent: $this->actualSent\n";
		$ob.= "Received: $this->realReceived\n";
		$ob.= "Doubles: $this->doubleReceived\n";
		$ob.= "Resents: $this->resendAppeals\n";
		$ob.= "Ticket deny: $this->deniedByTicketSequence\n";

		return $ob;
	}
}
