<?php

namespace Illusion;

class Config {

    static $_SCAN_COMMON    = 'Illusion\Common:vendor/hallemaen/illusion-common/src';
    static $_SCAN_WEBIF     = 'Illusion\WebIF:vendor/hallemaen/illusion-webif/src';

    static $DEBUG   = true;
    static $INSTALL = true;

    static $VENDOR  = 'hallemaen';
    static $BRAND   = 'illusion';

    static $VERSION = '3.0.4';
    static $HELLO   = 'IllUSION (PHP) v3.0.4';

    static $LANGUAGE    = 'EN';

    static $TIMEZONE    = 'Europe/Stockholm';

    static $COMMAND     = 'illusion';
    static $PATH_EXE    = '/usr/bin/php';

    static $ENVIRONMENTS    = '';

    static $REQUEST_EXTS    = 'php|txt';

    static $ROUTE_PREFIX    = '';
    static $ROUTE_PACKET    = 'packet';

    static $DEFAULT_SES_STORE   = 'sql';
    static $DEFAULT_SES_BACKEND = 'sql';

    static $DEFAULT_DATABASE    = 'mysql';

    static $_LOAD_CORE      = 'Core/functions.php';
    static $_LOAD_CRUD      = 'Crud/functions.php';
    static $_LOAD_SQL       = 'Sequel/functions.php';
    static $_LOAD_MARKUP    = 'Markup/functions.php';

    static $_PATH_ROOT      = '.';
    static $_PATH_WRITABLE  = 'Writable';
    static $_WPATH_SQLITE   = 'sqlite';
    static $_WPATH_CACHE    = 'cache';
    static $_WPATH_TEMP     = 'temp';
    static $_WPATH_DATA     = 'data';
    static $_WPATH_LOG      = 'log';

    static $_PATH_DETACH    = 'detach.php';
    static $_PATH_LAUNCH    = 'launch.php';

    static $CACHE_DEFAULT_TTL   = 1;
    static $CACHE_CLEAN_CHANCE  = 0.01;

    static $SECURE_STORE_KEY = 'password';

    static $MYSQL_SERVER    = 'localhost';
    static $MYSQL_DATABASE  = 'dbname';
    static $MYSQL_USERNAME  = 'username';
    static $MYSQL_PASSWORD  = 'password';

    static $SMTP_ADDRESS    = 'example@gmail.com';
    static $SMTP_USERNAME   = 'example@gmail.com';
    static $SMTP_PASSWORD   = 'password';

    static $SMTP_PROTOCOL   = 'ssl';
    static $SMTP_HOST       = 'smtp.gmail.com';
    static $SMTP_PORT       = 465;

    static $IF;

    static $LIBROOT;
    static $DOCROOT;
    static $TOPNAME;
    static $INITDIR;

    static $ROOT;
    static $SCAN;
}
