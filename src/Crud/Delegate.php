<?php

namespace Illusion\Crud;

use Illusion\Core\AnObj;

use Illusion\Core\Util;

use Exception;

class Delegate {

    protected $blocks;
    protected $strict;

    protected $render;
    protected $query;

    protected $access;

    function __construct($blocks, $strict = false, $context = null) {

        $this->blocks = $blocks;
        $this->strict = $strict;
        $this->context = $context;

        $this->render = new Render($blocks);
        $this->query = $this->render->apply();
    }

    /// TO ///

    function toDrop() {

        return $this->format($this->buildDrop());
    }

    function toSetup() {

        return $this->format($this->buildSetup());
    }

    function toRead() {

        return $this->format($this->buildRead());
    }

    function toCreate() {

        return $this->format($this->buildCreate());
    }

    function toUpdate() {

        return $this->format($this->buildUpdate());
    }

    function toDelete() {

        return $this->format($this->buildDelete());
    }

    /// DO ///

    function doDrop() {

        $blocks = $this->blocks;
        $build = $this->buildDrop();

        $this->checkPermission('drop', $blocks, $build);

        $build->sql->exe();

        return (object) [ 'query' => $this->query ];
    }

    function doSetup() {

        $blocks = $this->blocks;
        $build = $this->buildSetup();

        $this->checkPermission('setup', $blocks, $build);

        $build->sql->exe();

        return (object) [ 'query' => $this->query ];
    }

    function doRead() {

        $blocks = $this->blocks;
        $build = $this->buildRead();

        $this->checkPermission('read', $blocks, $build);

        $items = $build->sql->all();

        $prev = null;
        $next = null;

        $offset = $blocks['offset'];
        $length = $blocks['length'];

        if (0 < $blocks['offset']) {

            $extent = ['offset' => $offset - $length];
            $prev = $this->render->apply($extent);
        }

        if ($blocks['length'] < count($items)) {

            $extent = ['offset' => $offset + $length];
            $next = $this->render->apply($extent);
        }

        $items = array_slice($items, 0, $blocks['length']);

        return (object) [

            'prev' => $prev,
            'next' => $next,
            'items' => $items,
            'get' => $blocks['get'],
            'query' => $this->query,
        ];
    }

    function doCreate() {

        $blocks = $this->blocks;
        $build = $this->buildCreate();

        $this->checkPermission('create', $blocks, $build);

        $last = $build->sql->exe();
        // $last = $build->sql->db()->lastInsertId();

        return (object) [

            'id' => $last,
            'set' => $blocks['set'],
            'query' => $this->query,
        ];
    }

    function doUpdate() {

        $blocks = $this->blocks;
        $build = $this->buildUpdate();

        $this->checkPermission('update', $blocks, $build);

        $rows = $build->sql->exe();

        return (object) [

            'rows' => $rows,
            'set' => $blocks['set'],
            'query' => $this->query,
        ];
    }

    function doDelete() {

        $blocks = $this->blocks;
        $build = $this->buildDelete();

        $this->checkPermission('delete', $blocks, $build);

        $rows = $build->sql->exe();

        return (object) [

            'rows' => $rows,
            'query' => $this->query,
        ];
    }

    /// BUILD ///

    function buildDrop() {

        $blocks = $this->blocks;

        if (!$blocks['from']){

            throw new Exception('bad from');
        }

        $obj = new AnObj();

        $obj->toString = function () use ($blocks) {

            return "DROP TABLE `$blocks[from]`";
        };

        $obj->exe = function () use ($obj) {

            sql()->exe($obj->toString());
        };

        return (object) [ 'sql' => $obj ];
    }

    function buildSetup() {

        $blocks = $this->blocks;

        if (!$blocks['from']){

            throw new Exception('bad from');
        }

        $table = sql()->getTableFromName($blocks['from']);

        if (!$table) {

            throw new Exception('bad table');
        }

        $sql = sql()->create()->table($table['table'])->engine($table['engine']);

        foreach ($table['columns'] as $args) {

            $sql->column($args[0], $args[1]);
        }

        foreach ($table['foreigners'] as $args) {

            $sql->foreign($args[0], $args[1], $args[2], @$args[3]);
        }

        foreach ($table['customs'] as $arg) {

            $sql->custom($arg);
        }

        return (object) [ 'sql' => $sql ];
    }

    function buildRead() {

        $blocks = $this->blocks;

        if (!$blocks['from']) {

            throw new Exception('bad from');
        }

        if (!$blocks['get']) {

            $blocks['get'] = [];

            $tables = array_merge([['me' => $blocks['from']]], $blocks['joins']);

            foreach ($tables as $table) {

                $name = trim($table['me'], ':');

                $info = sql()->getTableFromName($name);

                if ($info) {

                    foreach ($info['columns'] as $column) {

                        $blocks['get']["$name.$column[0]"] = null;
                    }
                }
            }
        }

        $sql = sql()->select()->from($blocks['from']);

        foreach ($blocks['get'] as $field => $alias) {

            $sql->field($field, $alias ? $alias : preg_replace('/[.:]/', '_', $field));
        }

        $chain = [];

        foreach ($blocks['joins'] as $row) {

            if ($row['my'] || $row['you'] || $row['your']) {

                $row['my'] ||	$row['my'] =	'id';
                $row['you'] ||	$row['you'] =	$blocks['from'];
                $row['your'] ||	$row['your'] =	$row['me'];

                $condition = "$row[me].$row[my] = $row[you].$row[your]";

                $chain[] = [ $row['me'], $row['my'], $row['you'], $row['your'] ];

            } else {

                $condition = null;

                // next part of this block is only to build join chain for access control

                $base = $blocks['from']; // table alias if supported in future versions
                $real = $blocks['from'];

                $alias = null; // alias of this join if supported in future versions

                $n = preg_match_all('/([a-z_][a-z0-9_]*[.:]?)/i', "$row[me].", $matches) - 1;

                foreach ($matches[1] as $i => $table) {

                    $back = substr($table, -1) === ':';
                    $table = substr($table, 0, -1);

                    if ($base && $base !== $table) {

                        if ($back) {

                            $chain[] = [ $n == $i && $alias ? $alias : $table, $real, $base, 'id' ];

                        } else {

                            $chain[] = [ $n == $i && $alias ? $alias : $table, 'id', $base, $table ];
                        }
                    }

                    $base = $table;
                    $real = $table;
                }
            }

            if (!$row['type']) {

                $row['type'] = 'INNER';
            }

            $sql->join($row['me'], null, $condition, $row['type']);
        }

        foreach ($blocks['filter'] as $filter) {

            $query = $this->renderFilter($filter);

            $values = [];

            foreach ($filter as $row) {

                $values[] = $row['value'];
            }

            $sql->whereArgs($query, $values);
        }

        foreach ($blocks['group'] as $field) {

            $sql->group($field);
        }

        foreach ($blocks['sort'] as $field => $order) {

            $sql->order($field, $order);
        }

        if (!$blocks['sort']) {

            $sql->order("$blocks[from].id", false);
        }

        $sql->limit($blocks['length'] + 1);
        $sql->offset($blocks['offset']);

        $filter = $this->enhanceFilter($blocks['from'], $blocks['filter']);
        $fields = $this->enhanceFields($blocks['from'], $blocks['get']);

        return (object) [ 'sql' => $sql, 'filter' => $filter, 'fields' => $fields, 'chain' => $chain ];
    }

    function buildCreate() {

        $blocks = $this->blocks;

        if (!$blocks['from']) {

            throw new Exception('bad from');
        }

        if (!$blocks['set']) {

            throw new Exception('bad set');
        }

        $sql = sql()->insert()->into($blocks['from']);

        foreach ($blocks['set'] as $field => $value) {

            $sql->set($field, $value);
        }

        $fields = $this->enhanceFields($blocks['from'], $blocks['set']);

        return (object) [ 'sql' => $sql, 'fields' => $fields ];
    }

    function buildUpdate() {

        $blocks = $this->blocks;

        if (!$blocks['from']) {

            throw new Exception('bad from');
        }

        if (!$blocks['set']) {

            throw new Exception('bad set');
        }

        if (!$blocks['filter']) {

            throw new Exception('bad filter');
        }

        $sql = sql()->update()->table($blocks['from']);

        foreach ($blocks['set'] as $field => $value) {

            $sql->set($field, $value);
        }

        $this->addNestedFilter($sql, $blocks);

        $filter = $this->enhanceFilter($blocks['from'], $blocks['filter']);
        $fields = $this->enhanceFields($blocks['from'], $blocks['set']);

        return (object) [ 'sql' => $sql, 'filter' => $filter, 'fields' => $fields ];
    }

    function buildDelete() {

        $blocks = $this->blocks;

        if (!$blocks['from']) {

            throw new Exception('bad from');
        }

        if (!$blocks['filter']) {

            throw new Exception('bad filter');
        }

        $sql = sql()->delete()->from($blocks['from']);

        $this->addNestedFilter($sql, $blocks);

        $filter = $this->enhanceFilter($blocks['from'], $blocks['filter']);

        return (object) [ 'sql' => $sql, 'filter' => $filter ];
    }

    /// TOOLS ///

    protected function format($object) {

        $sequel = $object->sql->toString();
        $filter = @$object->filter;

        if ($filter) {

            $format = "- FILTER:\n\n%s\n\n";

            $filter = @json_encode($filter, JSON_PRETTY_PRINT);
            $fitler = sprintf($format, $filter);
        }

        $format = "- CRUD:\n\n%s\n\n- SQL:\n\n%s\n\n%s";

        return sprintf($format, $this->query, $sequel, $filter);
    }

    protected function enhanceFields($from, $fields) {

        $array = [ 'values' => [], 'fields' => $fields ];

        foreach ($array['fields'] as $field => $value) {

            list($table, $column) = array_slice(array_merge([$from], preg_split('/[.:]/', $field)), -2);

            $array['values'][$table][$column] = $value;
        }

        return $array;
    }

    protected function enhanceFilter($from, $filters) {

        $array = [ 'safe' => true, 'values' => [], 'precise' => [], 'filter' => $filters ];

        foreach ($array['filter'] as $i => $filter) {

            foreach ($filter as $j => $row) {

                list($table, $column) = array_slice(array_merge([$from], preg_split('/[.:]/', $row['field'])), -2);

                $array['filter'][$i][$j]['table'] = $table;
                $array['filter'][$i][$j]['column'] = $column;

                $array['values'][$table][$column] = $row['value'];

                if ($row['comparator'] == '=') {

                    $array['precise'][$table][$column] = $row['value'];
                }

                if ($row['operator'] && $row['operator'] != 'AND') {

                    $array['safe'] = false;
                }
            }
        }

        return $array;
    }

    protected function renderFilter($filter) {

        $query = '';

        foreach ($filter as $row) {

            if ($row['operator']) {

                $query .= " $row[operator] ";
            }

            $query .= "$row[field] $row[comparator] ?";
        }

        return $query;
    }

    protected function addNestedFilter($sql, $blocks) {

        $mine = [];
        $yours = [];

        foreach ($blocks['filter'] as $filter) {

            $bits = 0;

            foreach ($filter as $row) {

                $bits |= (preg_match('/(\w+)[.:]/', $row['field'], $match) && $match[1] != $blocks['from'] ? 1 : 0) + 1;
            }

            switch ($bits) {

                case 1: $mine[] =	$filter; break;
                case 2: $yours[] =	$filter; break;

                default: throw new Exception('a filter refers to self and others, divide and conquer');
            }
        }

        if ($yours && !$blocks['joins']) {

            throw new Exception('a filter refers to other tables, explicit join required');
        }

        if (1 < count($blocks['joins'])) { //?? deprecated

            $from = $blocks['joins'][0]['me'];

            foreach (array_slice($blocks['joins'], 1) as $join) {

                if ($join['you'] && $join['you'] != $from) {

                    throw new Exception('the nested joins are disjoint from the nested select, unite them with a nest before any join');
                }
            }
        }

        foreach ($mine as $filter) {

            $query = $this->renderFilter($filter);

            $values = [];

            foreach ($filter as $row) {

                $values[] = $row['value'];
            }

            $sql->whereArgs($query, $values);
        }

        if ($blocks['joins']) {

            $row = $blocks['joins'][0];
            $back = substr($row['me'], -1) == ':';

            // var_dump($blocks['from'], $blocks['joins']);

            /* if ($blocks['from'] == $row['me']) {

                $row['my'] ||	$row['my'] =	'id';
                $row['you'] ||	$row['you'] =	$row['me'];
                $row['your'] ||	$row['your'] =	'id';

            } else */

            if ($back) {

                $row['me'] = substr($row['me'], 0, -1);

                $row['my'] ||	$row['my'] =	'id';
                $row['you'] ||	$row['you'] =	$blocks['from'];
                $row['your'] ||	$row['your'] =	$row['me'];

            } else {

                $row['my'] ||	$row['my'] =	$blocks['from'];
                $row['you'] ||	$row['you'] =	$blocks['from'];
                $row['your'] ||	$row['your'] =	'id';
            }

            $sub = $sql->db()->select()->from($row['me']); //->base($blocks['from']);

            $sub->field("$row[me].$row[my]");
            $sql->where("$row[you].$row[your] IN ?", $sub);

            foreach (array_slice($blocks['joins'], 1) as $row) {

                if ($row['my'] || $row['you'] || $row['your']) {

                    $row['my'] ||	$row['my'] =	'id';
                    $row['you'] ||	$row['you'] =	$blocks['from'];
                    $row['your'] ||	$row['your'] =	$row['me'];

                    $condition = "$row[me].$row[my] = $row[you].$row[your]";

                } else {

                    $condition = null;
                }

                if (!$row['type']) {

                    $row['type'] = 'INNER';
                }

                $sub->join($row['me'], null, $condition, $row['type']);
            }

            foreach ($yours as $filter) {

                $query = $this->renderFilter($filter);

                $values = [];

                foreach ($filter as $row) {

                    $values[] = $row['value'];
                }

                $sub->whereArgs($query, $values);
            }
        }

        return $sql;
    }

    protected function checkPermission($verb, $blocks, $build) {

        if (!$this->strict) {

            return;
        }

        $check = [];

        $tables = array_merge([[ 'me' => $blocks['from'] ]], $blocks['joins']);

        foreach ($tables as $join) {

            $name = trim($join['me'], ':');

            $table = sql()->getTableFromName($name);

            if (!@$table) {

                throw new Exception('bad access: unknown table');
            }

            $check[$name] = [ 'class' => $table['class'] ];
        }

        foreach (@$build->filter['values'] ?: [] as $name => $values) {

            $table = sql()->getTableFromName($name);

            if (!@$table) {

                throw new Exception('bad access: unknown table');
            }

            $check[$name] = [ 'class' => $table['class'] ];
        }

        foreach ($check as $name => $row) {

            $check[$name]['filter'] = @$build->filter['values'][$name] ?: [];
            $check[$name]['precise'] = @$build->filter['precise'][$name] ?: [];
            $check[$name]['fields'] = @$build->fields['values'][$name] ?: [];
        }

        $safe = @$build->filter ? $build->filter['safe'] : false;

        foreach (@$build->chain ?: [] as $link) {

            $table = sql()->getTableFromName($link[0]);

            if (!@$table) {

                throw new Exception('bad access: unknown table');
            }

            $match = false;

            foreach ($table['associates'] as $row) {

                if ($link[1] == $row[0] && $link[2] == $row[1] && $link[3] == $row[2]) {

                    $match = true;

                    break;
                }
            }

            if (!$match) {

                $safe = false;

                break;
            }
        }

        $proceed = false;

        foreach ($check as $name => $row) {

            $callable = [ $row['class'], 'access' ];
            $fields = $row['fields'];

            if (Util::isCallable($callable)) {

                if ($verb == 'read') {

                    foreach (array_keys($fields) as $field) {

                        $fields[$field] = $field;
                    }
                }

                $ok = Util::call($callable, $this->context, $verb, $safe, $fields, $row['precise'], $row['filter']);

            } else {

                $ok = false;
            }

            if ($ok === true) {

                $proceed = true;

            } else if ($ok === false) {

                $proceed = false;

                break;
            }
        }

        if (!$proceed) {

            throw new Exception('crud: bad access', 2);
        }
    }
}
