<?php

namespace Illusion\Crud;

use Exception;

class Build {

    static function factory() {

        return function ($verb = null, $from = null) {

            return new Build($verb, $from);
        };
    }

    protected $strict = false;
    protected $context;

    protected $blocks = [

        'verb' =>   null,
        'from' =>   null,

        'get' =>    [],
        'set' =>    [],
        'joins' =>  [],
        'filter' => [],

        'length' => 100,
        'offset' => 0,

        'group' =>  [],
        'sort' =>   []
    ];

    function __construct($verb = null, $from = null) {

        $this->verb($verb);
        $this->from($from);
    }

    function strict($context = null, $strict = true) {

        $this->strict = $strict;
        $this->context = $context;

        return $this;
    }

    function read($from = null) {

        $this->verb('read');
        $this->from($from);

        return $this;
    }

    function create($from = null) {

        $this->verb('create');
        $this->from($from);

        return $this;
    }

    function update($from = null) {

        $this->verb('update');
        $this->from($from);

        return $this;
    }

    function delete($from = null) {

        $this->verb('delete');
        $this->from($from);

        return $this;
    }

    function to() {

        $delegate = new Delegate($this->blocks, $this->strict, $this->context);

        switch ($this->blocks['verb']) {

            case 'drop' :	return $delegate->toDrop();
            case 'setup' :	return $delegate->toSetup();
            case 'read' :	return $delegate->toRead();
            case 'create' :	return $delegate->toCreate();
            case 'update' :	return $delegate->toUpdate();
            case 'delete' :	return $delegate->toDelete();

            default : throw new Exception('bad verb');
        }
    }

    function end() {

        $delegate = new Delegate($this->blocks, $this->strict, $this->context);

        switch ($this->blocks['verb']) {

            case 'drop' :	return $delegate->doDrop();
            case 'setup' :	return $delegate->doSetup();
            case 'read' :	return $delegate->doRead();
            case 'create' :	return $delegate->doCreate();
            case 'update' :	return $delegate->doUpdate();
            case 'delete' :	return $delegate->doDelete();

            default : throw new Exception('bad verb');
        }
    }

    function sql() {

        $delegate = new Delegate($this->blocks, $this->strict, $this->context);

        switch ($this->blocks['verb']) {

            case 'drop' :	return $delegate->buildDrop()->sql;
            case 'setup' :	return $delegate->buildSetup()->sql;
            case 'read' :	return $delegate->buildRead()->sql;
            case 'create' :	return $delegate->buildCreate()->sql;
            case 'update' :	return $delegate->buildUpdate()->sql;
            case 'delete' :	return $delegate->buildDelete()->sql;

            default : throw new Exception('bad verb');
        }
    }

    function verb($verb = null) {

        if ($verb) {

            $this->blocks['verb'] = $verb;
        }

        return $this;
    }

    function from($from = null) {

        if ($from) {

            $this->blocks['from'] = $from;
        }

        return $this;
    }

    function get($field, $alias = null) {

        $this->blocks['get'][$field] = $alias;

        return $this;
    }

    function set($field, $value = null) {

        $this->blocks['set'][$field] = $value;

        return $this;
    }

    /**
     * In case of update or delete with a nested filter, sometimes the
     * relation between the 'WHERE column IN' block and the nested
     * 'SELECT column FROM' block needs to be explicitly defined.
     */
    function nest($me, $my = null, $you = null, $your = null) { //?? deprecated

        $this->join($me, $my, $you, $your);

        return $this;
    }

    function join($me, $my = null, $you = null, $your = null, $type = null) {

        $this->blocks['joins'][] = [ 'type' => strtoupper($type) , 'me' => $me, 'my' => $my, 'you' => $you, 'your' => $your ];

        return $this;
    }

    function filter($query) {

        return $this->filterArgs($query, array_slice(func_get_args(), 1));
    }

    function filterArgs($query, $values = []) {

        $array = $this->parseFilter($query);

        $filter = [];

        for ($i = 0; $i < min(count($array), count($values)); $i++) {

            $filter[] = array_merge($array[$i], [ 'value' => $values[$i] ]);
        }

        $this->blocks['filter'][] = $filter;

        return $this;
    }

    function range($length = null, $offset = null) {

        $this->blocks['length'] = (int) max(0, min($length, 1000));
        $this->blocks['offset'] = (int) max(0, $offset);

        return $this;
    }

    function group($field) {

        $this->blocks['group'][] = $field;

        return $this;
    }

    function sort($field, $asc = true) {

        $this->blocks['sort'][$field] = !!$asc;

        return $this;
    }

    protected function parseFilter($query) {

        $filter = [];

        foreach (preg_split('/(?=\b(AND|OR)\b)/i', $query) as $chunk) {

            if ($chunk && preg_match('/^\s*(AND|OR)?\s*(\w+[.:]?\w*)\s*(=|<|>|<>|LIKE)\s*\\?\s*$/i', $chunk, $match)) {

                $filter[] = ['field' => $match[2], 'comparator' => $match[3], 'operator' => $match[1] ? strtoupper($match[1]) : null];

            } else {

                throw new Exception('syntax error');
            }
        }

        return $filter;
    }
}
