<?php

namespace Illusion\Crud;

class Render {

    protected $blocks;

    function __construct($blocks) {

        $this->blocks = $blocks;
    }

    function apply($extend = []) {

        $blocks = array_merge($this->blocks, $extend);
        $query = "$blocks[verb]($blocks[from])";

        if ($blocks['get']) {

            $chunk = '';

            foreach ($blocks['get'] as $field => $value) {

                $chunk && $chunk .= ', ';
                $chunk .= $field;

                if ($value !== null) {

                    $chunk .= " = $value";
                }
            }

            $query .= ".get($chunk)";
        }

        if ($blocks['set']) {

            $chunk = '';

            foreach ($blocks['set'] as $field => $value) {

                $chunk && $chunk .= ', ';
                $chunk .= $field;

                if ($value !== null) {

                    $chunk .= " = $value";
                }
            }

            $query .= ".set($chunk)";
        }

        if ($blocks['joins']) {

            $chunk = '';

            foreach ($blocks['joins'] as $row) {

                $chunk && $chunk .= ', ';

                if ($row['type'] && $row['type'] !== 'INNER') {

                    $chunk .= strtolower($row['type']) . '! ';
                }

                if ($row['my'] || $row['you'] || $row['your']) {

                    $row['my'] ||	$row['my'] =	'id';
                    $row['you'] ||	$row['you'] =	$blocks['from'];
                    $row['your'] ||	$row['your'] =	$row['me'];

                    $chunk .= "$row[me].$row[my] = $row[you].$row[your]";

                } else {

                    $chunk .= $row['me'];
                }
            }

            $query .= ".join($chunk)";
        }

        foreach ($blocks['filter'] as $filter) {

            $query .= '.filter(';

            foreach ($filter as $row) {

                if ($row['operator']) {

                    $query .= " $row[operator] ";
                }

                $query .= "$row[field] $row[comparator] $row[value]";
            }

            $query .= ')';
        }

        if ($blocks['length'] !== 100 || $blocks['offset'] !== 0) {

            $query .= ".range($blocks[length], $blocks[offset])";
        }

        if ($blocks['group']) {

            $chunk = '';

            foreach ($blocks['group'] as $field) {

                $chunk && $chunk .= ', ';
                $chunk .= $field;
            }

            $query .= ".group($chunk)";
        }

        if ($blocks['sort']) {

            $chunk = '';

            foreach ($blocks['sort'] as $field => $asc) {

                $chunk && $chunk .= ', ';
                $chunk .= ($asc ? '' : 'DESC! ') . $field;
            }

            $query .= ".sort($chunk)";
        }

        return $query;
    }
}
