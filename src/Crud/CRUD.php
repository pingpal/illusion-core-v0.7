<?php

namespace Illusion\Crud;

class CRUD {

    static function factory() {

        return function ($query = null) {

            return (new Parse())->apply($query);
        };
    }
}
