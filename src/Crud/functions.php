<?php

function crud() {

    static $crud;

    $crud || $crud = \Illusion\Crud\CRUD::factory();

    return call_user_func_array($crud, func_get_args());
}
