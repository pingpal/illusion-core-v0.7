<?php

namespace Illusion\Crud;

use Exception;

class Parse {

    function apply($query = null) {

        $crud = new Build();

        if ($query) {

            foreach (preg_split('/(?<=\\))\s*\\.\s*(?=\w+\\()/', $query) as $chunk) {

                if ($chunk && preg_match('/^\s*(\w+)\s*\\(([^)]*)\\)\s*$/', $chunk, $match)) {

                    list(, $key, $query) = $match;

                    switch ($key) {

                        case 'read':
                        case 'create':
                        case 'update':
                        case 'delete':
                        case 'setup':
                        case 'drop':

                            if (preg_match('/^\s*(\w+)\s*$/', $query, $match)) {

                                $crud->verb($key);
                                $crud->from($match[1]);

                            } else {

                                throw new Exception('syntax error');
                            }

                            break;

                        case 'get':
                        case 'set':

                            foreach (preg_split('/\s*,\s*/', $query) as $chunk) {

                                if ($chunk && preg_match('/^\s*(\w+[.:]?\w*)(?:\s*=\s*([\s\S]+))?\s*$/', $chunk, $match)) {

                                    if ($key == 'get') {

                                        $crud->get($match[1], @$match[2]);

                                    } else {

                                        $crud->set($match[1], @$match[2]);
                                    }


                                } else {

                                    throw new Exception('syntax error');
                                }
                            }

                            break;

                        case 'join':

                            foreach (preg_split('/\s*,\s*/', $query) as $chunk) {

                                if ($chunk && preg_match('/^\s*(?:(\w+)\s*!\s*)?(\w+[.:]?)\s*$/', $chunk, $match)) {

                                    $crud->join($match[2], null, null, null, $match[1] ? $match[1] : null);

                                } else if ($chunk && preg_match('/^\s*(?:(\w+)\s*!\s*)?(\w+)(?:\.(\w+))?(?:\s*=\s*(\w+)?(?:\.(\w+))?)?\s*$/', $chunk, $match)) {

                                    $crud->join($match[2], @$match[3], @$match[4], @$match[5], $match[1] ? $match[1] : null);

                                } else {

                                    throw new Exception('syntax error');
                                }
                            }

                            break;

                        case 'filter':

                            $array = [];

                            foreach (preg_split('/(?=\b(AND|OR)\b)/i', $query) as $chunk) {

                                if ($chunk && preg_match('/^\s*(AND|OR)?\s*(\w+[.:]?\w*)\s*(=|<|>|<>|LIKE)\s*([\s\S]*?)\s*$/i', $chunk, $match)) {

                                    $operator = $match[1] ? $match[1] : null;
                                    $value = ctype_digit($match[4]) ? (int) $match[4] : $match[4];

                                    $array[] = ['key' => $match[2], 'comparator' => $match[3], 'value' => $value, 'operator' => $operator];

                                } else {

                                    throw new Exception('syntax error');
                                }
                            }

                            $query = '';
                            $values = [];

                            foreach ($array as $row) {

                                if ($row['operator']) {

                                    $query .= " $row[operator] ";
                                }

                                $query .= "$row[key] $row[comparator] ?";
                                $values[] = $row['value'];
                            }

                            $crud->filterArgs($query, $values);

                            break;

                        case 'range':

                            if (preg_match('/^\s*(\d+)\s*,?\s*(\d*)\s*$/', $query, $match)) {

                                $crud->range(0 < $match[1] && $match[1] <= 1000 ? $match[1] : 1000, @$match[2] ? $match[2] : 0);

                            } else {

                                throw new Exception('syntax error');
                            }

                            break;

                        case 'group':

                            foreach (preg_split('/\s*,\s*/', $query) as $chunk) {

                                if ($chunk && preg_match('/^\s*(\w+[.:]?\w*)\s*$/', $chunk, $match)) {

                                    $crud->group($match[1]);

                                } else {

                                    throw new Exception('syntax error');
                                }
                            }

                            break;

                        case 'sort':

                            foreach (preg_split('/\s*,\s*/', $query) as $chunk) {

                                if ($chunk && preg_match('/^\s*(?:(\w+)\s*!\s*)?(\w+[.:]?\w*)\s*$/', $chunk, $match)) {

                                    $crud->sort($match[2], strtolower($match[1]) != 'desc');

                                } else {

                                    throw new Exception('syntax error');
                                }
                            }

                            break;

                        default:

                            throw new Exception('syntax error');
                    }

                } else {

                    throw new Exception('syntax error');
                }
            }
        }

        return $crud;
    }
}
