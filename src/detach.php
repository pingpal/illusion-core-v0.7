<?php

    declare(ticks = 1);

    namespace Illusion\Core;

    use Illusion\Core\Context\CLI;

    use Illusion\Config;

    include 'Core/App.php';

    $app = new App();
    $copy = $argv;

    $args = OptParse::instance()->apply($copy);

    $action = @$args[0];
    $action && array_shift($args);

    @array_shift($copy);
    @array_shift($copy);
    $raw = implode(' ', $copy);

    $log = t($args, 'log/text');
    $aid = t($args, 'aid/text');
    $aid = $aid && $aid != 'false';
    unset($args['log'], $args['aid']);

    $vars = new InitVars($args, $raw);
    $context = new CLI($vars);
    $app->context($context);

    if ($log && $log[0] != '/') {

        $log = Config::$LIBROOT . '/' . $log;
    }

    Detach::instance()->apply($log, $aid);

    l($action, $args, $context);
